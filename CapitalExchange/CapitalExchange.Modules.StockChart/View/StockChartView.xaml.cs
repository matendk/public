﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.StockChart.ViewModel;

namespace CapitalExchange.Modules.StockChart.View
{
    /// <summary>
    /// Interaction logic for StockChartView.xaml
    /// </summary>
    [Export]
    public partial class StockChartView : UserControl
    {
        [ImportingConstructor]
        public StockChartView()
        {
            InitializeComponent();
        }

        [Import]
        public StockChartViewModel ViewModel
        {
            get
            {
                return (StockChartViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}