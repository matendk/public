﻿using System.ComponentModel.Composition;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;

namespace CapitalExchange.Modules.StockChart
{
    [ModuleExport(nameof(StockChartModule), typeof(StockChartModule))]
    public class StockChartModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(MarketRegionNames.StockChartRegion, typeof(View.StockChartView));
        }
    }
}