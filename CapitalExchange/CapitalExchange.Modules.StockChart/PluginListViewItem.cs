﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace CapitalExchange.Modules.StockChart
{
    internal class PluginListViewItem : ListViewItem
    {
        public PluginListViewItem(string pluginName, Color lineColor)
        {
            
        }

        public string PluginName { get; set; }
        public Color LineColor { get; set; }
        //public TYPE Type { get; set; }
        //public TYPE Type { get; set; }
    }
}
