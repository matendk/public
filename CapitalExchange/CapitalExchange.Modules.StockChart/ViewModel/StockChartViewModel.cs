﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.Practices.ServiceLocation;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Prism.Mvvm;
using CapitalExchange.Plugin;

namespace CapitalExchange.Modules.StockChart.ViewModel
{
    [Export]
    public class StockChartViewModel : BindableBase, IPluginHost
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IStockEngine stockEngine;
        private IEnumerable<IHistoricalStockQuote> historicalStockQuotes;
        private IList<string> stockQuoteTickers;
        private readonly IList<DataPoint> dataPoints = new List<DataPoint>();
        private DateTime stockStartDate = DateTime.Now.AddMonths(-1);
        private PlotModel plotModel;
        private string selectedStockQuote;
        private bool isMaxSelected;
        private bool isFiveYearsSelected;
        private bool isOneYearSelected;
        private bool isThreeMonthsSelected;
        private bool isOneMonthSelected = true;
        private bool isOneWeekSelected;
        private object stockChartLineColor;
        private IEnumerable<IPlugin> plugins;


        [ImportingConstructor]
        public StockChartViewModel(IServiceLocator serviceLocator, IStockEngine stockEngine)
        {
            this.serviceLocator = serviceLocator;
            this.stockEngine = stockEngine;
            StockChartLineColor = Colors.Green;

            stockEngine.RegisterForUpdate(UpdateAction);
            this.SetupPlotModel();
        }

        private async void UpdateAction(IEnumerable<LiveStockQuote> liveStockQuotes)
        {
            await Task.Factory.StartNew(() =>
            {
                if (StockQuoteTickers != null &&
                    liveStockQuotes.Select(x => x.Name + " (" + x.Symbol + ")")
                        .ToList()
                        .SequenceEqual(stockQuoteTickers))
                    return;

                stockQuoteTickers = new List<string>();
                var oldSelectedStockQuote = SelectedStockQuote;

                foreach (var item in liveStockQuotes)
                {
                    stockQuoteTickers.Add(item.Name + " (" + item.Symbol + ")");
                }
                
                OnPropertyChanged(() => StockQuoteTickers);
                SelectedStockQuote = oldSelectedStockQuote ?? StockQuoteTickers.FirstOrDefault();
            });
        }

        private async Task GetHistoricalStockQuotes(IEnumerable<string> tickers)
        {
            HistoricalStockQuotes = await stockEngine.GetHistoricalStockQuotes(tickers, StockStartDate, DateTime.Now.AddDays(-1));
        }

        private void UpdateStock()
        {
            if (StockStartDate == DateTime.MinValue || string.IsNullOrEmpty(SelectedStockQuote))
                return;

            Task.Factory.StartNew(() =>
            {
                GetHistoricalStockQuotes(new[] { stockEngine.StockQuotes.FirstOrDefault(x => x.Value.NameAndSymbol == SelectedStockQuote).Value?.Symbol }).Wait();
                if (HistoricalStockQuotes == null) return;

                dataPoints.Clear();

                foreach (var quote in HistoricalStockQuotes)
                {
                    this.dataPoints.Add(DateTimeAxis.CreateDataPoint(DateTime.Parse(quote.Date), double.Parse(quote.Close.Replace(".", ",")))); ;
                }

                LineSeries.Points.Clear();
                LineSeries.Points.AddRange(dataPoints);

                PlotModel.InvalidatePlot(true);
                PlotModel.Axes[0].Reset();
                PlotModel.Axes[1].Reset();
            });
        }

        private void SetupPlotModel()
        {
            var tempModel = new PlotModel();

            var xAxis = new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                StringFormat = "dd/MM/yyyy",
                Title = "Date",
                //AbsoluteMinimum = 0,
                //StartPosition = Axis.ToDouble(DateTime.Now.AddYears(-1)),
                //EndPosition = Axis.ToDouble(DateTime.Now)
            };

            var yAxis = new LinearAxis()
            {
                Position = AxisPosition.Left,
                Title = "Price",
                StartPosition = 0,
                //EndPosition = 150,
                MajorGridlineStyle = LineStyle.Automatic,
                MinorGridlineStyle = LineStyle.Automatic,
            };

            tempModel.Axes.Add(xAxis);
            tempModel.Axes.Add(yAxis);

            // Create a line series
            LineSeries = new LineSeries
            {
                Tag = "Default",
                Smooth = false,
                StrokeThickness = 1,
                MarkerSize = 1,
                DataFieldX = "Date",
                DataFieldY = "Price",
                Color = ToOxyColor((Color)StockChartLineColor),
                MarkerStroke = ToOxyColor((Color)StockChartLineColor),
                MarkerFill = ToOxyColor((Color)StockChartLineColor),
                MarkerType = MarkerType.Circle
            };

            tempModel.Series.Add(LineSeries);
            PlotModel = tempModel;
        }

        [ImportMany(typeof(IPlugin))]
        public IEnumerable<IPlugin> Plugins
        {
            get { return plugins; }
            set
            {
                SetProperty(ref plugins, value);

                foreach (var plugin in plugins)
                {
                    plugin.RegisterHost(this);
                    plugin.DataPointsChanged += Plugin_DataPointsChanged;
                }
            }
        }

        private void Plugin_DataPointsChanged(IPlugin sender, DataPointsChangedEventArgs e)
        {
            if (e.DataPoints == null) return;

            var dataPoints = e.DataPoints;
            var lineSeries = PlotModel.Series.FirstOrDefault(x => x.Tag.ToString() == sender.PluginName) as LineSeries;

            var newDataPoints = dataPoints.Select(dataPoint =>
                                                DateTimeAxis.CreateDataPoint(dataPoint.Item2, Convert.ToDouble(dataPoint.Item1))).ToList();

            if (lineSeries != null)
            {
                if (!sender.PluginEnabled)
                {
                    lineSeries.Points.Clear();
                    PlotModel.InvalidatePlot(true);
                    return;
                }
                else
                {
                    if (lineSeries.MarkerFill != ToOxyColor(sender.PluginLineColor))
                    {
                        lineSeries.MarkerFill = ToOxyColor(sender.PluginLineColor);
                        lineSeries.Color = ToOxyColor(sender.PluginLineColor);
                        lineSeries.MarkerStroke = ToOxyColor(sender.PluginLineColor);
                    }

                    lineSeries.Points.Clear();
                    lineSeries.Points.AddRange(newDataPoints);
                }
            }

            if (lineSeries == null)
            {
                lineSeries = new LineSeries
                {
                    Tag = sender.PluginName,
                    Smooth = false,
                    StrokeThickness = 1,
                    MarkerSize = 1,
                    DataFieldX = "Date",
                    DataFieldY = "Price",
                    Color = ToOxyColor(sender.PluginLineColor),
                    MarkerStroke = ToOxyColor(sender.PluginLineColor),
                    MarkerFill = ToOxyColor(sender.PluginLineColor),
                    MarkerType = MarkerType.Circle
                };

                PlotModel.Series.Add(lineSeries);
                lineSeries.Points.AddRange(newDataPoints);
            }

            PlotModel.InvalidatePlot(true);
            PlotModel.Axes[0].Reset();
            PlotModel.Axes[1].Reset();
        }

        private OxyColor ToOxyColor(Color color)
        {
            return OxyColor.FromArgb(color.A, color.R, color.G, color.B);
        }

        private OxyColor ToOxyColor(SolidColorBrush brush)
        {
            return OxyColor.FromArgb(brush.Color.A, brush.Color.R, brush.Color.G, brush.Color.B);
        }

        public LineSeries LineSeries { get; private set; }

        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { SetProperty(ref plotModel, value); }
        }

        public IList<string> StockQuoteTickers => stockQuoteTickers;

        public string SelectedStockQuote
        {
            get { return selectedStockQuote; }
            set
            {
                if (!SetProperty(ref selectedStockQuote, value)) return;

                UpdateStock();
            }
        }

        public DateTime StockStartDate
        {
            get { return stockStartDate; }
            set { SetProperty(ref stockStartDate, value); }
        }

        public bool IsMaxSelected
        {
            get { return isMaxSelected; }
            set
            {
                if (!SetProperty(ref isMaxSelected, value)) return;
                if (!isMaxSelected) return;

                StockStartDate = DateTime.Now.AddYears(-10);
                UpdateStock();
            }
        }

        public bool IsFiveYearsSelected
        {
            get { return isFiveYearsSelected; }
            set
            {
                if (!SetProperty(ref isFiveYearsSelected, value)) return;
                if (!isFiveYearsSelected) return;

                StockStartDate = DateTime.Now.AddYears(-5);
                UpdateStock();
            }
        }

        public bool IsOneYearSelected
        {
            get { return isOneYearSelected; }
            set
            {
                if (!SetProperty(ref isOneYearSelected, value)) return;
                if (!isOneYearSelected) return;

                StockStartDate = DateTime.Now.AddYears(-1);
                UpdateStock();
            }
        }

        public bool IsThreeMonthsSelected
        {
            get { return isThreeMonthsSelected; }
            set
            {
                if (!SetProperty(ref isThreeMonthsSelected, value)) return;
                if (!isThreeMonthsSelected) return;

                StockStartDate = DateTime.Now.AddMonths(-3);
                UpdateStock();
            }
        }

        public bool IsOneMonthSelected
        {
            get { return isOneMonthSelected; }
            set
            {
                if (!SetProperty(ref isOneMonthSelected, value)) return;
                if (!isOneMonthSelected) return;

                StockStartDate = DateTime.Now.AddMonths(-1);
                UpdateStock();
            }
        }

        public bool IsOneWeekSelected
        {
            get { return isOneWeekSelected; }
            set
            {
                if (!SetProperty(ref isOneWeekSelected, value)) return;
                if (!isOneWeekSelected) return;

                StockStartDate = DateTime.Now.AddDays(-7);
                UpdateStock();
            }
        }

        public object StockChartLineColor
        {
            get { return stockChartLineColor; }
            set
            {
                if (!SetProperty(ref stockChartLineColor, value)) return;
                if (LineSeries == null) return;

                var t = value.ToString().Replace("System.Windows.Media.Color ", "");
                var color = ColorConverter.ConvertFromString(t);

                LineSeries.MarkerFill = ToOxyColor((Color)color);
                LineSeries.MarkerStroke = ToOxyColor((Color)color);
                LineSeries.Color = ToOxyColor((Color)color);
                PlotModel.InvalidatePlot(false);
            }
        }

        public IEnumerable<IHistoricalStockQuote> HistoricalStockQuotes
        {
            get { return historicalStockQuotes; }
            set { SetProperty(ref historicalStockQuotes, value); }
        }

        public double GetDataValueFromDate(DateTime dateTime)
        {
            return DateTimeAxis.ToDouble(dateTime);
        }

        public DateTime GetDateTimeValueFromDouble(double dateTimeValue)
        {
            return DateTimeAxis.ToDateTime(dateTimeValue);
        }
    }
}