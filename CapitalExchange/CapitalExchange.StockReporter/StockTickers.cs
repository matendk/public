﻿namespace CapitalExchange.StockReporter
{
    internal static class StockTickers
    {
        internal static readonly string[] Tickers = new string[]
        {
            "AAPL",
            "MSFT",
            "GOOG",
            "ORCL",
            "GOOG",
            "INTC",
            "QCOM",
            "AMZN",
            "CSCO",
            "BAC",
            "FB",
            "GPRO",
            "TEVA",
            "ATVI",
            "WFC",
            "SYMC",
            "F",
            "C",
            "NOK",
            "TWTR",
            "SBUX"
        };
    }
}