﻿using Prism.Mef.Modularity;
using Prism.Modularity;

namespace CapitalExchange.StockReporter
{
    [ModuleExport(nameof(StockReporterModule), typeof(StockReporterModule))]
    public class StockReporterModule : IModule
    {
        public void Initialize()
        {
            // Do nothing   
        }
    }
}