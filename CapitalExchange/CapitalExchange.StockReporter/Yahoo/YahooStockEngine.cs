﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Threading;
using CapitalExchange.SharedTypes;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;

namespace CapitalExchange.StockReporter.Yahoo
{
    [Export(typeof(IStockEngine))]
    public class YahooStockEngine : IStockEngine
    {
        private readonly YahooDownloader yahooDownloader;

        private readonly List<Action<IEnumerable<LiveStockQuote>>> actions = new List<Action<IEnumerable<LiveStockQuote>>>();

        public YahooStockEngine()
        {
            ConnectionStatus.StockStatus = "Connecting";
            yahooDownloader = new YahooDownloader();
            StockTickEvent(null, null); // Call once
            var stockTimer = new DispatcherTimer(DispatcherPriority.Background) { Interval = TimeSpan.FromSeconds(10) };
            stockTimer.Tick += StockTickEvent;
            stockTimer.Start();
        }

        private void StockTickEvent(object sender, EventArgs e)
        {
            GetLiveStockQuotes(StockTickers.Tickers);
        }

        public void GetLiveStockQuotes(IEnumerable<string> symbols)
        {
            Task.Run(() =>
            {
                var result = yahooDownloader.DownloadLiveStockQuotes(symbols).Result;
                if (result == null) return;

                foreach (var liveStockQuote in result)
                {
                    if (StockQuotes.ContainsKey(liveStockQuote.Symbol))
                    {
                        StockQuotes[liveStockQuote.Symbol] = liveStockQuote;
                    }
                    else
                    {
                        if (!StockQuotes.TryAdd(liveStockQuote.Symbol, liveStockQuote))
                            throw new Exception("Failed to add record to live stock cache.");
                    }
                }

                lock (actions)
                {
                    foreach (var action in actions)
                    {
                        try
                        {
                            action(result);
                            ConnectionStatus.StockStatus = "Connected";
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                            //Suppress
                            ConnectionStatus.StockStatus = "Error";
                        }
                    }
                }
            });
        }

        public ConcurrentDictionary<string, LiveStockQuote> StockQuotes { get; set; } = new ConcurrentDictionary<string, LiveStockQuote>();

        public async Task<IList<IHistoricalStockQuote>> GetHistoricalStockQuotes(IEnumerable<string> symbols, DateTime startDate, DateTime stopDate)
        {
            var historicalStockQuotes = new List<IHistoricalStockQuote>();
            
            var result = await yahooDownloader.DownloadHistoricalStockQuotes(symbols, startDate, stopDate);
            if (result == null) return null;

            historicalStockQuotes.AddRange(result);
            return historicalStockQuotes;
        }

        public void RegisterForUpdate(Action<IEnumerable<LiveStockQuote>> updateAction)
        {
            lock (actions)
            {
                if (actions.Contains(updateAction)) return;
                actions.Add(updateAction);
            }
        }

        public void UnregisterForUpdate(Action<IEnumerable<LiveStockQuote>> updateAction)
        {
            lock (actions)
            {
                if (!actions.Contains(updateAction)) return;
                actions.Remove(updateAction);
            }
        }
    }
}