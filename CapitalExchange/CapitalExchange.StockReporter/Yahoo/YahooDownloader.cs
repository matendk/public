﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CapitalExchange.StockReporter.Yahoo
{
    internal class YahooDownloader
    {
        private const string YahooLiveStockUrl = "https://query.yahooapis.com/v1/public/yql?q=" +
                                                 "select%20Name%2CSymbol%2CLastTradePriceOnly%2CChange%2CLastTradeDate%2CPreviousClose%2CPercentChange%2CVolume%20from%20" +
                                                 "yahoo.finance.quotes%20where%20symbol%20in%20({0})%0A%09%09&format=json&diagnostics=false&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

        private const string YahooHistoricalStockUrl = "https://query.yahooapis.com/v1/public/yql?q=" +
                                                       "select%20*%20from%20" +
                                                       "yahoo.finance.historicaldata%20where%20symbol%20in%20({0})%20and%20startDate=%20{1}%20and%20endDate=%20{2}%20%0A%09%09&format=json&diagnostics=false&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

        internal async Task<IEnumerable<LiveStockQuote>> DownloadLiveStockQuotes(IEnumerable<string> symbols)
        {
            var symbolsString = string.Join("%2C", symbols.Select(s => "%22" + s + "%22").ToArray()); // Join our symbols into a string
            var queryUrl = string.Format(YahooLiveStockUrl, symbolsString); // Add symbols to query

            var jsonContent = await DownloadAsync(queryUrl);
            var dataObject = JObject.Parse(jsonContent);

            var jsonArray = (JArray)dataObject["query"]?["results"]?["quote"];
            if (jsonArray == null) return null; // TODO: Handle this type of response (no data returned).
            var result = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<IEnumerable<LiveStockQuote>>(jsonArray.ToString()));
            return result;
        }

        public async Task<IEnumerable<IHistoricalStockQuote>> DownloadHistoricalStockQuotes(IEnumerable<string> symbols,
            DateTime startDate, DateTime stopDate)
        {
            var listOfYears = GetYearsBetweenDates(startDate, stopDate);
            var symbolsString = string.Join("%2C", symbols.Select(s => "%22" + s + "%22").ToArray()); // Join our symbols into a string
            var list = new List<HistoricalStockQuote>();

            await Task.Run(() => Parallel.ForEach(listOfYears, (tuple) =>
            {
                var queryUrl = string.Format(YahooHistoricalStockUrl, symbolsString,
                    "%22" + tuple.Item1.ToString("yyyy-MM-dd") + "%22",
                    "%22" + tuple.Item2.ToString("yyyy-MM-dd") + "%22"); // Add symbols to query
                var jsonContent = DownloadAsync(queryUrl).Result; // TODO: Handle "Be back soon" message
                var dataObject = JObject.Parse(jsonContent);

                if (dataObject.ToString().Contains("null")) return; // TODO: Handle this case more elegantly

                var jsonArray = (JArray)dataObject["query"]?["results"]?["quote"];

                if (jsonArray == null) return; // TODO: Handle this type of response (no data returned).
                var result = JsonConvert.DeserializeObject<IEnumerable<HistoricalStockQuote>>(jsonArray.ToString());
                list.AddRange(result);
            }));

            return list.OrderByDescending(x => DateTime.Parse(x.Date));
        }

        private List<Tuple<DateTime, DateTime>> GetYearsBetweenDates(DateTime startDate, DateTime stopDate)
        {
            var list = new List<Tuple<DateTime, DateTime>>();
            var oneYear = TimeSpan.FromDays(365.25);
            var tempStopDate = startDate + oneYear;
            var tempStartDate = startDate;
            var first = true;

            if (stopDate - startDate < oneYear)
            {
                list.Add(new Tuple<DateTime, DateTime>(startDate, stopDate));
                return list;
            }

            while (tempStopDate != stopDate)
            {
                list.Add(new Tuple<DateTime, DateTime>(tempStartDate, tempStopDate));
                if (first)
                {
                    tempStartDate += oneYear.Add(TimeSpan.FromDays(1));
                    first = false;
                }
                else
                    tempStartDate += oneYear;

                tempStopDate += oneYear;

                if (tempStopDate > stopDate)
                {
                    tempStopDate = stopDate;
                    list.Add(new Tuple<DateTime, DateTime>(tempStartDate, stopDate));
                }
            }

            return list;
        }

        private static readonly HttpClient HttpClient = new HttpClient();
        private static async Task<string> DownloadAsync(string queryUrl)
        {
            HttpClient.DefaultRequestHeaders.Clear();
            HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await HttpClient.GetAsync(queryUrl);

            return await response.Content.ReadAsStringAsync();
        }
    }
}