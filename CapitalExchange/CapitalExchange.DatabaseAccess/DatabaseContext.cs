﻿using System;
using System.Data.Entity;
using CapitalExchange.DatabaseAccess.Entities;

namespace CapitalExchange.DatabaseAccess
{
    public class DatabaseContext : DbContext
    {
        //Tell the database context which connection to use
        public DatabaseContext() : base("name=DatabaseConnection")
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");
        }

        //Define tabels that should be in the database
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ShareEntity> Shares { get; set; }
        public DbSet<PortfolioEntity> Portfolios { get; set; }
        public DbSet<SellOrderEntity> SellOrders { get; set; }
        public DbSet<BuyOrderEntity> BuyOrders { get; set; }
        public DbSet<PostedSellOrderEntity> PostedSellOrders { get; set; }
        public DbSet<PostedBuyOrderEntity> PostedBuyOrders { get; set; }
        public DbSet<AccessLevelEntity> AccessLevels { get; set; }
        public DbSet<TrackedStockEntity> TrackedStocks { get; set; }
        public DbSet<WithdrawTransactionEntity> WithdrawTransactions { get; set; }
        public DbSet<DepositTransactionEntity> DepositTransactions { get; set; }
        public DbSet<SalesActionEntity> SalesActions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Rename tables, so that they have a fitting name
            modelBuilder.Entity<UserEntity>().ToTable("Users");
            modelBuilder.Entity<ShareEntity>().ToTable("Shares");
            modelBuilder.Entity<WithdrawTransactionEntity>().ToTable("WithdrawTransactions");
            modelBuilder.Entity<DepositTransactionEntity>().ToTable("DepositTransactions");
            modelBuilder.Entity<SellOrderEntity>().ToTable("SellOrders");
            modelBuilder.Entity<BuyOrderEntity>().ToTable("BuyOrders");
            modelBuilder.Entity<PostedSellOrderEntity>().ToTable("PostedSellOrders");
            modelBuilder.Entity<PostedBuyOrderEntity>().ToTable("PostedBuyOrders");
            modelBuilder.Entity<PortfolioEntity>().ToTable("Portfolios");
            modelBuilder.Entity<AccessLevelEntity>().ToTable("AccessLevels");
            modelBuilder.Entity<TrackedStockEntity>().ToTable("TrackedStocks");
            modelBuilder.Entity<SalesActionEntity>().ToTable("SalesActions");
        }
    }
}