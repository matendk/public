using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using CapitalExchange.SharedTypes.Interfaces;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using CapitalExchange.Frameworks.Helpers;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;

namespace CapitalExchange.DatabaseAccess
{
    [Export(typeof(IPersistenceService))]
    public class PersistenceService : IPersistenceService
    {

        #region User
        public async Task<bool> AddUser(IUser user, string password)
        {
            var newUser = user.ToEntity();

            newUser.Salt = HashingHelper.GenerateSalt();
            newUser.Password = HashingHelper.GenerateHash(newUser.Salt, password);

            using (var databaseContext = new DatabaseContext())
            {
                var result = await databaseContext.Users.FirstOrDefaultAsync(x => x.Email == user.Email);
                if (result != null)
                    return false;

                databaseContext.Users.Add(newUser);
                databaseContext.Entry(newUser).State = EntityState.Added;
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<IUser> GetUserById(int id)
        {
            return await Task.Run(() =>
            {
                using (var db = new DatabaseContext())
                {
                    return
                        db.Users.Include(u => u.Shares)
                        .Include(u => u.Shares)
                        .Include(u => u.BuyOrders)
                        .Include(u => u.SellOrders)
                        .Include(u => u.WithdrawTransactions)
                        .Include(u => u.DepositTransactions)
                        .Include(u => u.AccessLevel)
                        .Include(u => u.SalesActions)
                        .Include(u => u.TrackedStocks)
                        .Include(u => u.Portfolios)
                        .FirstOrDefault(x => x.ID.Equals(id))
                        .ToModel();
                }
            });
        }

        public async Task<IUser> GetUserByEmail(string email)
        {
            return await Task.Run(() =>
            {
                using (var db = new DatabaseContext())
                {
                    return
                        db.Users.Include(u => u.Shares)
                            .Include(u => u.Shares)
                            .Include(u => u.BuyOrders)
                            .Include(u => u.SellOrders)
                            .Include(u => u.WithdrawTransactions)
                            .Include(u => u.DepositTransactions)
                            .Include(u => u.AccessLevel)
                            .Include(u => u.SalesActions)
                            .Include(u => u.TrackedStocks)
                            .Include(u => u.Portfolios)
                            .FirstOrDefault(x => x.Email.Equals(email))
                            .ToModel();
                }
            });
        }

        public async Task<bool> UpdateUserInformation(IUser user)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var dbUser = await databaseContext.Users
                    .FirstOrDefaultAsync(x => x.ID == user.ID);

                if (dbUser == null) return false;

                dbUser.Address = user.Address;
                dbUser.Country = user.Country;
                dbUser.Funds = user.Funds;
                dbUser.Name = user.Name;
                dbUser.Surname = user.Surname;

                await databaseContext.SaveChangesAsync();
            }

            return true;
        }

        public async Task<List<IUser>> GetAllUsersWithOrders()
        {
            var returnlist = new List<IUser>();

            await Task.Run(() =>
            {

                using (var dbcontext = new DatabaseContext())
                {
                    returnlist = dbcontext.Users
                        .Include(u => u.SellOrders)
                        .Include(u => u.Shares)
                        .Include(u => u.BuyOrders)
                        .Include(u => u.AccessLevel)
                        .Where(u => u.SellOrders.Count != 0 || u.BuyOrders.Count != 0)
                        .ToList()
                        .ToModel();
                }
            });

            return returnlist;
        }

        public async Task<string> GenerateNewPassword(IUser user)
        {
            var newPassword = PasswordGeneratorHelper.GenerateNewPassword(8);
            var newSalt = HashingHelper.GenerateSalt();
            var newPasswordHash = HashingHelper.GenerateHash(newSalt, newPassword);

            using (var databaseContext = new DatabaseContext())
            {
                var result = await databaseContext.Users.FirstOrDefaultAsync(x => x.Email == user.Email);
                if (result == null)
                    return string.Empty;

                result.Password = newPasswordHash;
                result.Salt = newSalt;
                await databaseContext.SaveChangesAsync();
            }
            return newPassword;
        }

        public async void SetFunds(decimal amount, IUser user)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var dbUser = await databaseContext.Users.FirstOrDefaultAsync(x => x.ID == user.ID);
                if (dbUser == null) return;

                dbUser.Funds = amount;

                await databaseContext.SaveChangesAsync();
            }
        }

        public async void SetReservedFunds(decimal amount, IUser user)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var dbUser = await databaseContext.Users.FirstOrDefaultAsync(x => x.ID == user.ID);
                if (dbUser == null) return;

                dbUser.ReservedFunds = amount;

                await databaseContext.SaveChangesAsync();
            }
        }

        public IList<IAccessLevel> GetAccessLevels()
        {
            var accessList = new List<IAccessLevel>();

            using (var databaseContext = new DatabaseContext())
            {
                foreach (var item in databaseContext.AccessLevels)
                {
                    accessList.Add(item.ToModel());
                }

                return accessList;
            }
        }

        public async void UpdateAccessLevel(IUser user, AccessLevelType accessLevelType)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var dbUser = await databaseContext.Users.FirstOrDefaultAsync(x => x.ID == user.ID);
                if (dbUser == null) return;

                dbUser.AccessLevelID = (int)accessLevelType;

                await databaseContext.SaveChangesAsync();
            }
        }
        #endregion

        #region Share
        public async Task<bool> AddShare(IShare share)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newShare = share.ToEntity();
                databaseContext.Shares.Add(newShare);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateShare(IShare share)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newShare = share.ToEntity();
                databaseContext.Shares.AddOrUpdate(item => item.ID, newShare);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteShare(IShare share)
        {
            using (var databaseContext = new DatabaseContext())
            {
                databaseContext.Shares.Remove(databaseContext.Shares.FirstOrDefault(item => item.ID == share.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }
        #endregion

        #region BuyOrder
        public ObservableCollection<IBuyOrder> GetBuyOrders()
        {
            ObservableCollection<IBuyOrder> returnList;
            using (var dbcontext = new DatabaseContext())
            {
                returnList = dbcontext.BuyOrders.Include(b => b.User).Include(u => u.User.AccessLevel).ToList().ToModel();
            }
            return returnList;
        }

        public async Task<bool> AddBuyOrder(IBuyOrder buyOrder)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newBuyOrder = buyOrder.ToEntity();
                databaseContext.BuyOrders.Add(newBuyOrder);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateBuyOrder(IBuyOrder buyOrder)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newBuyOrder = buyOrder.ToEntity();
                databaseContext.BuyOrders.AddOrUpdate(item => item.ID, newBuyOrder);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteBuyOrder(IBuyOrder buyOrder)
        {
            using (var databaseContext = new DatabaseContext())
            {
                databaseContext.BuyOrders.Remove(databaseContext.BuyOrders.FirstOrDefault(item => item.ID == buyOrder.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> PostBuyOrder(IBuyOrder buyOrder)
        {
            using (var dbcontext = new DatabaseContext())
            {
                dbcontext.PostedBuyOrders.Add(buyOrder.ToEntity().ToPosted());
                dbcontext.BuyOrders.Remove(dbcontext.BuyOrders.FirstOrDefault(item => item.ID == buyOrder.ID));
                await dbcontext.SaveChangesAsync();
            }

            return true;
        }
        #endregion

        #region SellOrder
        public ObservableCollection<ISellOrder> GetSellOrders()
        {
            ObservableCollection<ISellOrder> returnList;
            using (var dbcontext = new DatabaseContext())
            {
                returnList = dbcontext.SellOrders.Include(b => b.User).Include(u => u.User.AccessLevel).ToList().ToModel();
            }
            return returnList;
        }

        public async Task<bool> AddSellOrder(ISellOrder sellOrder, IEnumerable<IShare> sharesToSell)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newSellOrder = sellOrder.ToEntity();
                databaseContext.SellOrders.Add(newSellOrder);

                await databaseContext.SaveChangesAsync();

                foreach (var share in sharesToSell)
                {
                    var dbShare = databaseContext.Shares.FirstOrDefault(x => x.ID == share.ID);
                    dbShare.SellOrderID = newSellOrder.ID;
                    databaseContext.Entry(dbShare).State = EntityState.Modified;
                }

                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateSellOrder(ISellOrder sellOrder, ObservableCollection<IShare> sharesToBeRemoved, ObservableCollection<IShare> sharesToBeAdded, IUser user)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newSellOrder = sellOrder.ToEntity();

                if (sharesToBeAdded != null)
                {
                    foreach (var share in sharesToBeAdded)
                    {
                        var dbShare = databaseContext.Shares.FirstOrDefault(x => x.ID == share.ID);
                        dbShare.SellOrderID = newSellOrder.ID;
                        databaseContext.Entry(dbShare).State = EntityState.Modified;
                    }
                }

                if (sharesToBeRemoved != null)
                {
                    foreach (var share in sharesToBeRemoved)
                    {
                        var dbShare = databaseContext.Shares.FirstOrDefault(x => x.ID == share.ID);
                        dbShare.SellOrderID = null;
                        databaseContext.Entry(dbShare).State = EntityState.Modified;
                    }
                }

                databaseContext.SellOrders.AddOrUpdate(item => item.ID, newSellOrder);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteSellOrder(ISellOrder sellOrder)
        {
            using (var databaseContext = new DatabaseContext())
            {
                foreach (var share in sellOrder.Shares)
                {
                    var dbShare = databaseContext.Shares.FirstOrDefault(x => x.ID == share.ID);
                    dbShare.SellOrderID = null;
                    databaseContext.Entry(dbShare).State = EntityState.Modified;
                }

                databaseContext.SellOrders.Remove(databaseContext.SellOrders.FirstOrDefault(item => item.ID == sellOrder.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> PostSellOrder(ISellOrder sellOrder)
        {
            using (var dbcontext = new DatabaseContext())
            {
                dbcontext.PostedSellOrders.Add(sellOrder.ToEntity().ToPosted());
                dbcontext.SellOrders.Remove(dbcontext.SellOrders.FirstOrDefault(item => item.ID == sellOrder.ID));
                await dbcontext.SaveChangesAsync();
            }
            return true;
        }
        #endregion

        #region SalesAction

        public async Task<bool> AddSalesAction(ISalesAction salesAction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newSalesAction = salesAction.ToEntity();
                databaseContext.SalesActions.Add(newSalesAction);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateSalesAction(ISalesAction salesAction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newSalesAction = salesAction.ToEntity();
                databaseContext.SalesActions.AddOrUpdate(item => item.ID, newSalesAction);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteSalesAction(ISalesAction salesAction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                databaseContext.SalesActions.Remove(databaseContext.SalesActions.FirstOrDefault(item => item.ID == salesAction.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }


        #endregion

        #region Portfolio
        public async Task<bool> AddPortfolio(IPortfolio portfolio)
        {
            using (var databaseContext = new DatabaseContext())
            {
                //Add new Portfolio to DB.
                var newPortfolio = portfolio.ToEntity();
                databaseContext.Portfolios.Add(newPortfolio);

                //Set PortfolioID for connected shares
                newPortfolio.Shares.ForEach(share =>
                {
                    share.SellOrderID = databaseContext.Portfolios.LastOrDefault(item => item.UserID == portfolio.UserID).ID; //Find last added Portfolio by the current user and get the ID of it.
                    databaseContext.Shares.AddOrUpdate(item => item.ID, share);
                });

                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdatePortfolio(IPortfolio portfolio)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newPortfolio = portfolio.ToEntity();
                databaseContext.Portfolios.AddOrUpdate(item => item.ID, newPortfolio);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeletePortfolio(IPortfolio portfolio)
        {
            using (var databaseContext = new DatabaseContext())
            {
                databaseContext.Portfolios.Remove(databaseContext.Portfolios.FirstOrDefault(item => item.ID == portfolio.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }
        #endregion

        #region TrackedStock
        public async Task<bool> AddTrackedStock(ITrackedStock trackedStock)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newTrackedStock = trackedStock.ToEntity();
                databaseContext.TrackedStocks.Add(newTrackedStock);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> AddOrUpdateTrackedStock(ITrackedStock trackedStock)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newTrackedStock = trackedStock.ToEntity();
                databaseContext.TrackedStocks.AddOrUpdate(item => item.Symbol, newTrackedStock);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateTrackedStock(ITrackedStock trackedStock)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newTrackedStock = trackedStock.ToEntity();
                databaseContext.TrackedStocks.AddOrUpdate(item => item.ID, newTrackedStock);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteTrackedStock(ITrackedStock trackedStock)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var dbTrackedStock = databaseContext.TrackedStocks.FirstOrDefault(item => item.Symbol == trackedStock.Symbol);
                if (dbTrackedStock == null) return false;
                databaseContext.TrackedStocks.Remove(dbTrackedStock);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }
        #endregion

        #region DepositTransaction
        public async Task<bool> AddDepositTransaction(IDepositTransaction depositTransaction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newDepositTransaction = depositTransaction.ToEntity();
                databaseContext.DepositTransactions.Add(newDepositTransaction);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateDepositTransaction(IDepositTransaction depositTransaction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newDepositTransaction = depositTransaction.ToEntity();
                databaseContext.DepositTransactions.AddOrUpdate(transaction => transaction.ID, newDepositTransaction);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteDepositTransaction(IDepositTransaction depositTransaction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                databaseContext.DepositTransactions.Remove(databaseContext.DepositTransactions.FirstOrDefault(transaction => transaction.ID == depositTransaction.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }
        #endregion

        #region WithdrawTransaction
        public async Task<bool> AddWithdrawTransaction(IWithdrawTransaction withdrawTransaction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newWithdrawTransaction = withdrawTransaction.ToEntity();
                databaseContext.WithdrawTransactions.Add(newWithdrawTransaction);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateWithdrawTransaction(IWithdrawTransaction withdrawTransaction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                var newWithdrawTransaction = withdrawTransaction.ToEntity();
                databaseContext.WithdrawTransactions.AddOrUpdate(transaction => transaction.ID, newWithdrawTransaction);
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteWithdrawTransaction(IWithdrawTransaction withdrawTransaction)
        {
            using (var databaseContext = new DatabaseContext())
            {
                databaseContext.WithdrawTransactions.Remove(databaseContext.WithdrawTransactions.FirstOrDefault(transaction => transaction.ID == withdrawTransaction.ID));
                await databaseContext.SaveChangesAsync();
            }
            return true;
        }
        #endregion
    }
}