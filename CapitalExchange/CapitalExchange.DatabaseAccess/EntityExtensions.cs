﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CapitalExchange.DatabaseAccess.Entities;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;

namespace CapitalExchange.DatabaseAccess
{
    internal static class EntityExtensions
    {
        #region User
        internal static IUser ToModel(this UserEntity userEntity)
        {
            if (userEntity == null) return null;

            return new User(
                userEntity.ID, 
                userEntity.Name, 
                userEntity.Surname, 
                userEntity.Email, 
                userEntity.Address,
                userEntity.Country,
                userEntity.Funds, 
                userEntity.ReservedFunds, 
                userEntity.Salt, 
                userEntity.Password,
                userEntity.RegistrationDate, 
                userEntity.AccessLevel.ToModel(), 
                userEntity.Shares.ToModel(),
                userEntity.SellOrders.ToModel(),
                userEntity.BuyOrders.ToModel(), 
                userEntity.PostedSellOrders.ToModel(),
                userEntity.PostedBuyOrders.ToModel(),
                userEntity.Portfolios.ToModel(), 
                userEntity.SalesActions.ToModel(),
                userEntity.DepositTransactions.ToModel(),
                userEntity.WithdrawTransactions.ToModel(),
                userEntity.TrackedStocks.ToModel()
                );
        }

        internal static UserEntity ToEntity(this IUser user)
        {
            return new UserEntity()
            {
                AccessLevelID = user.AccessLevel.ToEntity().ID,
                ID = user.ID,
                Address = user.Address,
                Country = user.Country,
                Email = user.Email,
                ReservedFunds = user.ReservedFunds,
                Funds = user.Funds,
                Name = user.Name,
                Surname = user.Surname,
                RegistrationDate = user.RegistrationDate,
                Shares = user.Shares.ToEntity(),
                SellOrders = user.SellOrders.ToEntity(),
                BuyOrders = user.BuyOrders.ToEntity(),
                SalesActions = user.SalesActions.ToEntity(),
                DepositTransactions = user.DepositTransactions.ToEntity(),
                WithdrawTransactions = user.WithdrawTransactions.ToEntity()
            };
        }

        internal static List<IUser> ToModel(this List<UserEntity> userEntity)
        {
            if (userEntity == null) return null;
            List<IUser> returenlist = new List<IUser>();
            userEntity.ForEach(u => returenlist.Add(u.ToModel()));
            return returenlist;
        }
        #endregion

        #region AccessLevel
        internal static IAccessLevel ToModel(this AccessLevelEntity accessLevel)
        {
            AccessLevelType accesLevelType;
            if (!Enum.TryParse(accessLevel.Name, out accesLevelType))
                throw new InvalidOperationException("Could not parse AccessLevel type.");

            return new AccessLevel { Id = accessLevel.ID, AccessLevelType = accesLevelType, Price = accessLevel.Price };
        }

        internal static AccessLevelEntity ToEntity(this IAccessLevel accessLevel)
        {
            return new AccessLevelEntity()
            {
                ID = (int)accessLevel.AccessLevelType,
                Name = accessLevel.AccessLevelType.ToString(),
                Price = accessLevel.Price
            };
        }
        #endregion EndRegion

        #region Share
        internal static IShare ToModel(this ShareEntity shareEntity)
        {
            return new Share(shareEntity.ID, shareEntity.Name, shareEntity.StockSymbol, shareEntity.LastBuyPrice, shareEntity.LastBuyDate, shareEntity.SellOrderID, shareEntity.UserID, shareEntity.PortfolioID);
        }

        internal static ShareEntity ToEntity(this IShare shareEntity)
        {
            return new ShareEntity
            {
                ID = shareEntity.ID,
                UserID = shareEntity.UserID,
                PortfolioID = shareEntity.PortfolioID,
                SellOrderID = shareEntity.SellOrderID,
                Name = shareEntity.Name,
                StockSymbol = shareEntity.StockSymbol,
                LastBuyPrice = shareEntity.LastBuyPrice,
                LastBuyDate = shareEntity.LastBuyDate
            };
        }

        internal static ObservableCollection<IShare> ToModel(this List<ShareEntity> shareEntities)
        {
            if (shareEntities == null) return null;
            var shares = new ObservableCollection<IShare>();
            shareEntities.ForEach(share => shares.Add(share.ToModel()));
            return shares;
        }

        internal static List<ShareEntity> ToEntity(this ObservableCollection<IShare> shares)
        {
            return shares?.Select(item => item.ToEntity()).ToList();
        }

        internal static List<ShareEntity> ToEntity(this IEnumerable<IShare> shares)
        {
            return shares?.Select(item => item.ToEntity()).ToList();
        }

        #endregion

        #region BuyOrder
        internal static IBuyOrder ToModel(this BuyOrderEntity buyOrder)
        {
            return new BuyOrder(buyOrder.ID,
                buyOrder.ExpirationDate,
                buyOrder.Price,
                buyOrder.Volume,
                buyOrder.StockName,
                buyOrder.StockSymbol,
                buyOrder.CreationDate,
                buyOrder.UserID,
                buyOrder.User.Name + " " + buyOrder.User.Surname);
        }

        internal static ObservableCollection<IBuyOrder> ToModel(this List<BuyOrderEntity> buyOrderEntities)
        {
            if (buyOrderEntities == null) return null;
            var buyOrders = new ObservableCollection<IBuyOrder>();
            buyOrderEntities.ForEach(buyOrder => buyOrders.Add(buyOrder.ToModel()));
            return buyOrders;
        }

        internal static List<BuyOrderEntity> ToEntity(this ObservableCollection<IBuyOrder> buyOrders)
        {
            return buyOrders?.Select(item => item.ToEntity()).ToList();
        }

        internal static BuyOrderEntity ToEntity(this IBuyOrder buyOrder)
        {
            return new BuyOrderEntity()
            {
                ID = buyOrder.ID,
                ExpirationDate = buyOrder.ExpirationDate,
                Price = buyOrder.Price,
                StockName = buyOrder.StockName,
                Volume = buyOrder.Volume,
                StockSymbol = buyOrder.StockSymbol,
                CreationDate = buyOrder.CreationDate,
                UserID = buyOrder.UserID
            };
        }
        #endregion

        #region SellOrder
        internal static ISellOrder ToModel(this SellOrderEntity sellOrder)
        {
            return new SellOrder(sellOrder.ID,
                sellOrder.ExpirationDate,
                sellOrder.Price,
                sellOrder.Volume,
                sellOrder.StockName,
                sellOrder.StockSymbol,
                sellOrder.CreationDate,
                sellOrder.UserID,
                sellOrder.User.Name + " " + sellOrder.User.Surname,
                sellOrder.Shares.ToModel());
        }

        internal static ObservableCollection<ISellOrder> ToModel(this List<SellOrderEntity> sellOrderEntities)
        {
            if (sellOrderEntities == null) return null;
            var sellOrders = new ObservableCollection<ISellOrder>();
            sellOrderEntities.ForEach(sellOrder => sellOrders.Add(sellOrder.ToModel()));
            return sellOrders;
        }

        internal static List<SellOrderEntity> ToEntity(this ObservableCollection<ISellOrder> sellOrders)
        {
            return sellOrders?.Select(item => item.ToEntity()).ToList();

        }

        internal static SellOrderEntity ToEntity(this ISellOrder sellOrder)
        {
            return new SellOrderEntity()
            {
                ID = sellOrder.ID,
                ExpirationDate = sellOrder.ExpirationDate,
                Price = sellOrder.Price,
                Volume = sellOrder.Volume,
                StockSymbol = sellOrder.StockSymbol,
                StockName = sellOrder.StockName,
                CreationDate = sellOrder.CreationDate,
                UserID = sellOrder.UserID,
                Shares = sellOrder.Shares.ToEntity()
            };
        }
        #endregion

        #region DepositTransaction
        internal static IDepositTransaction ToModel(this DepositTransactionEntity depositTransaction)
        {
            return new DepositTransaction(depositTransaction.ID, depositTransaction.Amount, depositTransaction.Date, depositTransaction.UserID);
        }

        internal static ObservableCollection<IDepositTransaction> ToModel(this List<DepositTransactionEntity> depositTransactionEntities)
        {
            if (depositTransactionEntities == null) return null;
            var depositTransactions = new ObservableCollection<IDepositTransaction>();
            depositTransactionEntities.ForEach(depositTransaction => depositTransactions.Add(depositTransaction.ToModel()));
            return depositTransactions;
        }

        internal static List<DepositTransactionEntity> ToEntity(this ObservableCollection<IDepositTransaction> depositTransactions)
        {
            return depositTransactions?.Select(item => item.ToEntity()).ToList();

        }

        internal static DepositTransactionEntity ToEntity(this IDepositTransaction depositTransaction)
        {
            return new DepositTransactionEntity()
            {
                ID = depositTransaction.ID,
                Amount = depositTransaction.Amount,
                Date = depositTransaction.Date,
                UserID = depositTransaction.UserID
            };
        }
        #endregion

        #region WithdrawTransaction
        internal static IWithdrawTransaction ToModel(this WithdrawTransactionEntity withdrawTransaction)
        {
            return new WithdrawTransaction(withdrawTransaction.ID, withdrawTransaction.Amount, withdrawTransaction.Date, withdrawTransaction.UserID);
        }

        internal static ObservableCollection<IWithdrawTransaction> ToModel(this List<WithdrawTransactionEntity> withdrawTransactionEntities)
        {
            if (withdrawTransactionEntities == null) return null;
            var withdrawTransactions = new ObservableCollection<IWithdrawTransaction>();
            withdrawTransactionEntities.ForEach(withdrawTransaction => withdrawTransactions.Add(withdrawTransaction.ToModel()));
            return withdrawTransactions;
        }

        internal static List<WithdrawTransactionEntity> ToEntity(this ObservableCollection<IWithdrawTransaction> withdrawTransactions)
        {
            return withdrawTransactions?.Select(item => item.ToEntity()).ToList();

        }

        internal static WithdrawTransactionEntity ToEntity(this IWithdrawTransaction withdrawTransaction)
        {
            return new WithdrawTransactionEntity()
            {
                ID = withdrawTransaction.ID,
                Amount = withdrawTransaction.Amount,
                Date = withdrawTransaction.Date,
                UserID = withdrawTransaction.UserID
            };
        }
        #endregion

        #region PostedSellOrder
        internal static IPostedSellOrder ToModel(this PostedSellOrderEntity postedSellOrder)
        {
            return new PostedSellOrder(
                postedSellOrder.ID, 
                postedSellOrder.ExpirationDate,
                postedSellOrder.Price,
                postedSellOrder.Volume, 
                postedSellOrder.StockSymbol, 
                postedSellOrder.CreationDate,
                postedSellOrder.PostedDate,
                postedSellOrder.UserID
                );
        }

        internal static ObservableCollection<IPostedSellOrder> ToModel(this List<PostedSellOrderEntity> postedSellOrderEntities)
        {
            if (postedSellOrderEntities == null) return null;
            var postedSellOrders = new ObservableCollection<IPostedSellOrder>();
            postedSellOrderEntities.ForEach(postedSellOrder => postedSellOrders.Add(postedSellOrder.ToModel()));
            return postedSellOrders;
        }

        internal static List<PostedSellOrderEntity> ToEntity(this ObservableCollection<IPostedSellOrder> postedSellOrders)
        {
            return postedSellOrders?.Select(item => item.ToEntity()).ToList();
        }

        internal static PostedSellOrderEntity ToEntity(this IPostedSellOrder postedSellOrder)
        {
            return new PostedSellOrderEntity()
            {
                ID = postedSellOrder.ID,
                ExpirationDate = postedSellOrder.ExpirationDate,
                Price = postedSellOrder.Price,
                Volume = postedSellOrder.Volume,
                StockSymbol = postedSellOrder.StockSymbol,
                PostedDate = postedSellOrder.PostedDate,
                CreationDate = postedSellOrder.CreationDate,
                UserID = postedSellOrder.UserID
            };
        }

        internal static PostedSellOrderEntity ToPosted(this SellOrderEntity sellOrderEntity)
        {
            return new PostedSellOrderEntity()
            {
                ID = sellOrderEntity.ID,
                ExpirationDate = sellOrderEntity.ExpirationDate,
                Price = sellOrderEntity.Price,
                Volume = sellOrderEntity.Volume,
                StockSymbol = sellOrderEntity.StockSymbol,
                CreationDate = sellOrderEntity.CreationDate,
                UserID = sellOrderEntity.UserID,
                PostedDate = DateTime.Now
            };
        }
        #endregion

        #region PostedBuyOrder
        internal static IPostedBuyOrder ToModel(this PostedBuyOrderEntity postedBuyOrder)
        {
            return new PostedBuyOrder(
                postedBuyOrder.ID,
                postedBuyOrder.ExpirationDate,
                postedBuyOrder.Price,
                postedBuyOrder.Volume,
                postedBuyOrder.StockSymbol,
                postedBuyOrder.CreationDate,
                postedBuyOrder.PostedDate,
                postedBuyOrder.UserID
                );
        }

        internal static ObservableCollection<IPostedBuyOrder> ToModel(this List<PostedBuyOrderEntity> postedBuyOrderEntities)
        {
            if (postedBuyOrderEntities == null) return null;
            var postedBuyOrders = new ObservableCollection<IPostedBuyOrder>();
            postedBuyOrderEntities.ForEach(postedBuyOrder => postedBuyOrders.Add(postedBuyOrder.ToModel()));
            return postedBuyOrders;
        }

        internal static List<PostedBuyOrderEntity> ToEntity(this ObservableCollection<IPostedBuyOrder> postedBuyOrders)
        {
            return postedBuyOrders?.Select(item => item.ToEntity()).ToList();
        }

        internal static PostedBuyOrderEntity ToEntity(this IPostedBuyOrder postedBuyOrder)
        {
            return new PostedBuyOrderEntity()
            {
                ID = postedBuyOrder.ID,
                ExpirationDate = postedBuyOrder.ExpirationDate,
                Price = postedBuyOrder.Price,
                Volume = postedBuyOrder.Volume,
                StockSymbol = postedBuyOrder.StockSymbol,
                PostedDate = postedBuyOrder.PostedDate,
                CreationDate = postedBuyOrder.CreationDate,
                UserID = postedBuyOrder.UserID
            };
        }

        internal static PostedBuyOrderEntity ToPosted(this BuyOrderEntity buyOrder)
        {
            return new PostedBuyOrderEntity()
            {
                ID = buyOrder.ID,
                ExpirationDate = buyOrder.ExpirationDate,
                Price = buyOrder.Price,
                Volume = buyOrder.Volume,
                StockSymbol = buyOrder.StockSymbol,
                CreationDate = buyOrder.CreationDate,
                UserID = buyOrder.UserID,
                PostedDate = DateTime.Now
            };
        }
        #endregion

        #region Portfolio
        internal static IPortfolio ToModel(this PortfolioEntity portfolio)
        {
            return new Portfolio(portfolio.ID, portfolio.Name, portfolio.Shares.ToModel(), portfolio.UserID);
        }

        internal static ObservableCollection<IPortfolio> ToModel(this List<PortfolioEntity> portfolioEntities)
        {
            if (portfolioEntities == null) return null;
            var portfolios = new ObservableCollection<IPortfolio>();
            portfolioEntities.ForEach(portfolio => portfolios.Add(portfolio.ToModel()));
            return portfolios;
        }

        internal static List<PortfolioEntity> ToEntity(this ObservableCollection<IPortfolio> portfolios)
        {
            return portfolios?.Select(item => item.ToEntity()).ToList();
        }

        internal static PortfolioEntity ToEntity(this IPortfolio portfolio)
        {
            return new PortfolioEntity()
            {
                ID = portfolio.ID,
                Name = portfolio.Name,
                Shares = portfolio.Shares.ToEntity(),
                UserID = portfolio.UserID
            };
        }
        #endregion

        #region TrackedStock
        internal static ITrackedStock ToModel(this TrackedStockEntity trackedStock)
        {
            return new TrackedStock(trackedStock.ID, trackedStock.Symbol, trackedStock.UserID);
        }

        internal static ObservableCollection<ITrackedStock> ToModel(this List<TrackedStockEntity> trackedStockEntities)
        {
            if (trackedStockEntities == null) return null;
            var trackedStocks = new ObservableCollection<ITrackedStock>();
            trackedStockEntities.ForEach(trackedStock => trackedStocks.Add(trackedStock.ToModel()));
            return trackedStocks;
        }

        internal static List<TrackedStockEntity> ToEntity(this ObservableCollection<ITrackedStock> trackedStocks)
        {
            return trackedStocks?.Select(item => item.ToEntity()).ToList();
        }

        internal static TrackedStockEntity ToEntity(this ITrackedStock trackedStock)
        {
            return new TrackedStockEntity()
            {
                ID = trackedStock.ID,
                Symbol = trackedStock.Symbol,
                UserID = trackedStock.UserID
            };
        }
        #endregion

        #region SalesAction
        internal static ISalesAction ToModel(this SalesActionEntity salesActionEntity)
        {
            return new SalesAction(salesActionEntity.ID, salesActionEntity.Volume, salesActionEntity.Price,
                salesActionEntity.PreviousPrice, salesActionEntity.Date, salesActionEntity.UserID);
        }

        internal static ObservableCollection<ISalesAction> ToModel(this List<SalesActionEntity> salesActionEntities)
        {
            if (salesActionEntities == null) return null;
            var salesActions = new ObservableCollection<ISalesAction>();
            salesActionEntities.ForEach(saleAction => salesActions.Add(saleAction.ToModel()));
            return salesActions;
        }

        internal static List<SalesActionEntity> ToEntity(this ObservableCollection<ISalesAction> salesActions)
        {
            return salesActions?.Select(item => item.ToEntity()).ToList();
        }

        internal static SalesActionEntity ToEntity(this ISalesAction salesAction)
        {
            return new SalesActionEntity()
            {
                ID = salesAction.ID,
                Price = salesAction.Price,
                PreviousPrice = salesAction.PreviousPrice,
                Volume = salesAction.Volume,
                Date = salesAction.Date,
                UserID = salesAction.UserID
            };
        }
        #endregion

        #region Generic
        public static void Join(this List<BuyOrderEntity> orderEntity, List<BuyOrderEntity> newOrders)
        {
            foreach (var item in newOrders)
            {
                // Add new
                var order = orderEntity.FirstOrDefault(x => x.ID == item.ID);
                if (order == null)
                {
                    orderEntity.Add(item);
                    continue;
                }

                // Update
                order.ExpirationDate = item.ExpirationDate;
                order.Price = item.Price;
                order.Volume = item.Volume;
            }

            for (int i = 0; i < orderEntity.Count; i++)
            {
                var order = newOrders.FirstOrDefault(x => x.ID == orderEntity[i].ID);

                if (order == null)
                    orderEntity.RemoveAt(i);
            }

        }
        #endregion
    }
}