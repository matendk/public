namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMissingFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Password", c => c.String());
            AddColumn("dbo.Shares", "StockSymbol", c => c.String());
            DropColumn("dbo.Shares", "Symbol");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Shares", "Symbol", c => c.String());
            DropColumn("dbo.Shares", "StockSymbol");
            DropColumn("dbo.Users", "Password");
        }
    }
}
