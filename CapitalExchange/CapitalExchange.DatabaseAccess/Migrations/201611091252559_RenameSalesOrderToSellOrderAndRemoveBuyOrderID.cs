namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameSalesOrderToSellOrderAndRemoveBuyOrderID : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Shares", name: "BuyOrderID", newName: "BuyOrderEntity_ID");
            RenameColumn(table: "dbo.Shares", name: "SalesOrderID", newName: "SellOrderID");
            RenameIndex(table: "dbo.Shares", name: "IX_SalesOrderID", newName: "IX_SellOrderID");
            RenameIndex(table: "dbo.Shares", name: "IX_BuyOrderID", newName: "IX_BuyOrderEntity_ID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Shares", name: "IX_BuyOrderEntity_ID", newName: "IX_BuyOrderID");
            RenameIndex(table: "dbo.Shares", name: "IX_SellOrderID", newName: "IX_SalesOrderID");
            RenameColumn(table: "dbo.Shares", name: "SellOrderID", newName: "SalesOrderID");
            RenameColumn(table: "dbo.Shares", name: "BuyOrderEntity_ID", newName: "BuyOrderID");
        }
    }
}
