namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTableAddedSalesActionsActuallyFixedCascade : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders");
            DropForeignKey("dbo.SalesActions", "UserID", "dbo.Users");
            AddForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders", "ID", cascadeDelete: false);
            AddForeignKey("dbo.SalesActions", "UserID", "dbo.Users", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesActions", "UserID", "dbo.Users");
            DropForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders");
            AddForeignKey("dbo.SalesActions", "UserID", "dbo.Users", "ID");
            AddForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders", "ID");
        }
    }
}
