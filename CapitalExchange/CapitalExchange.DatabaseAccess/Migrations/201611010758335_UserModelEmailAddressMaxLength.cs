namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserModelEmailAddressMaxLength : DbMigration
    {
        public override void Up()
        {
            //This is empty because of a small mistake in the user model, email was marked an index and unique, but didn't have a maxlength, which indexes do (900 bytes)
        }
        
        public override void Down()
        {
        }
    }
}
