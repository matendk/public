namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccessLevels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.BuyOrders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ExpirationDate = c.DateTime(nullable: false),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Volume = c.Int(nullable: false),
                        StockSymbol = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Name = c.String(),
                        Surname = c.String(),
                        Street = c.String(),
                        Country = c.String(),
                        Salt = c.String(),
                        RegistrationDate = c.DateTime(nullable: false),
                        Funds = c.Int(nullable: false),
                        AccessLevelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccessLevels", t => t.AccessLevelID, cascadeDelete: true)
                .Index(t => t.AccessLevelID);
            
            CreateTable(
                "dbo.Shares",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Symbol = c.String(),
                        LastBuyPrice = c.Decimal(nullable: false, storeType: "money"),
                        LastSellPrice = c.Decimal(nullable: false, storeType: "money"),
                        SalesOrderID = c.Int(),
                        BuyOrderID = c.Int(),
                        PortfolioID = c.Int(),
                        UserID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BuyOrders", t => t.BuyOrderID)
                .ForeignKey("dbo.Portfolios", t => t.PortfolioID)
                .ForeignKey("dbo.SalesOrders", t => t.SalesOrderID)
                .ForeignKey("dbo.Users", t => t.UserID)
                .Index(t => t.SalesOrderID)
                .Index(t => t.BuyOrderID)
                .Index(t => t.PortfolioID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Portfolios",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.SalesOrders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ExpirationDate = c.DateTime(nullable: false),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Volume = c.Int(nullable: false),
                        StockSymbol = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.PostedBuyOrders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ExpirationDate = c.DateTime(nullable: false),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Volume = c.Int(nullable: false),
                        StockSymbol = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        PostedDate = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.PostedSalesOrders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ExpirationDate = c.DateTime(nullable: false),
                        Price = c.Decimal(nullable: false, storeType: "money"),
                        Volume = c.Int(nullable: false),
                        StockSymbol = c.String(),
                        CreationDate = c.DateTime(nullable: false),
                        PostedDate = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostedSalesOrders", "UserID", "dbo.Users");
            DropForeignKey("dbo.PostedBuyOrders", "UserID", "dbo.Users");
            DropForeignKey("dbo.BuyOrders", "UserID", "dbo.Users");
            DropForeignKey("dbo.Shares", "UserID", "dbo.Users");
            DropForeignKey("dbo.Shares", "SalesOrderID", "dbo.SalesOrders");
            DropForeignKey("dbo.SalesOrders", "UserID", "dbo.Users");
            DropForeignKey("dbo.Shares", "PortfolioID", "dbo.Portfolios");
            DropForeignKey("dbo.Portfolios", "UserID", "dbo.Users");
            DropForeignKey("dbo.Shares", "BuyOrderID", "dbo.BuyOrders");
            DropForeignKey("dbo.Users", "AccessLevelID", "dbo.AccessLevels");
            DropIndex("dbo.PostedSalesOrders", new[] { "UserID" });
            DropIndex("dbo.PostedBuyOrders", new[] { "UserID" });
            DropIndex("dbo.SalesOrders", new[] { "UserID" });
            DropIndex("dbo.Portfolios", new[] { "UserID" });
            DropIndex("dbo.Shares", new[] { "UserID" });
            DropIndex("dbo.Shares", new[] { "PortfolioID" });
            DropIndex("dbo.Shares", new[] { "BuyOrderID" });
            DropIndex("dbo.Shares", new[] { "SalesOrderID" });
            DropIndex("dbo.Users", new[] { "AccessLevelID" });
            DropIndex("dbo.BuyOrders", new[] { "UserID" });
            DropTable("dbo.PostedSalesOrders");
            DropTable("dbo.PostedBuyOrders");
            DropTable("dbo.SalesOrders");
            DropTable("dbo.Portfolios");
            DropTable("dbo.Shares");
            DropTable("dbo.Users");
            DropTable("dbo.BuyOrders");
            DropTable("dbo.AccessLevels");
        }
    }
}
