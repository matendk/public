using CapitalExchange.DatabaseAccess.Entities;
using System.Data.Entity.Migrations;

namespace CapitalExchange.DatabaseAccess.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DatabaseContext context)
        {

        }
    }
}