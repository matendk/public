namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTableAddedSalesActions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalesActions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Volume = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        PreviousPrice = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                        BuyOrderID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BuyOrders", t => t.BuyOrderID, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: false)
                .Index(t => t.UserID)
                .Index(t => t.BuyOrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesActions", "UserID", "dbo.Users");
            DropForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders");
            DropIndex("dbo.SalesActions", new[] { "BuyOrderID" });
            DropIndex("dbo.SalesActions", new[] { "UserID" });
            DropTable("dbo.SalesActions");
        }
    }
}
