namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedReservedFunds : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BuyOrders", "StockName", c => c.String());
            AddColumn("dbo.Users", "ReservedFunds", c => c.Decimal(nullable: false, storeType: "money"));
            AddColumn("dbo.SellOrders", "StockName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SellOrders", "StockName");
            DropColumn("dbo.Users", "ReservedFunds");
            DropColumn("dbo.BuyOrders", "StockName");
        }
    }
}
