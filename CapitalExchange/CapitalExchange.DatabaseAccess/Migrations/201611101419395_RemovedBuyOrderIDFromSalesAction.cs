namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedBuyOrderIDFromSalesAction : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders");
            DropIndex("dbo.SalesActions", new[] { "BuyOrderID" });
            DropColumn("dbo.SalesActions", "BuyOrderID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalesActions", "BuyOrderID", c => c.Int(nullable: false));
            CreateIndex("dbo.SalesActions", "BuyOrderID");
            AddForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders", "ID");
        }
    }
}
