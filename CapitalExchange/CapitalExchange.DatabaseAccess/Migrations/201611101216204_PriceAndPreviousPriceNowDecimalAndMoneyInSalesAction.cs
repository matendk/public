namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PriceAndPreviousPriceNowDecimalAndMoneyInSalesAction : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SalesActions", "Price", c => c.Decimal(nullable: false, storeType: "money"));
            AlterColumn("dbo.SalesActions", "PreviousPrice", c => c.Decimal(nullable: false, storeType: "money"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SalesActions", "PreviousPrice", c => c.Int(nullable: false));
            AlterColumn("dbo.SalesActions", "Price", c => c.Int(nullable: false));
        }
    }
}
