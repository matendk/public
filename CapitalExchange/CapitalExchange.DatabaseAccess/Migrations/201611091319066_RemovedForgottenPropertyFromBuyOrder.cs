namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedForgottenPropertyFromBuyOrder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Shares", "BuyOrderEntity_ID", "dbo.BuyOrders");
            DropIndex("dbo.Shares", new[] { "BuyOrderEntity_ID" });
            DropColumn("dbo.Shares", "BuyOrderEntity_ID");
        }

        public override void Down()
        {
            AddColumn("dbo.Shares", "BuyOrderEntity_ID", c => c.Int());
            CreateIndex("dbo.Shares", "BuyOrderEntity_ID");
            AddForeignKey("dbo.Shares", "BuyOrderEntity_ID", "dbo.BuyOrders", "ID");
        }
    }
}
