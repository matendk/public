namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTableAddedSalesActionsActuallyFixedCascade1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders");
            AddForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders");
            AddForeignKey("dbo.SalesActions", "BuyOrderID", "dbo.BuyOrders", "ID", cascadeDelete: true);
        }
    }
}
