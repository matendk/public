namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingLastBuyDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Shares", "LastBuyDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Shares", "LastSellPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Shares", "LastSellPrice", c => c.Decimal(nullable: false, storeType: "money"));
            DropColumn("dbo.Shares", "LastBuyDate");
        }
    }
}
