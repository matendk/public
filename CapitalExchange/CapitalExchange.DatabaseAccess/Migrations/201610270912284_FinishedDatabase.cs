namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinishedDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DepositTransactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, storeType: "money"),
                        Date = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.WithdrawTransactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, storeType: "money"),
                        Date = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WithdrawTransactions", "UserID", "dbo.Users");
            DropForeignKey("dbo.DepositTransactions", "UserID", "dbo.Users");
            DropIndex("dbo.WithdrawTransactions", new[] { "UserID" });
            DropIndex("dbo.DepositTransactions", new[] { "UserID" });
            DropTable("dbo.WithdrawTransactions");
            DropTable("dbo.DepositTransactions");
        }
    }
}
