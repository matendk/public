namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewTableAddedTrackedStocks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TrackedStocks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Symbol = c.String(),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TrackedStocks", "UserID", "dbo.Users");
            DropIndex("dbo.TrackedStocks", new[] { "UserID" });
            DropTable("dbo.TrackedStocks");
        }
    }
}
