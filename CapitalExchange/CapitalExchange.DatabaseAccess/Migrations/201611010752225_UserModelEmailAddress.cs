namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserModelEmailAddress : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AccessLevels", newName: "AccessLevelEntities");
            RenameTable(name: "dbo.BuyOrders", newName: "BuyOrderEntities");
            RenameTable(name: "dbo.Users", newName: "UserEntities");
            RenameTable(name: "dbo.DepositTransactions", newName: "DepositTransactionEntities");
            RenameTable(name: "dbo.Portfolios", newName: "PortfolioEntities");
            RenameTable(name: "dbo.Shares", newName: "ShareEntities");
            RenameTable(name: "dbo.PostedBuyOrders", newName: "PostedBuyOrderEntities");
            RenameTable(name: "dbo.PostedSalesOrders", newName: "PostedSalesOrderEntities");
            RenameTable(name: "dbo.SalesOrders", newName: "SalesOrderEntities");
            RenameTable(name: "dbo.WithdrawTransactions", newName: "WithdrawTransactionEntities");
            AddColumn("dbo.UserEntities", "Address", c => c.String());
            //CreateIndex("dbo.UserEntities", "Email", unique: true, name: "EmailIndex");
            DropColumn("dbo.UserEntities", "Street");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserEntities", "Street", c => c.String());
            DropIndex("dbo.UserEntities", "EmailIndex");
            DropColumn("dbo.UserEntities", "Address");
            RenameTable(name: "dbo.WithdrawTransactionEntities", newName: "WithdrawTransactions");
            RenameTable(name: "dbo.SalesOrderEntities", newName: "SalesOrders");
            RenameTable(name: "dbo.PostedSalesOrderEntities", newName: "PostedSalesOrders");
            RenameTable(name: "dbo.PostedBuyOrderEntities", newName: "PostedBuyOrders");
            RenameTable(name: "dbo.ShareEntities", newName: "Shares");
            RenameTable(name: "dbo.PortfolioEntities", newName: "Portfolios");
            RenameTable(name: "dbo.DepositTransactionEntities", newName: "DepositTransactions");
            RenameTable(name: "dbo.UserEntities", newName: "Users");
            RenameTable(name: "dbo.BuyOrderEntities", newName: "BuyOrders");
            RenameTable(name: "dbo.AccessLevelEntities", newName: "AccessLevels");
        }
    }
}
