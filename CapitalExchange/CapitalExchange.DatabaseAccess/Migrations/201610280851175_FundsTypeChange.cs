namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FundsTypeChange : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Funds", c => c.Decimal(nullable: false, storeType: "money"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Funds", c => c.Int(nullable: false));
        }
    }
}
