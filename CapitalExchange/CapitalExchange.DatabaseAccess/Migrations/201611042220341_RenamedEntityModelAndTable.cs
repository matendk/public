namespace CapitalExchange.DatabaseAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedEntityModelAndTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PostedSalesOrders", newName: "PostedSellOrders");
            RenameTable(name: "dbo.SalesOrders", newName: "SellOrders");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.SellOrders", newName: "SalesOrders");
            RenameTable(name: "dbo.PostedSellOrders", newName: "PostedSalesOrders");
        }
    }
}
