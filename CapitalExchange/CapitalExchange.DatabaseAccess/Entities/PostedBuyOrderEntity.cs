﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class PostedBuyOrderEntity
    {
        public int ID { get; set; }

        [DisplayName("Expiration Date")]
        public DateTime ExpirationDate { get; set; }
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }
        public int Volume { get; set; }
        [DisplayName("Stock")]
        public string StockSymbol { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime PostedDate { get; set; }

        public int UserID { get; set; }
        public UserEntity User { get; set; }
    }
}