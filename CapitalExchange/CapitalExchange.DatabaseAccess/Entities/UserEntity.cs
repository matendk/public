﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class UserEntity
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        [Index("EmailIndex", IsUnique = true)]
        [MaxLength(255)]
        public string Email { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Salt { get; set; }
        public string Password { get; set; }
        [Column(TypeName = "Money")]
        public decimal Funds { get; set; }
        [Column(TypeName = "Money")]
        public decimal ReservedFunds { get; set; }
        public DateTime RegistrationDate { get; set; }
        public List<ShareEntity> Shares { get; set; }
        public List<PortfolioEntity> Portfolios { get; set; }
        public List<SellOrderEntity> SellOrders { get; set; }
        public List<BuyOrderEntity> BuyOrders { get; set; }
        public List<PostedSellOrderEntity> PostedSellOrders { get; set; }
        public List<PostedBuyOrderEntity> PostedBuyOrders { get; set; }
        public List<WithdrawTransactionEntity> WithdrawTransactions { get; set; }
        public List<DepositTransactionEntity> DepositTransactions { get; set; }
        public List<TrackedStockEntity> TrackedStocks { get; set; }
        public List<SalesActionEntity> SalesActions { get; set; }

        public int AccessLevelID { get; set; }
        public AccessLevelEntity AccessLevel { get; set; }
    }
}