﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class DepositTransactionEntity
    {
        public int ID { get; set; }

        [Column(TypeName = "Money")]
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }

        public int UserID { get; set; }
        public UserEntity User { get; set; }
    }
}