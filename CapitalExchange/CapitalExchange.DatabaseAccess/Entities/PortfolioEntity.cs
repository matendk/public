﻿using System.Collections.Generic;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class PortfolioEntity
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public List<ShareEntity> Shares { get; set; }

        public int UserID { get; set; }
        public UserEntity User { get; set; }
    }
}