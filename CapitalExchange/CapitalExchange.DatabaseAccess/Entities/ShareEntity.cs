﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class ShareEntity
    {
        public int ID { get; set; }

        public string Name { get; set; }
        [DisplayName("Stock")]
        public string StockSymbol { get; set; }
        [Column(TypeName = "Money")]
        public decimal LastBuyPrice { get; set; }
        public DateTime LastBuyDate { get; set; }

        public int? SellOrderID { get; set; }
        public SellOrderEntity SellOrder { get; set; }

        public int? PortfolioID { get; set; }
        public PortfolioEntity Portfolio { get; set; }

        public int? UserID { get; set; }
        public UserEntity User { get; set; }
    }
}