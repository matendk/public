﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class SellOrderEntity
    {
        public int ID { get; set; }

        [DisplayName("Expiration Date")]
        public DateTime ExpirationDate { get; set; }
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }
        public int Volume { get; set; }
        [DisplayName("Stock")]
        public string StockSymbol { get; set; }
        public string StockName { get; set; }

        public DateTime CreationDate { get; set; }

        public int UserID { get; set; }
        public UserEntity User { get; set; }
        public List<ShareEntity> Shares { get; set; } 
    }
}