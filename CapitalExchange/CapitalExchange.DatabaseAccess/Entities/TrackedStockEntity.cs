﻿namespace CapitalExchange.DatabaseAccess.Entities
{
    public class TrackedStockEntity
    {
        public int ID { get; set; }

        public string Symbol { get; set; }

        public int UserID { get; set; }
        public UserEntity User { get; set; }
    }
}