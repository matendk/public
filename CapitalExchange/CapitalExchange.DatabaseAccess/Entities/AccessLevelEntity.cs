﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class AccessLevelEntity
    {
        public int ID { get; set; }

        public string Name { get; set; }
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }
    }
}