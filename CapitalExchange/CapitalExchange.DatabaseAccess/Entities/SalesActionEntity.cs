﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CapitalExchange.DatabaseAccess.Entities
{
    public class SalesActionEntity
    {
        public int ID { get; set; }

        public int Volume { get; set; }
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }
        [Column(TypeName = "Money")]
        public decimal PreviousPrice { get; set; }
        public DateTime Date { get; set; }

        public int UserID { get; set; }
        public UserEntity User { get; set; }
    }
}