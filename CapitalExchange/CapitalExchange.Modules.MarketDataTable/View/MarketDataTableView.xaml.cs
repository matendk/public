﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.MarketDataTable.ViewModel;

namespace CapitalExchange.Modules.MarketDataTable.View
{
    /// <summary>
    /// Interaction logic for MarketDataTableView.xaml
    /// </summary>
    [Export]
    public partial class MarketDataTableView : UserControl
    {
        [ImportingConstructor]
        public MarketDataTableView()
        {
            InitializeComponent();
        }

        [Import]
        public MarketDataTableViewModel ViewModel
        {
            get
            {
                return (MarketDataTableViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}