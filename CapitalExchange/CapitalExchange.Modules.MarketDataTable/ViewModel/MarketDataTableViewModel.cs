﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common.Dialogs.Orders.View;
using Prism.Commands;
using Prism.Mvvm;

namespace CapitalExchange.Modules.MarketDataTable.ViewModel
{
    [Export]
    public class MarketDataTableViewModel : BindableBase
    {
        #region Constructor
        [ImportingConstructor]
        public MarketDataTableViewModel(IStockEngine stockEngine, IUserService userService)
        {
            this.stockEngine = stockEngine;
            this.userService = userService;
            this.activeUser = userService.GetActiveUser();
            stockEngine.RegisterForUpdate(DoStockUpdate);

            AddOrDeleteTrackedStockCommand = new DelegateCommand<bool?>(AddOrDeleteTrackedStockCommandExecute);
            BuyStockCommand = new DelegateCommand<LiveStockQuote>(BuyStockCommandExecute);
            SellStockCommand = new DelegateCommand<LiveStockQuote>(SellStockCommandExecute);
        }
        #endregion


        #region Privates
        private readonly IStockEngine stockEngine;
        private readonly IUserService userService;
        private readonly IUser activeUser;
        #endregion


        #region Properties
        private ObservableCollection<LiveStockQuote> liveStockQuotes;
        public ObservableCollection<LiveStockQuote> LiveStockQuotes
        {
            get { return liveStockQuotes; }
            set { SetProperty(ref liveStockQuotes, value); }
        }

        private ObservableCollection<LiveStockQuote> trackedLiveStockQuotes;
        public ObservableCollection<LiveStockQuote> TrackedLiveStockQuotes
        {
            get { return trackedLiveStockQuotes; }
            set { SetProperty(ref trackedLiveStockQuotes, value); }
        }

        private LiveStockQuote selectedLiveStockQuote;
        public LiveStockQuote SelectedLiveStockQuote
        {
            get { return selectedLiveStockQuote; }
            set
            {
                SetProperty(ref selectedLiveStockQuote, value);
            }
        }
        #endregion

        
        #region Commands
        public DelegateCommand<LiveStockQuote> BuyStockCommand { get; set; }
        private void BuyStockCommandExecute(LiveStockQuote stockQuote)
        {
            var createBuyOrderDialog = new CreateBuyOrderDialogView();
            createBuyOrderDialog.Show();
        }

        public DelegateCommand<LiveStockQuote> SellStockCommand { get; set; }
        private void SellStockCommandExecute(LiveStockQuote stockQuote)
        {
            var createSellOrderDialogView = new CreateSellOrderDialogView();
            createSellOrderDialogView.Show();
        }

        public DelegateCommand<bool?> AddOrDeleteTrackedStockCommand { get; set; }
        private void AddOrDeleteTrackedStockCommandExecute(bool? addTrackedStock)
        {
            if (addTrackedStock == true && SelectedLiveStockQuote != null)
            {
                //Update livestockquptes, trackedlivestockquotes, & user tracked stocks (have to do it here, too slow otherwise)
                var stock = LiveStockQuotes.FirstOrDefault(s => s.Symbol == SelectedLiveStockQuote.Symbol);
                stock.IsTracked = true;
                TrackedLiveStockQuotes.Add(stock);
                OnPropertyChanged(nameof(LiveStockQuotes));
                OnPropertyChanged(nameof(TrackedLiveStockQuotes));
                activeUser.TrackedStocks.Add(new TrackedStock(stock.Symbol, activeUser.ID));

                //Update database
                userService.AddOrUpdateTrackedStock(new TrackedStock(stock.Symbol, activeUser.ID));
            }
            else if (addTrackedStock == false && SelectedLiveStockQuote != null)
            {
                var trackedStock = activeUser.TrackedStocks.FirstOrDefault(s => s.Symbol == SelectedLiveStockQuote.Symbol);

                //Update livestockquptes, trackedlivestockquotes, & user tracked stocks (have to do it here, too slow otherwise)
                var stock = LiveStockQuotes.FirstOrDefault(s => s.Symbol == trackedStock.Symbol);
                TrackedLiveStockQuotes.Remove(stock);
                stock.IsTracked = false;
                OnPropertyChanged(nameof(LiveStockQuotes));
                OnPropertyChanged(nameof(TrackedLiveStockQuotes));
                activeUser.TrackedStocks.Remove(trackedStock);

                //Update database
                userService.DeleteTrackedStock(trackedStock);
            }
        }
        #endregion


        #region Methods
        private void DoStockUpdate(IEnumerable<LiveStockQuote> stockList)
        {
            //Get the currently tracked stocks
            var trackedStocks = stockList.Where(stock => activeUser.TrackedStocks.FirstOrDefault(trackedStock => trackedStock.Symbol == stock.Symbol) != null);

            //Update IsTracked for each stock
            stockList.ToList().Where(stock => trackedStocks.Contains(stock)).ToList().ForEach(stock => stock.IsTracked = true);

            //Set the stock list
            LiveStockQuotes = new ObservableCollection<LiveStockQuote>(stockList);
            OnPropertyChanged(nameof(LiveStockQuotes));

            //Set the tracked stock list
            if (trackedStocks != null)
            {
                TrackedLiveStockQuotes = new ObservableCollection<LiveStockQuote>(trackedStocks);
                OnPropertyChanged(nameof(TrackedLiveStockQuotes));
            }
        }
        #endregion
    }
}