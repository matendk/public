﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.MarketDataTable.View;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;

namespace CapitalExchange.Modules.MarketDataTable
{
    [ModuleExport(nameof(MarketDataTableModule), typeof(MarketDataTableModule))]
    [ModuleDependency("StockEngineModule")]
    public class MarketDataTableModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(MarketRegionNames.MarketDataTableRegion, typeof(MarketDataTableView));
        }
    }
}