﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.Plugin
{
    public interface IPluginHost : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets the historical stock quotes.
        /// </summary>
        /// <value>
        /// The historical stock quotes.
        /// </value>
        IEnumerable<IHistoricalStockQuote> HistoricalStockQuotes { get; }
        /// <summary>
        /// Gets the data value from date.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns></returns>
        double GetDataValueFromDate(DateTime dateTime);
        /// <summary>
        /// Gets the date time value from double.
        /// </summary>
        /// <param name="dateTimeValue">The date time value.</param>
        /// <returns></returns>
        DateTime GetDateTimeValueFromDouble(double dateTimeValue);
    }
}