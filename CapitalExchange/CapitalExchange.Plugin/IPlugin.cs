﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;

namespace CapitalExchange.Plugin
{
    public interface IPlugin : INotifyPropertyChanged
    {
        string PluginName { get; set; }
        bool PluginEnabled { get; set; }
        SolidColorBrush PluginLineColor { get; set; }

        /// <summary>
        /// Occurs when [data points changed].
        /// </summary>
        event DataPointsChangedHandler DataPointsChanged;

        /// <summary>
        /// Registers the host.
        /// </summary>
        /// <param name="host">The host.</param>
        void RegisterHost(IPluginHost host);
        /// <summary>
        /// Unregisters the host.
        /// </summary>
        void UnregisterHost();
    }

    /// <summary>
    /// Delegate used when data points changed called
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="DataPointsChangedEventArgs"/> instance containing the event data.</param>
    public delegate void DataPointsChangedHandler(IPlugin sender, DataPointsChangedEventArgs e);

    public class DataPointsChangedEventArgs : EventArgs
    {
        public IEnumerable<Tuple<decimal, DateTime>> DataPoints { get; private set; }

        public DataPointsChangedEventArgs(IEnumerable<Tuple<decimal, DateTime>> dataPoints)
        {
            DataPoints = dataPoints;
        }
    }
}