﻿using System;
using System.Linq;
using CapitalExchange.DatabaseAccess;
using CapitalExchange.Services;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EFTests
{
    [TestClass] // Vi definere at dette er en test klasse
    public class AccessLevelServiceTests 
    {
        [TestMethod] // Definere at dette er en test metode
        public void GivenAccessLevelServiceCreated_GetAllAccessLevels_ValidateAmountOfAccessLevelsIs2()
        {
            // Arrange (Her setter vi vores service op)
            IAccessLevelService accessLevelService = new AccessLevelService(new PersistenceService());

            // Act (Her laver vi en aktion)
            var accessLevelList = accessLevelService.GetAccessLevels();

            // Assert (Her validere vi vores aktion)
            Assert.AreEqual(accessLevelList.Count(), 2);
        }
    }
}