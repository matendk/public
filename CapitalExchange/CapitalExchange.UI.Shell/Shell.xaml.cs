﻿using System.ComponentModel.Composition;
using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.View;

namespace CapitalExchange.UI.Shell
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    [Export]
    public partial class Shell : Window
    {
        public Shell()
        {
            InitializeComponent();
        }

        [Import]
        public ShellViewModel ViewModel
        {
            get
            {
                return (ShellViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}