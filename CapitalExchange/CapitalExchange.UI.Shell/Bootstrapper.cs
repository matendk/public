﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.View;
using Prism.Mef;
using Prism.Modularity;

namespace CapitalExchange.UI.Shell
{
    public class Bootstrapper : MefBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return this.Container.GetExportedValue<Shell>();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();
            Application.Current.MainWindow = (Shell)this.Shell;

            ((Shell)this.Shell).Visibility = Visibility.Hidden;

            LoginRoutine();
            
            Application.Current.MainWindow.Show();
        }

        private void LoginRoutine()
        {
            var loginDialogView = new LoginDialogView();
            var dialogResult = loginDialogView.ShowDialog();

            loginDialogView.Close();

            if (dialogResult == null || !dialogResult.Value)
            {
                // User has no access.
                Application.Current.Shutdown();
            }
        }

        protected override IModuleCatalog CreateModuleCatalog()
        {
            return new DirectoryModuleCatalog { ModulePath = @".\\Modules" };
        }

        protected override void ConfigureAggregateCatalog()
        {
            base.ConfigureAggregateCatalog();

            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(Bootstrapper).Assembly));

            var modulesCatalog = new DirectoryCatalog("Modules");
            this.AggregateCatalog.Catalogs.Add(modulesCatalog);

            if (Directory.Exists("Plugins"))
            {
                var pluginsCatalog = new DirectoryCatalog("Plugins");
                this.AggregateCatalog.Catalogs.Add(pluginsCatalog);
            }
        }

        protected override CompositionContainer CreateContainer()
        {
            var container = base.CreateContainer();
            container.ComposeExportedValue(container);
            return container;
        }
    }
}