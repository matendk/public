﻿using System;
using System.ComponentModel.Composition;
using  CapitalExchange.SharedTypes;
using System.Timers;
using Prism.Mvvm;

namespace CapitalExchange.Modules.StatusBar.ViewModel
{
    [Export]
    public class StatusBarViewModel : BindableBase
    {
        private int count = 1;

        [ImportingConstructor]
        public StatusBarViewModel()
        {
            OrderCountdown_Elapsed(null,null);
            var OrderCountdown = new Timer(1000);
            OrderCountdown.Elapsed += OrderCountdown_Elapsed;
            OrderCountdown.AutoReset = true;
            OrderCountdown.Start();
        }

        private void OrderCountdown_Elapsed(object sender, ElapsedEventArgs e)
        {
            CountDown = "Update in " + Convert.ToString(count) + " seconds";
            if (count == 0)
            {
                CountDown = "Updating";
                if(ConnectionStatus.DatabaseStatus != null)
                DatabaseConnected = "Orders status: " + ConnectionStatus.DatabaseStatus;
                StockConnected = "Stock status: " + ConnectionStatus.StockStatus;
                count = 5;
            }
            else
            {
                count--;
            }
        }

        private string stockConnected;
        public string StockConnected
        {
            get { return stockConnected; }
            set { SetProperty(ref stockConnected, value); }
        }

        private string databaseConnected;
        public string DatabaseConnected
        {
            get { return databaseConnected; }
            set { SetProperty(ref databaseConnected, value); }
        }

        private string countDown;
        public string CountDown
        {
            get { return countDown; }
            set { SetProperty(ref countDown,value); }
        }

    }
}
