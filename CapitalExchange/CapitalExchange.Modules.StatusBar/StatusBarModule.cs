﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.StatusBar.View;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using CapitalExchange.UI.Common;

namespace CapitalExchange.Modules.StatusBar
{
    [ModuleExport(nameof(StatusBarModule), typeof(StatusBarModule))]
    public class StatusBarModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(RegionNames.StatusRegion, typeof(StatusBarView));
        }
    }
}
