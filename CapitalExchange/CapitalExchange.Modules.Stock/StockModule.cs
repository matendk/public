﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.Stock.View;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;

namespace CapitalExchange.Modules.Stock
{
    [ModuleExport("StockModule", typeof(StockModule))]
    [ModuleDependency("SellOrdersModule")]
    [ModuleDependency("BuyOrdersModule")]
    [ModuleDependency("OwnedSharesModule")]
    [ModuleDependency("FundsAndStatisticsModule")]
    public class StockModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(RegionNames.NavigationBarRegion, typeof(StockViewNavigationItemView));
        }
    }
}