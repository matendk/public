﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.FundsAndStatistics.View;
using CapitalExchange.UI.Common;
using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;
using Prism.Regions;

namespace CapitalExchange.Modules.Stock.ViewModel
{
    [Export]
    public class StockViewModel : BindableBase, INavigationAware
    {
        private readonly IRegionManager _regionManager;
        private readonly IServiceLocator _serviceLocator;

        [ImportingConstructor]
        public StockViewModel(IRegionManager regionManager, IServiceLocator serviceLocator)
        {
            this._regionManager = regionManager;
            this._serviceLocator = serviceLocator;
        }

        #region INavigationAware 

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var fundsAndStatisticsView = _serviceLocator.GetInstance<FundsAndStatiscticsView>();
            _regionManager.Regions[StockRegionNames.StockFundsAndStatisticsRegion].Activate(fundsAndStatisticsView);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            // This view will handle all navigation view requests for StockView, so we always return true.
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            var fundsAndStatisticsView = _serviceLocator.GetInstance<FundsAndStatiscticsView>();
            _regionManager.Regions[StockRegionNames.StockFundsAndStatisticsRegion].Deactivate(fundsAndStatisticsView);
        }

        #endregion
    }
}