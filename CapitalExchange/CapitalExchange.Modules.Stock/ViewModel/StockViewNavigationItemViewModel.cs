﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using CapitalExchange.Modules.Stock.View;
using CapitalExchange.UI.Common;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;

namespace CapitalExchange.Modules.Stock.ViewModel
{
    [Export]
    public class StockViewNavigationItemViewModel : BindableBase, IPartImportsSatisfiedNotification
    {
        private readonly IRegionManager _regionManager;
        private bool _isStockActive;

        [ImportingConstructor]
        public StockViewNavigationItemViewModel(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
            this.NavigateCommand = new DelegateCommand(Navigate);
        }

        public void OnImportsSatisfied()
        {
            //var mainContentRegion = this.RegionManager.Regions[RegionNames.MainRegion];

            //if (mainContentRegion?.NavigationService != null)
            //{
            //    mainContentRegion.NavigationService.Navigated += this.MainContentRegion_Navigated;
            //}
        }

        private void MainContentRegion_Navigated(object sender, RegionNavigationEventArgs e)
        {
        }

        private void Navigate()
        {
            this._regionManager.RequestNavigate(RegionNames.MainRegion, new Uri(nameof(StockView), UriKind.Relative));
        }

        public ICommand NavigateCommand { get; }

        #region Properties

        public bool IsMarketActive
        {
            get { return _isStockActive; }
            set { SetProperty(ref _isStockActive, value); }
        }

        #endregion
    }
}
