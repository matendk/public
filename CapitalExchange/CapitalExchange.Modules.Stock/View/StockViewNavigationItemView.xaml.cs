﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.Stock.ViewModel;

namespace CapitalExchange.Modules.Stock.View
{
    /// <summary>
    /// Interaction logic for MarketViewNavigationItem.xaml
    /// </summary>
    [Export]
    public partial class StockViewNavigationItemView : UserControl
    {
        public StockViewNavigationItemView()
        {
            InitializeComponent();
        }

        [Import]
        public StockViewNavigationItemViewModel ViewModel
        {
            set { this.DataContext = value; }
        }
    }
}