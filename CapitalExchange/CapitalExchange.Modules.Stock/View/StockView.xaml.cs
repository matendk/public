﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.Stock.ViewModel;
using Prism.Regions;

namespace CapitalExchange.Modules.Stock.View
{
    /// <summary>
    /// Interaction logic for MarketView.xaml
    /// </summary>
    [Export(nameof(StockView))]
    public partial class StockView : UserControl, INavigationAware
    {
        public StockView()
        {
            InitializeComponent();
        }

        [Import]
        public IRegionManager regionManager;

        [Import]
        public StockViewModel ViewModel
        {
            get
            {
                return (StockViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }

        #region INavigationAware Implementation

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            // This view will handle all navigation view requests for MarketView, so we always return true.
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            // Intentionally not implemented
        }

        #endregion
    }
}