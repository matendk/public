﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Windows.Threading;
using CapitalExchange.Frameworks.Helpers;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;

namespace CapitalExchange.Services
{
    [Export(typeof(IUserService))]
    public class UserService : IUserService
    {
        private readonly IPersistenceService persistenceService;
        readonly DispatcherTimer userUpdateTimer;
        private User activeUser;

        [ImportingConstructor]
        public UserService(IPersistenceService persistenceService)
        {
            this.persistenceService = persistenceService;
            userUpdateTimer = new DispatcherTimer(DispatcherPriority.Background) { Interval = TimeSpan.FromSeconds(5) };
            userUpdateTimer.Tick += UserUpdateTimerTick;
        }

        private async void UserUpdateTimerTick(object sender, EventArgs eventArgs)
        {
            var newUserData = await persistenceService.GetUserById(activeUser.ID);
            if (newUserData == null) return;
            UpdateActiveUser(newUserData);
        }

        private void UpdateActiveUser(IUser user)
        {
            if (activeUser == null) return;
            if (user == null) return;

            activeUser.Funds = user.Funds;
            activeUser.ReservedFunds = user.ReservedFunds;
            activeUser.AccessLevel = user.AccessLevel;
            activeUser.Address = user.Address;
            activeUser.Country = user.Country;
            activeUser.Name = user.Name;
            activeUser.Surname = user.Surname;
            activeUser.Email = user.Email;
            activeUser.Shares = user.Shares ?? new ObservableCollection<IShare>();
            activeUser.DepositTransactions = user.DepositTransactions ?? new ObservableCollection<IDepositTransaction>();
            activeUser.WithdrawTransactions = user.WithdrawTransactions ?? new ObservableCollection<IWithdrawTransaction>();
            activeUser.BuyOrders = user.BuyOrders ?? new ObservableCollection<IBuyOrder>();
            activeUser.SellOrders = user.SellOrders ?? new ObservableCollection<ISellOrder>();
            activeUser.Portfolios = user.Portfolios ?? new ObservableCollection<IPortfolio>();
            activeUser.SalesActions = user.SalesActions ?? new ObservableCollection<ISalesAction>();
            activeUser.TrackedStocks = user.TrackedStocks ?? new ObservableCollection<ITrackedStock>();
        }

        public async Task<bool> AddUser(IUser user, string password)
        {
            return await persistenceService.AddUser(user, password);
        }

        public async Task<IUser> GetUserById(int id)
        {
            return await persistenceService.GetUserById(id);
        }

        public async Task<IUser> GetUserByEmail(string email)
        {
            return await persistenceService.GetUserByEmail(email);
        }

        public IUser GetActiveUser()
        {
            return this.activeUser;
        }

        public async Task<bool> ValidateActiveUser(string email, string password)
        {
            var user = await GetUserByEmail(email);
            if (user == null) return false;
            if (!InputValidationRules.ValidateUserPassword(password, user.Salt, user.HashedPassword)) return false;
            return user.ID == activeUser.ID;
        }

        public bool PersistUser(IUser user)
        {
            UpdateUser(user);
            return true;
        }

        public async Task<bool> LoginUser(string email, string password)
        {
            var user = await GetUserByEmail(email);
            if (user == null) return false;

            if (!InputValidationRules.ValidateUserPassword(password, user.Salt, user.HashedPassword)) return false;

            // force update
            SetActiveUser(user);

            UpdateActiveUser(user);
            userUpdateTimer.Start();

            return true;
        }

        public void SetAccessLevel(IUser user, AccessLevelType accessLevelType)
        {
            this.persistenceService.UpdateAccessLevel(user, accessLevelType);
            UserUpdateTimerTick(null, null); //force update
        }

        public void AddFunds(decimal amount, IUser user)
        {
            this.persistenceService.SetFunds(user.Funds + amount, user);
        }

        public void RemoveFunds(decimal amount, IUser user)
        {
            this.persistenceService.SetFunds(user.Funds - amount, user);
        }

        public void AddReservedFunds(decimal amount, IUser user)
        {
            this.persistenceService.SetReservedFunds(user.ReservedFunds + amount, user);
        }

        public void RemoveReservedFunds(decimal amount, IUser user)
        {
            this.persistenceService.SetReservedFunds(user.ReservedFunds - amount, user);
        }

        public async Task<bool> AddOrUpdateTrackedStock(ITrackedStock trackedStock)
        {
            return await this.persistenceService.AddOrUpdateTrackedStock(trackedStock);
        }

        public async Task<bool> DeleteTrackedStock(ITrackedStock trackedStock)
        {
            return await this.persistenceService.DeleteTrackedStock(trackedStock);
        }

        public async Task<bool> AddDepositTransaction(IDepositTransaction depositTransaction)
        {
            return await this.persistenceService.AddDepositTransaction(depositTransaction);
        }

        public async Task<bool> AddWithdrawTransaction(IWithdrawTransaction withdrawTransaction)
        {
            return await this.persistenceService.AddWithdrawTransaction(withdrawTransaction);
        }

        public async void GenerateNewPasswordForUser(string email)
        {
            var user = await GetUserByEmail(email);
            if (user == null) return;

            var newPassword = await persistenceService.GenerateNewPassword(user);

            SimpleMailer.MailTo(email, "CapitalExchange Password Recovery!", "Hello, here is your new password: \n" + newPassword);
        }

        #region Private Methods

        private void UpdateUser(IUser user)
        {
            this.persistenceService.UpdateUserInformation(user);
        }

        private void SetActiveUser(IUser user)
        {
            this.activeUser = user as User;
        }

        #endregion
    }
}