using Prism.Mef.Modularity;
using Prism.Modularity;

namespace CapitalExchange.Services
{
    [ModuleExport(nameof(ServicesModule), typeof(ServicesModule))]
    public class ServicesModule : IModule
    {
        public void Initialize()
        {
            // Do nothing   
        }
    }
}