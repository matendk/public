using System.Collections.Generic;
using System.ComponentModel.Composition;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.ServiceInterfaces;

namespace CapitalExchange.Services
{
    [Export(typeof(IAccessLevelService))]
    public class AccessLevelService : IAccessLevelService
    {
        private readonly IPersistenceService persistenceService;

        [ImportingConstructor]
        public AccessLevelService(IPersistenceService persistenceService)
        {
            this.persistenceService = persistenceService;
        }

        public IEnumerable<IAccessLevel> GetAccessLevels()
        {
            return persistenceService.GetAccessLevels();
        }
    }
}