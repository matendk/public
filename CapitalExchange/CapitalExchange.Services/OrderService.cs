﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CapitalExchange.SharedTypes.Interfaces;
using System.ComponentModel.Composition;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using System.Timers;
using System.Threading.Tasks;
using System.Linq;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes;
using Prism.Mvvm;

namespace CapitalExchange.Services
{
    [Export(typeof(IOrderService))]
    public class OrderService : BindableBase, IOrderService
    {
        private readonly IPersistenceService persistenceService;
        private readonly IUserService userService;
        IEnumerable<IUser> usersWithAnyOrderList;
        IEnumerable<IBuyOrder> buyOrdersList;
        IEnumerable<ISellOrder> sellOrdersList;

        [ImportingConstructor]
        public OrderService(IPersistenceService persistenceService, IUserService userService)
        {
            this.persistenceService = persistenceService;
            this.userService = userService;

            UserTimer_Elapsed(null, null); // Get list quickly

            var userTimer = new Timer(5000);
            userTimer.Elapsed += UserTimer_Elapsed;
            userTimer.AutoReset = true;
            userTimer.Start();
        }

        private async void UserTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            UsersWithAnyOrderList = await persistenceService.GetAllUsersWithOrders();
            BuyOrdersList = GetBuyOrders();
            SellOrdersList = GetSellOrders();
            ConnectionStatus.DatabaseStatus = "Connected";
        }

        public async void PurchaseStockFromSellOrder(ISellOrder sellOrder, IUser user, int amountOfSharesToBuy)
        {
            var now = DateTime.Now;
            decimal price = 0;
            var lastBuyPrice = sellOrder.Shares[0].LastBuyPrice;

            for (int i = 0; i < amountOfSharesToBuy; i++)
            {
                var share = sellOrder.Shares[0];
                share.UserID = user.ID;
                sellOrder.Shares.Remove(share);
                price += sellOrder.Price;
                share.LastBuyPrice = sellOrder.Price;
                share.LastBuyDate = now;
                share.SellOrderID = null;
                sellOrder.Volume--;

                await persistenceService.UpdateShare(share);
                await UpdateSellOrder(sellOrder, user);
            }

            userService.AddFunds(price, UsersWithAnyOrderList.ToList().Find(u => u.ID == sellOrder.UserID));
            userService.RemoveFunds(price, user);
            
            // Create SalesAction
            await persistenceService.AddSalesAction(new SalesAction(amountOfSharesToBuy,
                sellOrder.Price, lastBuyPrice, now, sellOrder.UserID));

            if (sellOrder.Volume == 0)
            {
                await persistenceService.PostSellOrder(sellOrder);
            }
        }

        public async void SellStockToBuyOrder(IBuyOrder buyOrder, IUser user, int amountOfSharesToSell)
        {
            var sharesToSell = user.Shares.Where(s => s.StockSymbol == buyOrder.StockSymbol).Take(amountOfSharesToSell);
            var totalPrice = buyOrder.Price * amountOfSharesToSell;
            var oldSharePrice = sharesToSell.First().LastBuyPrice;
            var dateTimeNow = DateTime.Now;

            foreach (var share in sharesToSell)
            {
                share.LastBuyDate = dateTimeNow;
                share.LastBuyPrice = buyOrder.Price;
                share.UserID = buyOrder.UserID;
                buyOrder.Volume--;

                await persistenceService.UpdateShare(share);
                await UpdateBuyOrder(buyOrder);
            }

            userService.AddFunds(buyOrder.Price * amountOfSharesToSell, user);
            userService.RemoveReservedFunds(totalPrice, UsersWithAnyOrderList.ToList().Find(u => u.ID == buyOrder.UserID));

            // Create SalesAction
            await persistenceService.AddSalesAction(new SalesAction(amountOfSharesToSell,
                buyOrder.Price, oldSharePrice, dateTimeNow, user.ID));

            if (buyOrder.Volume == 0)
            {
                await persistenceService.PostBuyOrder(buyOrder);
            }
        }

        private IEnumerable<IBuyOrder> GetBuyOrders()
        {
            var returnlist = new List<IBuyOrder>();

            if (usersWithAnyOrderList == null) return returnlist;
            returnlist.AddRange(usersWithAnyOrderList.SelectMany(user => user.BuyOrders));

            return returnlist;
        }

        private IEnumerable<ISellOrder> GetSellOrders()
        {
            var returnlist = new List<ISellOrder>();
            if (usersWithAnyOrderList == null) return returnlist;
            returnlist.AddRange(usersWithAnyOrderList.SelectMany(user => user.SellOrders));

            return returnlist;
        }

        public async Task<bool> AddBuyOrder(IBuyOrder buyOrder)
        {
            return await persistenceService.AddBuyOrder(buyOrder);
        }

        public async Task<bool> UpdateBuyOrder(IBuyOrder buyOrder)
        {
            return await persistenceService.UpdateBuyOrder(buyOrder);
        }

        public async Task<bool> DeleteBuyOrder(IBuyOrder buyOrder)
        {
            return await persistenceService.DeleteBuyOrder(buyOrder);
        }

        public async Task<bool> AddSellOrder(ISellOrder sellOrder, IUser user)
        {
            var sharesToSell =
                user.Shares.Where(s => s.StockSymbol == sellOrder.StockSymbol && s.SellOrderID == null)
                    .Take(sellOrder.Volume);

            return await persistenceService.AddSellOrder(sellOrder, sharesToSell);
        }

        public async Task<bool> UpdateSellOrder(ISellOrder sellOrder, IUser user)
        {
            var oldShares = user.Shares.Where(s => s.SellOrderID == sellOrder.ID);
            ObservableCollection<IShare> sharesToRemove = null;
            ObservableCollection<IShare> sharesToAdd = null;

            if (oldShares.Count() > sellOrder.Volume)
            {
                sharesToRemove = new ObservableCollection<IShare>(user.Shares.Where(s => s.SellOrderID == sellOrder.ID).Take(oldShares.Count() - sellOrder.Volume).ToList());

                for (var i = 0; i < sharesToRemove.Count(); i++)
                {
                    sellOrder.Shares.RemoveAt(0);
                }
            }
            else if (oldShares.Count() < sellOrder.Volume)
            {
                sharesToAdd = new ObservableCollection<IShare>(user.Shares.Where(s => s.StockSymbol == sellOrder.StockSymbol && s.SellOrderID == null).Take(sellOrder.Volume - oldShares.Count()));
                sellOrder.Shares.AddRange(sharesToAdd);
            }

            return await persistenceService.UpdateSellOrder(sellOrder, sharesToRemove, sharesToAdd, user);
        }

        public async Task<bool> DeleteSellOrder(ISellOrder sellOrder)
        {
            return await persistenceService.DeleteSellOrder(sellOrder);
        }

        public IEnumerable<IUser> UsersWithAnyOrderList
        {
            get { return usersWithAnyOrderList; }
            set { SetProperty(ref usersWithAnyOrderList, value); }
        }
        public IEnumerable<IBuyOrder> BuyOrdersList
        {
            get { return buyOrdersList; }
            set { SetProperty(ref buyOrdersList, value); }
        }
        public IEnumerable<ISellOrder> SellOrdersList
        {
            get { return sellOrdersList; }
            set { SetProperty(ref sellOrdersList, value); }
        }
    }
}