﻿using System.ComponentModel.Composition;
using System.Windows;
using CapitalExchange.SharedTypes.ServiceInterfaces;

namespace CapitalExchange.Services
{
    [Export(typeof(IUserNotificationService))]
    public class UserNotificationService : IUserNotificationService
    {
        public void NotifyUser(string msg)
        {
            MessageBox.Show(msg);
        }
    }
}