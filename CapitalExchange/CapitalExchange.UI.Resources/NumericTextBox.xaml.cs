﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapitalExchange.UI.Resources
{
    /// <summary>
    /// Interaction logic for NumericTextBox.xaml
    /// </summary>
    public partial class NumericTextBox : UserControl
    {
        public NumericTextBox()
        {
            InitializeComponent();
        }

        private void UpArrow_Click(object sender, RoutedEventArgs e)
        {
            if (Text != "")
            {
                int i;
                if (int.TryParse(Text, out i))
                {
                    Text = Convert.ToString(i + 1);
                }
            }
        }

        private void DownArrow_Click(object sender, RoutedEventArgs e)
        {
            if (Text != "")
            {
                int i;
                if (int.TryParse(Text, out i))
                {
                    Text = Convert.ToString(i - 1);
                }
            }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(NumericTextBox),new FrameworkPropertyMetadata("0",OnTextChanged));

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            NumericTextBox t = d as NumericTextBox;
            t.Text = (string)e.NewValue;
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set
            {
                string newtext = value == null ? "0" : value;
                if (newtext.ToList().TrueForAll(c => char.IsNumber(c)))
                {
                    text.Text = newtext;
                    SetValue(TextProperty,newtext);
                }
                else
                {
                    string s = "";
                    foreach (var item in newtext.ToCharArray())
                    {
                        if (char.IsNumber(item))
                        {
                            s += item;
                        }
                    }
                    text.Text = s;
                    SetValue(TextProperty, s);
                }
                
            }
        }

        private void text_TextChanged(object sender, TextChangedEventArgs e)
        {
            Text = text.Text;
        }
    }
}