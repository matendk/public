﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.FundsAndStatistics.View;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using CapitalExchange.UI.Common;

namespace CapitalExchange.Modules.FundsAndStatistics
{
    [ModuleExport(nameof(FundsAndStatisticsModule), typeof(FundsAndStatisticsModule))]
    public class FundsAndStatisticsModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(MarketRegionNames.MarketFundsAndStatisticsRegion, typeof(FundsAndStatiscticsView));
            this.RegionManager.RegisterViewWithRegion(StockRegionNames.StockFundsAndStatisticsRegion, typeof(FundsAndStatiscticsView));
        }
    }
}