﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.FundsAndStatistics.ViewModel;

namespace CapitalExchange.Modules.FundsAndStatistics.View
{
    /// <summary>
    /// Interaction logic for FundsAndStatiscticsView.xaml
    /// </summary>
    [Export]
    public partial class FundsAndStatiscticsView : UserControl
    {
        [ImportingConstructor]
        public FundsAndStatiscticsView()
        {
            InitializeComponent();
        }

        [Import]
        public FundsAndStatisticsViewModel ViewModel
        {
            get
            {
                return (FundsAndStatisticsViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}