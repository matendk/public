﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Commands;
using CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.View;
using Microsoft.Practices.ServiceLocation;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Globalization;
using System.Threading.Tasks;

// ReSharper disable InconsistentNaming
namespace CapitalExchange.Modules.FundsAndStatistics.ViewModel
{
    [Export]
    public class FundsAndStatisticsViewModel : BindableBase
    {
        #region Constructor
        [ImportingConstructor]
        public FundsAndStatisticsViewModel(IServiceLocator serviceLocator, IUserService userService, IStockEngine stockEngine)
        {
            this.serviceLocator = ServiceLocator.Current;
            this.userService = userService;
            this.stockEngine = stockEngine;
            this.activeUser = userService.GetActiveUser();
            this.usCultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            this.usCultureInfo.NumberFormat.CurrencyNegativePattern = 1; //Allow negative numbers in the currency format

            stockEngine.RegisterForUpdate(UpdateAction);

            SetupPlotModel();
            SelectedGrapOption = GraphOptions.FirstOrDefault();
            PropertyConnect();
        }
        #endregion


        #region Privates
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IStockEngine stockEngine;
        private readonly IUser activeUser;
        private PropertyObserver<IUser> activeUserObserver;
        private readonly CultureInfo usCultureInfo;
        private readonly IList<DataPoint> dataPoints = new List<DataPoint>();
        private DateTime stockStartDate = DateTime.Now;
        private LineSeries LineSeries { get; set; }
        #endregion


        #region Properties
        public string Funds => activeUser.Funds.ToString("C", usCultureInfo);
        public string ReservedFunds => activeUser.ReservedFunds.ToString("C", usCultureInfo);
        public int ShareCount => activeUser.Shares.Count;
        public string ShareValueString => ShareValue.ToString("C", usCultureInfo);

        private decimal shareValue;
        public decimal ShareValue
        {
            get { return shareValue; }
            set { SetProperty(ref shareValue, value); }
        }

        public string Balance => (activeUser.Funds + activeUser.ReservedFunds + ShareValue).ToString("C", usCultureInfo);

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { SetProperty(ref plotModel, value); }
        }

        public List<string> GraphOptions { get; } = new List<string>() { "Earnings" };

        private string selectedGraphOption;
        public string SelectedGrapOption
        {
            get { return selectedGraphOption; }
            set { SetProperty(ref selectedGraphOption, value); }
        }
        #endregion


        #region Commands
        public ICommand ManageCommand => new DelegateCommand<UserControl>(MangeCommandExecute);
        private void MangeCommandExecute(UserControl view)
        {
            (new ManageFundsDialogView() { Owner = Window.GetWindow(view) }).ShowDialog();
        }
        #endregion


        #region Methods
        private void SetupPlotModel()
        {
            var tempModel = new PlotModel();

            var xAxis = new DateTimeAxis
            {
                Position = AxisPosition.Bottom,
                StringFormat = "dd/MM/yyyy",
                Title = "Date",
                AbsoluteMinimum = DateTimeAxis.ToDouble(userService.GetActiveUser().RegistrationDate),
                AbsoluteMaximum = DateTimeAxis.ToDouble(DateTime.Now),
                IsZoomEnabled = false,
                IsPanEnabled = false
            };

            var yAxis = new LinearAxis()
            {
                Position = AxisPosition.Left,
                Title = SelectedGrapOption,
                StartPosition = 0,
                MajorGridlineStyle = LineStyle.Automatic,
                MinorGridlineStyle = LineStyle.Automatic,
                IsZoomEnabled = false,
                IsPanEnabled = false
            };

            tempModel.Axes.Add(xAxis);
            tempModel.Axes.Add(yAxis);

            // Create a line series
            LineSeries = new LineSeries
            {
                Smooth = false,
                StrokeThickness = 1,
                MarkerSize = 1,
                DataFieldX = "Date",
                DataFieldY = "Balance",
                MarkerStroke = OxyColor.FromArgb(255, 70, 70, 70),
                MarkerFill = OxyColor.FromArgb(255, 70, 70, 70),
                MarkerType = MarkerType.Circle
            };

            tempModel.Series.Add(LineSeries);
            PlotModel = tempModel;
        }

        private void UpdateStock()
        {
            Task.Factory.StartNew(() =>
            {
                dataPoints.Clear();
                var totalEarnings = 0m;

                foreach (var salesAction in userService.GetActiveUser().SalesActions)
                {
                    totalEarnings += salesAction.Price - salesAction.PreviousPrice;
                    this.dataPoints.Add(DateTimeAxis.CreateDataPoint(salesAction.Date, Convert.ToDouble(totalEarnings))); ;
                }

                LineSeries.Points.Clear();
                LineSeries.Points.AddRange(dataPoints);
                PlotModel.Axes[0].AbsoluteMaximum = DateTimeAxis.ToDouble(DateTime.Now);

                PlotModel.InvalidatePlot(true);
                PlotModel.Axes[0].Reset();
                PlotModel.Axes[1].Reset();
            });
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);

            activeUserObserver.RegisterHandler(user => user.Shares, user => UpdateShares())
                .RegisterHandler(user => user.Funds, user => UpdateFunds())
                .RegisterHandler(user => user.ReservedFunds, user => UpdateFunds())
                .RegisterHandler(user => user.SalesActions, user => UpdateStock());
        }

        private void UpdateShares()
        {
            OnPropertyChanged(() => ShareValue);
            OnPropertyChanged(() => ShareCount);
            OnPropertyChanged(() => ShareValueString);
            OnPropertyChanged(() => Balance);
        }

        private void UpdateFunds()
        {
            OnPropertyChanged(() => Funds);
            OnPropertyChanged(() => ReservedFunds);
            OnPropertyChanged(() => Balance);
        }

        private void UpdateAction(IEnumerable<LiveStockQuote> liveStockQuotes)
        {
            ShareValue = 0;
            foreach (var share in activeUser.Shares)
            {
                var stock = liveStockQuotes.FirstOrDefault(s => s.Symbol.Equals(share.StockSymbol));
                if (stock == null)
                    continue;

                decimal lastTradPriceOnly;
                decimal.TryParse(stock.LastTradePriceOnly, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out lastTradPriceOnly);
                ShareValue += lastTradPriceOnly;
            }
        }

        #endregion
    }
}