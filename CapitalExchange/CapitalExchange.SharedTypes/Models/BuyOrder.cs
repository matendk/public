﻿using System;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class BuyOrder : ModelBase, IBuyOrder
    {
        #region Constructors
        public BuyOrder(DateTime expiratedDate, decimal price, int volume, string stockName, string stockSymbol, DateTime creationDate, int userId)
        {
            this.ExpirationDate = expiratedDate;
            this.Price = price;
            this.Volume = volume;
            this.StockSymbol = stockSymbol;
            this.StockName = stockName;
            this.CreationDate = creationDate;
            this.UserID = userId;
        }

        public BuyOrder(DateTime expiratedDate, decimal price, int volume, string stockName, string stockSymbol, DateTime creationDate, int userId, string ownerName)
        {
            this.ExpirationDate = expiratedDate;
            this.Price = price;
            this.Volume = volume;
            this.StockSymbol = stockSymbol;
            this.StockName = stockName;
            this.CreationDate = creationDate;
            this.UserID = userId;
            this.OwnerName = ownerName;
        }

        public BuyOrder(int id, DateTime expiratedDate, decimal price, int volume, string stockName, string stockSymbol, DateTime creationDate, int userId, string ownerName) : this(expiratedDate, price, volume, stockName, stockSymbol, creationDate, userId, ownerName)
        {
            this.ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private DateTime expirationDate;
        public DateTime ExpirationDate
        {
            get { return expirationDate; }
            set { SetProperty(ref expirationDate, value); }
        }

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }

        private int volume;
        public int Volume
        {
            get { return volume; }
            set { SetProperty(ref volume, value); }
        }

        private string stockSymbol;
        public string StockSymbol
        {
            get { return stockSymbol; }
            set { SetProperty(ref stockSymbol, value); }
        }

        private string name;
        public string StockName
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        public string NameAndSymbol => StockName + " (" + StockSymbol + ")";

        private DateTime creationDate;
        public DateTime CreationDate
        {
            get { return creationDate; }
            set { SetProperty(ref creationDate, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }

        private string ownerName;
        public string OwnerName
        {
            get { return ownerName; }
            set { SetProperty(ref ownerName, value); }
        }
        #endregion
    }
}