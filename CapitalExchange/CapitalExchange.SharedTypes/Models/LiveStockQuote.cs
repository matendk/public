﻿namespace CapitalExchange.SharedTypes.Models
{
    public class LiveStockQuote : ModelBase //, ILiveStockQuote
    {
        public string Name { get; set; }
        public string Symbol { get; set; }

        public string NameAndSymbol => Name + " (" + Symbol + ")";

        public string LastTradeDate { get; set; }
        public string Volume { get; set; }
        public string LastTradePriceOnly { get; set; }
        public string Change { get; set; }
        public string PercentChange { get; set; }
        public string PreviousClose { get; set; }

        public bool IsChangePositive
        {
            get
            {
                if (Change == null) return false;
                else return double.Parse(Change) > 0;
            }
        }

        private bool isTracked;
        public bool IsTracked
        {
            get { return isTracked; }
            set { SetProperty(ref isTracked, value); }
        }
        public bool IsShareOwned => true;
    }
}