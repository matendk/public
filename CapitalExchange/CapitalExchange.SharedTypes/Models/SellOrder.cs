﻿using System;
using System.Collections.ObjectModel;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class SellOrder : ModelBase, ISellOrder
    {
        #region Constructor
        public SellOrder(DateTime expiratedDate, decimal price, int volume, string stockName, string stockSymbol, DateTime creationDate, int userId)
        {
            this.ExpirationDate = expiratedDate;
            this.Price = price;
            this.Volume = volume;
            this.StockSymbol = stockSymbol;
            this.StockName = stockName;
            this.CreationDate = creationDate;
            this.UserID = userId;
        
            if(Shares == null)
                Shares = new ObservableCollection<IShare>();
        }

        public SellOrder(DateTime expiratedDate, decimal price, int volume, string stockName, string stockSymbol, DateTime creationDate, int userId, string ownerName)
        {
            this.ExpirationDate = expiratedDate;
            this.Price = price;
            this.Volume = volume;
            this.StockSymbol = stockSymbol;
            this.StockName = stockName;
            this.CreationDate = creationDate;
            this.UserID = userId;
            this.OwnerName = ownerName;

            if (Shares == null)
                Shares = new ObservableCollection<IShare>();
        }

        public SellOrder(int id, DateTime expiratedDate, decimal price, int volume, string stockName, string stockSymbol,
            DateTime creationDate, int userId, string ownerName, ObservableCollection<IShare> shareList) : this(expiratedDate, price, volume, stockName, stockSymbol, creationDate, userId, ownerName)
        {
            this.ID = id;
            Shares = shareList;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private DateTime expirationDate;
        public DateTime ExpirationDate
        {
            get { return expirationDate; }
            set { SetProperty(ref expirationDate, value); }
        }

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }

        private int volume;
        public int Volume
        {
            get { return volume; }
            set { SetProperty(ref volume, value); }
        }

        private string stockSymbol;
        public string StockSymbol
        {
            get { return stockSymbol; }
            set { SetProperty(ref stockSymbol, value); }
        }

        private string stockName;
        public string StockName
        {
            get { return stockName; }
            set { SetProperty(ref stockName, value); }
        }

        public string NameAndSymbol => StockName + " (" + StockSymbol +")";

        private DateTime creationDate;
        public DateTime CreationDate
        {
            get { return creationDate; }
            set { SetProperty(ref creationDate, value); }
        }

        private ObservableCollection<IShare> shares;
        public ObservableCollection<IShare> Shares
        {
            get { return shares; }
            set { SetProperty(ref shares, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }

        private string ownerName;
        public string OwnerName
        {
            get { return ownerName; }
            set { SetProperty(ref ownerName, value); }
        }
        #endregion
    }
}