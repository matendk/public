﻿using System;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class DepositTransaction : ModelBase, IDepositTransaction
    {
        #region Constructors
        public DepositTransaction(decimal amount, DateTime date, int userID)
        {
            Amount = amount;
            Date = date;
            UserID = userID;
        }

        public DepositTransaction(int id, decimal amount, DateTime date, int userID) : this(amount, date, userID)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private decimal amount;
        public decimal Amount
        {
            get { return amount; }
            set { SetProperty(ref amount, value); }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { SetProperty(ref date, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }
        #endregion
    }
}