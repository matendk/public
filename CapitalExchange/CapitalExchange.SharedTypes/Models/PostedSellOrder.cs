﻿using System;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class PostedSellOrder : ModelBase, IPostedSellOrder
    {
        #region Constructors
        public PostedSellOrder(DateTime expirationDate, decimal price, int volume, string stockSymbol, DateTime creationDate, DateTime postedDate, int userID)
        {
            ExpirationDate = expirationDate;
            Price = price;
            Volume = volume;
            StockSymbol = stockSymbol;
            CreationDate = creationDate;
            PostedDate = postedDate;
            UserID = userID;
        }

        public PostedSellOrder(int id, DateTime expirationDate, decimal price, int volume, string stockSymbol, DateTime creationDate, DateTime postedDate, int userID) : this(expirationDate, price, volume, stockSymbol, creationDate, postedDate, userID)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private DateTime expirationDate;
        public DateTime ExpirationDate
        {
            get { return expirationDate; }
            set { SetProperty(ref expirationDate, value); }
        }

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }

        private int volume;
        public int Volume
        {
            get { return volume; }
            set { SetProperty(ref volume, value); }
        }

        private string stockSymbol;
        public string StockSymbol
        {
            get { return stockSymbol; }
            set { SetProperty(ref stockSymbol, value); }
        }


        private DateTime creationDate;
        public DateTime CreationDate
        {
            get { return creationDate; }
            set { SetProperty(ref creationDate, value); }
        }

        private DateTime postedDate;
        public DateTime PostedDate
        {
            get { return postedDate; }
            set { SetProperty(ref postedDate, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }
        #endregion
    }
}
