﻿using System.Collections.ObjectModel;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class Portfolio : ModelBase, IPortfolio
    {
        #region Constructors
        public Portfolio(string name, ObservableCollection<IShare> shares, int userID)
        {
            Name = name;
            Shares = shares;
            UserID = userID;
        }

        public Portfolio(int id, string name, ObservableCollection<IShare> shares, int userID) : this(name, shares, userID)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        private ObservableCollection<IShare> shares;
        public ObservableCollection<IShare> Shares
        {
            get { return shares; }
            set { SetProperty(ref shares, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }
        #endregion
    }
}