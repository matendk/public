﻿using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class TrackedStock : ModelBase, ITrackedStock
    {
        #region Constructors
        public TrackedStock(string symbol, int userID)
        {
            Symbol = symbol;
            UserID = userID;
        }

        public TrackedStock(int id, string symbol, int userID) : this(symbol, userID)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private string symbol;
        public string Symbol
        {
            get { return symbol; }
            set { SetProperty(ref symbol, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }
        #endregion
    }
}