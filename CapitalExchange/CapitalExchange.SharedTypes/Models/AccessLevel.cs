﻿using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class AccessLevel : ModelBase, IAccessLevel
    {
        private AccessLevelType accessLevelType;
        private decimal price;
        private int id;

        public AccessLevelType AccessLevelType
        {
            get { return accessLevelType; }
            set { SetProperty(ref accessLevelType, value); }
        }

        public decimal Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }

        public int Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }
    }

    public enum AccessLevelType
    {
        Basic = 1,
        Premium = 2
    }
}