﻿using System;
using System.Collections.ObjectModel;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class User : ModelBase, IUser
    {
        #region Constructors
        public User(string name, string surname, string address, string country, string email, DateTime registrationDate, IAccessLevel accessLevel)
        {
            Name = name;
            Surname = surname;
            Address = address;
            Country = country;
            Email = email;
            RegistrationDate = registrationDate;
            AccessLevel = accessLevel;
        }

        public User(string name, string surname, string address, string country, string email, DateTime registrationDate, string salt, string password, IAccessLevel accessLevel) : this(name, surname, address, country, email, registrationDate, accessLevel)
        {
            HashedPassword = password;
            Salt = salt;
        }

        public User(string name, string surname, string email, string address, string country, decimal funds, decimal reservedFunds, string salt, string password, DateTime registrationDate, IAccessLevel accessLevel, ObservableCollection<IShare> shares, ObservableCollection<ISellOrder> sellOrders, ObservableCollection<IBuyOrder> buyOrders, ObservableCollection<IPostedSellOrder> postedSellOrders, ObservableCollection<IPostedBuyOrder> postedBuyOrders, ObservableCollection<IPortfolio> portfolios, ObservableCollection<ISalesAction> salesActions, ObservableCollection<IDepositTransaction> depositTransactions, ObservableCollection<IWithdrawTransaction> withdrawTransactions, ObservableCollection<ITrackedStock> trackedStocks)
        {
            Name = name;
            Surname = surname;
            Email = email;
            Address = address;
            Country = country;
            Funds = funds;
            ReservedFunds = reservedFunds;
            Salt = salt;
            HashedPassword = password;
            RegistrationDate = registrationDate;
            AccessLevel = accessLevel;
            Shares = shares;
            SellOrders = sellOrders;
            BuyOrders = buyOrders;
            PostedSellOrders = postedSellOrders;
            PostedBuyOrders = postedBuyOrders;
            Portfolios = portfolios;
            SalesActions = salesActions;
            DepositTransactions = depositTransactions;
            WithdrawTransactions = withdrawTransactions;
            TrackedStocks = trackedStocks;
        }

        public User(int id, string name, string surname, string email, string address, string country, decimal funds, decimal reservedFunds, string salt, string password, DateTime registrationDate, IAccessLevel accessLevel, ObservableCollection<IShare> shares, ObservableCollection<ISellOrder> sellOrders, ObservableCollection<IBuyOrder> buyOrders, ObservableCollection<IPostedSellOrder> postedSellOrders, ObservableCollection<IPostedBuyOrder> postedBuyOrders, ObservableCollection<IPortfolio> portfolios, ObservableCollection<ISalesAction> salesActions, ObservableCollection<IDepositTransaction> depositTransactions, ObservableCollection<IWithdrawTransaction> withdrawTransactions, ObservableCollection<ITrackedStock> trackedStocks) : this(name, surname, email, address, country, funds, reservedFunds, salt, password, registrationDate, accessLevel, shares, sellOrders, buyOrders, postedSellOrders, postedBuyOrders, portfolios, salesActions, depositTransactions, withdrawTransactions, trackedStocks)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        private string surname;
        public string Surname
        {
            get { return surname; }
            set { SetProperty(ref surname, value); }
        }

        private string address;
        public string Address
        {
            get { return address; }
            set { SetProperty(ref address, value); }
        }

        private string country;
        public string Country
        {
            get { return country; }
            set { SetProperty(ref country, value); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { SetProperty(ref email, value); }
        }

        private DateTime registrationDate;
        public DateTime RegistrationDate
        {
            get { return registrationDate; }
            set { SetProperty(ref registrationDate, value); }
        }

        private decimal funds;
        public decimal Funds
        {
            get { return funds; }
            set
            {
                SetProperty(ref funds, value);
            }
        }

        private decimal reservedFunds;
        public decimal ReservedFunds
        {
            get { return reservedFunds; }
            set { SetProperty(ref reservedFunds, value); }
        }

        private IAccessLevel accessLevel;
        public IAccessLevel AccessLevel
        {
            get { return accessLevel; }
            set { SetProperty(ref accessLevel, value); }
        }

        private ObservableCollection<IShare> shares;
        public ObservableCollection<IShare> Shares
        {
            get { return shares; }
            set { SetProperty(ref shares, value); }
        }

        private ObservableCollection<ISellOrder> sellOrders;
        public ObservableCollection<ISellOrder> SellOrders
        {
            get { return sellOrders; }
            set { SetProperty(ref sellOrders, value); }
        }

        private ObservableCollection<IBuyOrder> buyOrders;
        public ObservableCollection<IBuyOrder> BuyOrders
        {
            get { return buyOrders; }
            set { SetProperty(ref buyOrders, value); }
        }

        private ObservableCollection<IPortfolio> portfolios;
        public ObservableCollection<IPortfolio> Portfolios
        {
            get { return portfolios; }
            set { SetProperty(ref portfolios, value); }
        }

        private ObservableCollection<IWithdrawTransaction> withdrawTransactions;
        public ObservableCollection<IWithdrawTransaction> WithdrawTransactions
        {
            get { return withdrawTransactions; }
            set { SetProperty(ref withdrawTransactions, value); }
        }

        private ObservableCollection<IDepositTransaction> depositTransactions;
        public ObservableCollection<IDepositTransaction> DepositTransactions
        {
            get { return depositTransactions; }
            set { SetProperty(ref depositTransactions, value); }
        }

        private ObservableCollection<ITrackedStock> trackedStocks;
        public ObservableCollection<ITrackedStock> TrackedStocks
        {
            get { return trackedStocks; }
            set { SetProperty(ref trackedStocks, value); }
        }

        private ObservableCollection<IPostedSellOrder> postedSellOrders;
        public ObservableCollection<IPostedSellOrder> PostedSellOrders
        {
            get { return postedSellOrders; }
            set { SetProperty(ref postedSellOrders, value); }
        }

        private ObservableCollection<IPostedBuyOrder> postedBuyOrders;
        public ObservableCollection<IPostedBuyOrder> PostedBuyOrders
        {
            get { return postedBuyOrders; }
            set { SetProperty(ref postedBuyOrders, value); }
        }

        private ObservableCollection<ISalesAction> salesActions;
        public ObservableCollection<ISalesAction> SalesActions
        {
            get { return salesActions; }
            set { SetProperty(ref salesActions, value); }
        }

        private string hashedPassword;
        public string HashedPassword
        {
            get { return hashedPassword; }
            set { SetProperty(ref hashedPassword, value); }
        }

        private string salt;
        public string Salt
        {
            get { return salt; }
            set { SetProperty(ref salt, value); }
        }
        #endregion
    }
}