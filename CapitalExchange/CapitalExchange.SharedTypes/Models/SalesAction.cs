﻿using System;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class SalesAction : ModelBase, ISalesAction
    {
        #region Constructor
        public SalesAction(int volume, decimal price, decimal previousPrice, DateTime date, int userID)
        {
            Volume = volume;
            Price = price;
            PreviousPrice = previousPrice;
            Date = date;
            UserID = userID;
        }

        public SalesAction(int id, int volume, decimal price, decimal previousPrice, DateTime date, int userID) : this(volume, price, previousPrice, date, userID)
        {
            ID = id;
        }
        #endregion

        #region Properties

        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private int volume;
        public int Volume
        {
            get { return volume; }
            set { SetProperty(ref volume, value); }
        }

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }

        private decimal previousPrice;
        public decimal PreviousPrice
        {
            get { return previousPrice; }
            set { SetProperty(ref previousPrice, value); }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { SetProperty(ref date, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }
        #endregion
    }
}
