﻿using System;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class WithdrawTransaction : ModelBase, IWithdrawTransaction
    {
        #region Constructors
        public WithdrawTransaction(decimal amount, DateTime date, int userID)
        {
            Amount = amount;
            Date = date;
            UserID = userID;
        }

        public WithdrawTransaction(int id, decimal amount, DateTime date, int userID) : this(amount, date, userID)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private decimal amount;
        public decimal Amount
        {
            get { return amount; }
            set { SetProperty(ref amount, value); }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { SetProperty(ref date, value); }
        }

        private int userID;
        public int UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }
        #endregion
    }
}