﻿using System;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class Share : ModelBase, IShare
    {
        #region Constructors
        public Share(string name, string stockSymbol, decimal lastBuyprice, DateTime lastBuyDate, int? sellOrderID, int? userID, int? portfolioID)
        {
            Name = name;
            StockSymbol = stockSymbol;
            LastBuyPrice = lastBuyprice;
            LastBuyDate = lastBuyDate;
            SellOrderID = sellOrderID;
            UserID = userID;
            PortfolioID = portfolioID;
        }

        public Share(int id, string name, string stockSymbol, decimal lastBuyprice, DateTime lastBuyDate, int? sellOrderID, int? userID, int? portfolioID) : this(name, stockSymbol, lastBuyprice, lastBuyDate, sellOrderID, userID, portfolioID)
        {
            ID = id;
        }
        #endregion

        #region Properties
        private int id;
        public int ID
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        private string stockSymbol;
        public string StockSymbol
        {
            get { return stockSymbol; }
            set { SetProperty(ref stockSymbol, value); }
        }

        private decimal lastBuyPrice;
        public decimal LastBuyPrice
        {
            get { return lastBuyPrice; }
            set { SetProperty(ref lastBuyPrice, value); }
        }

        private DateTime lastBuyDate;
        public DateTime LastBuyDate
        {
            get { return lastBuyDate; }
            set { SetProperty(ref lastBuyDate, value); }
        }

        private int? sellOrderID;
        public int? SellOrderID
        {
            get { return sellOrderID; }
            set { SetProperty(ref sellOrderID, value); }
        }

        private int? userID;
        public int? UserID
        {
            get { return userID; }
            set { SetProperty(ref userID, value); }
        }

        private int? portfolioID;
        public int? PortfolioID
        {
            get { return portfolioID; }
            set { SetProperty(ref portfolioID, value); }
        }
        #endregion
    }
}