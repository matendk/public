﻿using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.Models
{
    public class OwnShare : ModelBase, IOwnShare
    {
        #region Constructors
        public OwnShare()
        {

        }

        public OwnShare(string name, string volume, string buyPrice, string price, string shareValue)
        {
            Name = name;
            Volume = volume;
            BuyPrice = buyPrice;
            Price = price;
            ShareValue = shareValue;
        }
        #endregion

        #region Properties
        private string name;
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }

        private string volume;
        public string Volume
        {
            get { return volume; }
            set { SetProperty(ref volume, value); }
        }

        private string buyPrice;
        public string BuyPrice
        {
            get { return buyPrice; }
            set { SetProperty(ref buyPrice, value); }
        }

        private string price;
        public string Price
        {
            get { return price; }
            set { SetProperty(ref price, value); }
        }

        private string shareValue;
        public string ShareValue
        {
            get { return shareValue; }
            set { SetProperty(ref shareValue, value); }
        }
        #endregion
    }
}