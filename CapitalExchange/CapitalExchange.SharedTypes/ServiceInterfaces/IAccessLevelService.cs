﻿using System.Collections.Generic;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.ServiceInterfaces
{
    public interface IAccessLevelService
    {
        /// <summary>
        /// Gets the access levels.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IAccessLevel> GetAccessLevels();
    }
}
