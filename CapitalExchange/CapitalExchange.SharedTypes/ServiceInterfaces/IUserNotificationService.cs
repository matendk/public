﻿namespace CapitalExchange.SharedTypes.ServiceInterfaces
{
    public interface IUserNotificationService
    {
        /// <summary>
        /// Notifies the user.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        void NotifyUser(string msg);
    }
}
