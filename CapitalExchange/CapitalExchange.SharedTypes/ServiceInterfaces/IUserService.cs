﻿using System.Threading.Tasks;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;

namespace CapitalExchange.SharedTypes.ServiceInterfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        Task<bool> AddUser(IUser user, string password);
        /// <summary>
        /// Gets the user by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        Task<IUser> GetUserByEmail(string email);
        /// <summary>
        /// Gets the active user.
        /// </summary>
        /// <returns></returns>
        IUser GetActiveUser();
        /// <summary>
        /// Persists the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        bool PersistUser(IUser user);
        /// <summary>
        /// Logins the user.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        Task<bool> LoginUser(string email, string password);
        /// <summary>
        /// Sets the access level.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="accessLevelType">Type of the access level.</param>
        void SetAccessLevel(IUser user, AccessLevelType accessLevelType);
        /// <summary>
        /// Adds the funds.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="user">The user.</param>
        void AddFunds(decimal amount, IUser user);
        /// <summary>
        /// Removes the funds.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="user">The user.</param>
        void RemoveFunds(decimal amount, IUser user);
        /// <summary>
        /// Adds the reserved funds.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="user">The user.</param>
        void AddReservedFunds(decimal amount, IUser user);
        /// <summary>
        /// Removes the reserved funds.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="user">The user.</param>
        void RemoveReservedFunds(decimal amount, IUser user);
        /// <summary>
        /// Adds a tracked stock to the users trackedstocks or updates it, if it already exists.
        /// </summary>
        /// <param name="trackedStock"></param>
        /// <returns></returns>
        Task<bool> AddOrUpdateTrackedStock(ITrackedStock trackedStock);
        /// <summary>
        /// Deletes a tracked stock from the users trackedstocks.
        /// </summary>
        /// <param name="trackedStock"></param>
        /// <returns></returns>
        Task<bool> DeleteTrackedStock(ITrackedStock trackedStock);
        /// <summary>
        /// Adds a deposit-transaction to the users deposittransactions.
        /// </summary>
        /// <param name="depositTransaction"></param>
        /// <returns></returns>
        Task<bool> AddDepositTransaction(IDepositTransaction depositTransaction);
        /// <summary>
        /// Adds a withdraw-transaction to the users withdrawtransactions.
        /// </summary>
        /// <param name="withdrawTransaction"></param>
        /// <returns></returns>
        Task<bool> AddWithdrawTransaction(IWithdrawTransaction withdrawTransaction);
        /// <summary>
        /// Generates the new password for user.
        /// </summary>
        /// <param name="email">The email.</param>
        void GenerateNewPasswordForUser(string email);
        /// <summary>
        /// Validates the active user.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        Task<bool> ValidateActiveUser(string email, string password);
    }
}