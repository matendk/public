﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;

namespace CapitalExchange.SharedTypes.ServiceInterfaces
{
    public interface IPersistenceService
    {
        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        Task<bool> AddUser(IUser user, string password);
        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<IUser> GetUserById(int id);
        /// <summary>
        /// Gets the user by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        Task<IUser> GetUserByEmail(string email);
        /// <summary>
        /// Gets all users with orders.
        /// </summary>
        /// <returns></returns>
        Task<List<IUser>> GetAllUsersWithOrders();
        /// <summary>
        /// Updates the user information.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        Task<bool> UpdateUserInformation(IUser user);
        /// <summary>
        /// Updates the access level.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="accessLevelType">Type of the access level.</param>
        void UpdateAccessLevel(IUser user, AccessLevelType accessLevelType);
        /// <summary>
        /// Gets the access levels.
        /// </summary>
        /// <returns></returns>
        IList<IAccessLevel> GetAccessLevels();
        /// <summary>
        /// Sets the funds.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="activeUser">The active user.</param>
        void SetFunds(decimal amount, IUser activeUser);
        /// <summary>
        /// Sets the reserved funds.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="activeUser">The active user.</param>
        void SetReservedFunds(decimal amount, IUser activeUser);

        /// <summary>
        /// Adds the share.
        /// </summary>
        /// <param name="share">The share.</param>
        /// <returns></returns>
        Task<bool> AddShare(IShare share);
        /// <summary>
        /// Updates the share.
        /// </summary>
        /// <param name="share">The share.</param>
        /// <returns></returns>
        Task<bool> UpdateShare(IShare share);
        /// <summary>
        /// Deletes the share.
        /// </summary>
        /// <param name="share">The share.</param>
        /// <returns></returns>
        Task<bool> DeleteShare(IShare share);

        /// <summary>
        /// Adds the buy order.
        /// </summary>
        /// <param name="buyOrder">The buy order.</param>
        /// <returns></returns>
        Task<bool> AddBuyOrder(IBuyOrder buyOrder);
        /// <summary>
        /// Updates the buy order.
        /// </summary>
        /// <param name="buyOrder">The buy order.</param>
        /// <returns></returns>
        Task<bool> UpdateBuyOrder(IBuyOrder buyOrder);
        /// <summary>
        /// Deletes the buy order.
        /// </summary>
        /// <param name="buyOrder">The buy order.</param>
        /// <returns></returns>
        Task<bool> DeleteBuyOrder(IBuyOrder buyOrder);
        /// <summary>
        /// Posts the buy order.
        /// </summary>
        /// <param name="buyorder">The buyorder.</param>
        /// <returns></returns>
        Task<bool> PostBuyOrder(IBuyOrder buyorder);

        /// <summary>
        /// Adds the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <param name="sharesToSell">The shares to sell.</param>
        /// <returns></returns>
        Task<bool> AddSellOrder(ISellOrder sellOrder, IEnumerable<IShare> sharesToSell);

        /// <summary>
        /// Updates the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <param name="sharesToBeRemoved">The shares to be removed.</param>
        /// <param name="sharesToBeAdded">The shares to be added.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        Task<bool> UpdateSellOrder(ISellOrder sellOrder, ObservableCollection<IShare> sharesToBeRemoved,
            ObservableCollection<IShare> sharesToBeAdded, IUser user);
        /// <summary>
        /// Deletes the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <returns></returns>
        Task<bool> DeleteSellOrder(ISellOrder sellOrder);
        /// <summary>
        /// Posts the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <returns></returns>
        Task<bool> PostSellOrder(ISellOrder sellOrder);

        /// <summary>
        /// Adds the deposit transaction.
        /// </summary>
        /// <param name="depositTransaction">The deposit transaction.</param>
        /// <returns></returns>
        Task<bool> AddDepositTransaction(IDepositTransaction depositTransaction);
        /// <summary>
        /// Updates the deposit transaction.
        /// </summary>
        /// <param name="depositTransaction">The deposit transaction.</param>
        /// <returns></returns>
        Task<bool> UpdateDepositTransaction(IDepositTransaction depositTransaction);
        /// <summary>
        /// Deletes the deposit transaction.
        /// </summary>
        /// <param name="depositTransaction">The deposit transaction.</param>
        /// <returns></returns>
        Task<bool> DeleteDepositTransaction(IDepositTransaction depositTransaction);

        /// <summary>
        /// Adds the withdraw transaction.
        /// </summary>
        /// <param name="withdrawTransaction">The withdraw transaction.</param>
        /// <returns></returns>
        Task<bool> AddWithdrawTransaction(IWithdrawTransaction withdrawTransaction);
        /// <summary>
        /// Updates the withdraw transaction.
        /// </summary>
        /// <param name="withdrawTransaction">The withdraw transaction.</param>
        /// <returns></returns>
        Task<bool> UpdateWithdrawTransaction(IWithdrawTransaction withdrawTransaction);
        /// <summary>
        /// Deletes the withdraw transaction.
        /// </summary>
        /// <param name="withdrawTransaction">The withdraw transaction.</param>
        /// <returns></returns>
        Task<bool> DeleteWithdrawTransaction(IWithdrawTransaction withdrawTransaction);

        /// <summary>
        /// Adds the portfolio.
        /// </summary>
        /// <param name="portfolio">The portfolio.</param>
        /// <returns></returns>
        Task<bool> AddPortfolio(IPortfolio portfolio);
        /// <summary>
        /// Updates the portfolio.
        /// </summary>
        /// <param name="portfolio">The portfolio.</param>
        /// <returns></returns>
        Task<bool> UpdatePortfolio(IPortfolio portfolio);
        /// <summary>
        /// Deletes the portfolio.
        /// </summary>
        /// <param name="portfolio">The portfolio.</param>
        /// <returns></returns>
        Task<bool> DeletePortfolio(IPortfolio portfolio);

        /// <summary>
        /// Adds the tracked stock.
        /// </summary>
        /// <param name="trackedStock">The tracked stock.</param>
        /// <returns></returns>
        Task<bool> AddTrackedStock(ITrackedStock trackedStock);
        /// <summary>
        /// Updates the tracked stock.
        /// </summary>
        /// <param name="trackedStock">The tracked stock.</param>
        /// <returns></returns>
        Task<bool> UpdateTrackedStock(ITrackedStock trackedStock);
        /// <summary>
        /// Deletes the tracked stock.
        /// </summary>
        /// <param name="trackedStock">The tracked stock.</param>
        /// <returns></returns>
        Task<bool> DeleteTrackedStock(ITrackedStock trackedStock);

        /// <summary>
        /// Adds the sales action.
        /// </summary>
        /// <param name="salesAction">The sales action.</param>
        /// <returns></returns>
        Task<bool> AddSalesAction(ISalesAction salesAction);
        /// <summary>
        /// Updates the sales action.
        /// </summary>
        /// <param name="salesAction">The sales action.</param>
        /// <returns></returns>
        Task<bool> UpdateSalesAction(ISalesAction salesAction);
        /// <summary>
        /// Deletes the sales action.
        /// </summary>
        /// <param name="salesAction">The sales action.</param>
        /// <returns></returns>
        Task<bool> DeleteSalesAction(ISalesAction salesAction);

        /// <summary>
        /// Generates the new password.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        Task<string> GenerateNewPassword(IUser user);
        /// <summary>
        /// Adds the or update tracked stock.
        /// </summary>
        /// <param name="trackedStock">The tracked stock.</param>
        /// <returns></returns>
        Task<bool> AddOrUpdateTrackedStock(ITrackedStock trackedStock);
    }
}