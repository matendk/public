﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;

namespace CapitalExchange.SharedTypes.ServiceInterfaces
{
    public interface IStockEngine
    {
        /// <summary>
        /// Registers for update.
        /// </summary>
        /// <param name="updateAction">The update action.</param>
        void RegisterForUpdate(Action<IEnumerable<LiveStockQuote>> updateAction);

        /// <summary>
        /// Unregisters for update.
        /// </summary>
        /// <param name="updateAction">The update action.</param>
        void UnregisterForUpdate(Action<IEnumerable<LiveStockQuote>> updateAction);

        /// <summary>
        /// Gets the live stock quotes.
        /// </summary>
        /// <param name="symbols">The symbols.</param>
        void GetLiveStockQuotes(IEnumerable<string> symbols);

        /// <summary>
        /// Gets the historical stock quotes.
        /// </summary>
        /// <param name="symbols">The symbols.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="stopDate">The stop date.</param>
        /// <returns></returns>
        Task<IList<IHistoricalStockQuote>> GetHistoricalStockQuotes(IEnumerable<string> symbols, DateTime startDate,DateTime stopDate);

        /// <summary>
        /// Gets or sets the stock quotes.
        /// </summary>
        /// <value>
        /// The stock quotes.
        /// </value>
        ConcurrentDictionary<string, LiveStockQuote> StockQuotes { get; set; }
    }
}