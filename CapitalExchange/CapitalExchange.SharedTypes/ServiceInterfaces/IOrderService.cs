﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.SharedTypes.ServiceInterfaces
{
    public interface IOrderService : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets the users with any order list.
        /// </summary>
        /// <value>
        /// The users with any order list.
        /// </value>
        IEnumerable<IUser> UsersWithAnyOrderList { get; }
        /// <summary>
        /// Gets the buy orders list.
        /// </summary>
        /// <value>
        /// The buy orders list.
        /// </value>
        IEnumerable<IBuyOrder> BuyOrdersList { get; }
        /// <summary>
        /// Gets the sell orders list.
        /// </summary>
        /// <value>
        /// The sell orders list.
        /// </value>
        IEnumerable<ISellOrder> SellOrdersList { get; }

        /// <summary>
        /// Adds the buy order.
        /// </summary>
        /// <param name="buyOrder">The buy order.</param>
        /// <returns></returns>
        Task<bool> AddBuyOrder(IBuyOrder buyOrder);
        /// <summary>
        /// Updates the buy order.
        /// </summary>
        /// <param name="buyOrder">The buy order.</param>
        /// <returns></returns>
        Task<bool> UpdateBuyOrder(IBuyOrder buyOrder);
        /// <summary>
        /// Deletes the buy order.
        /// </summary>
        /// <param name="buyOrder">The buy order.</param>
        /// <returns></returns>
        Task<bool> DeleteBuyOrder(IBuyOrder buyOrder);
        /// <summary>
        /// Adds the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        Task<bool> AddSellOrder(ISellOrder sellOrder, IUser user);
        /// <summary>
        /// Updates the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        Task<bool> UpdateSellOrder(ISellOrder sellOrder, IUser user);
        /// <summary>
        /// Deletes the sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <returns></returns>
        Task<bool> DeleteSellOrder(ISellOrder sellOrder);

        /// <summary>
        /// Purchases the stock from sell order.
        /// </summary>
        /// <param name="sellOrder">The sell order.</param>
        /// <param name="Buyer">The buyer.</param>
        /// <param name="amount">The amount.</param>
        void PurchaseStockFromSellOrder(ISellOrder sellOrder, IUser Buyer, int amount);
        /// <summary>
        /// Sells the stock to buy order.
        /// </summary>
        /// <param name="buyorder">The buyorder.</param>
        /// <param name="user">The user.</param>
        /// <param name="amount">The amount.</param>
        void SellStockToBuyOrder(IBuyOrder buyorder, IUser user, int amount);
    }
}
