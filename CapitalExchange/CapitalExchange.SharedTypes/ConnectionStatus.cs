﻿namespace CapitalExchange.SharedTypes
{
    public static class ConnectionStatus
    {
        /// <summary>
        /// Gets or sets the database status.
        /// </summary>
        /// <value>
        /// The database status.
        /// </value>
        public static string DatabaseStatus { get; set; }

        /// <summary>
        /// Gets or sets the stock status.
        /// </summary>
        /// <value>
        /// The stock status.
        /// </value>
        public static string StockStatus { get; set; }
    }
}