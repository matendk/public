﻿namespace CapitalExchange.SharedTypes
{
    public enum CreditCardType
    {
        Unavailable,
        Visa,
        MasterCard,
        AmericanExpress,
        Discover,
        Jcb
    }
}