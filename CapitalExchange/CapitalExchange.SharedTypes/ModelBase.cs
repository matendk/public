﻿using CapitalExchange.SharedTypes.Interfaces;
using Prism.Mvvm;

namespace CapitalExchange.SharedTypes
{
    public abstract class ModelBase : BindableBase, IModel
    {
        
    }
}