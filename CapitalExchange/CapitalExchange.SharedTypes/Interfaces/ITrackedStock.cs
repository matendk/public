﻿using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface ITrackedStock : INotifyPropertyChanged
    {
        int ID { get; }
        string Symbol { get; }
        int UserID { get; }
    }
}