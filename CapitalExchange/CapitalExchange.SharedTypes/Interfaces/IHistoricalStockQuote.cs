﻿namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IHistoricalStockQuote
    {
        string Symbol { get; }
        string Date { get; }
        string Open { get; }
        string High { get; }
        string Low { get; }
        string Close { get; }
        string Volume { get; }
        string Adj_Close { get; }
    }
}
