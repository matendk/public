﻿using System;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IWithdrawTransaction
    {
        int ID { get; }
        decimal Amount { get; }
        DateTime Date { get; }
        int UserID { get; }
    }
}