﻿using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IModel : INotifyPropertyChanged
    {
    }
}