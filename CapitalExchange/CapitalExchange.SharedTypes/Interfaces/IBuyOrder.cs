﻿using System;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IBuyOrder
    {
        int ID { get; }
        int UserID { get; }
        decimal Price { get; set; }
        int Volume { get; set; }
        string StockSymbol { get; }
        string StockName { get; }
        string NameAndSymbol { get; }
        string OwnerName { get; }
        DateTime CreationDate { get; }
        DateTime ExpirationDate { get; set; }
    }
}