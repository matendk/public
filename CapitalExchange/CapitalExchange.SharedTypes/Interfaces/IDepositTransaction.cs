﻿using System;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IDepositTransaction
    {
        int ID { get; }
        decimal Amount { get; }
        DateTime Date { get; }
        int UserID { get; }
    }
}