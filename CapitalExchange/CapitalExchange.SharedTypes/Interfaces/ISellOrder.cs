﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface ISellOrder : INotifyPropertyChanged
    {
        int ID { get; }
        int UserID { get; }
        DateTime CreationDate { get; }
        DateTime ExpirationDate { get; set; }
        ObservableCollection<IShare> Shares { get; }
        decimal Price { get; set; }
        int Volume { get; set; }
        string StockSymbol { get; }
        string StockName { get; }
        string NameAndSymbol { get; }
        string OwnerName { get; }
    }
}