﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IUser : INotifyPropertyChanged
    {
        int ID { get; }
        string Name { get; }
        string Address { get; }
        string Country { get; }
        string Email { get; }
        string Surname { get; }
        DateTime RegistrationDate { get; }
        decimal Funds { get; }
        decimal ReservedFunds { get; }
        IAccessLevel AccessLevel { get; }
        ObservableCollection<IShare> Shares { get; }
        ObservableCollection<ISellOrder> SellOrders { get; }
        ObservableCollection<IBuyOrder> BuyOrders { get; }
        ObservableCollection<IPortfolio> Portfolios { get; }
        ObservableCollection<IPostedSellOrder> PostedSellOrders { get; }
        ObservableCollection<IPostedBuyOrder> PostedBuyOrders { get; }
        ObservableCollection<IWithdrawTransaction> WithdrawTransactions { get; }
        ObservableCollection<IDepositTransaction> DepositTransactions { get; }
        ObservableCollection<ITrackedStock> TrackedStocks { get; }
        ObservableCollection<ISalesAction> SalesActions { get; }
        string HashedPassword { get; }
        string Salt { get; }
    }
}