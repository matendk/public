﻿using System;
using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface ISalesAction : INotifyPropertyChanged
    {
        int ID { get; }
        int Volume { get; }
        decimal Price { get; }
        decimal PreviousPrice { get; }
        DateTime Date { get; }
        int UserID { get; }
    }
}