﻿using CapitalExchange.SharedTypes.Models;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IAccessLevel
    {
        AccessLevelType AccessLevelType { get; }
        decimal Price { get; }
        int Id { get; }
    }
}
