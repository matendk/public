﻿using System;
using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IShare : INotifyPropertyChanged
    {
        int ID { get; }
        string Name { get; }
        string StockSymbol { get; }
        decimal LastBuyPrice { get; set; }
        DateTime LastBuyDate { get; set; }
        int? SellOrderID { get; set; }
        int? UserID { get; set; }
        int? PortfolioID { get;  }
    }
}