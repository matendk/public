﻿using System;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface ILiveStockQuote
    {
        string Name { get; }
        string Symbol { get; }
        DateTime Date { get; set; }
        double DailyVolume { get; }
        decimal Price { get; }
        decimal Change { get;}
        decimal ChangePercent { get; }
    }
}
