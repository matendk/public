﻿using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IOwnShare : INotifyPropertyChanged
    {
        string Name { get; }
        string Volume { get; }
        string BuyPrice { get; }
        string Price { get; }
        string ShareValue { get; }
    }
}