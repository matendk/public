﻿using System.Collections.ObjectModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IPortfolio
    {
        int ID { get; }
        string Name { get; }
        ObservableCollection<IShare> Shares { get; }
        int UserID { get; }
    }
}