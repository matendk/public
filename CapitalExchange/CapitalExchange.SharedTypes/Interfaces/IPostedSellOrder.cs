﻿using System;
using System.ComponentModel;

namespace CapitalExchange.SharedTypes.Interfaces
{
    public interface IPostedSellOrder : INotifyPropertyChanged
    {
        int ID { get; }
        DateTime ExpirationDate { get; }
        decimal Price { get; }
        int Volume { get; }
        string StockSymbol { get; }
        DateTime CreationDate { get; }
        DateTime PostedDate { get; }
        int UserID { get; }
    }
}