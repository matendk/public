﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.FundsAndStatistics.View;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;
using Prism.Regions;

namespace CapitalExchange.Modules.Market.ViewModel
{
    [Export]
    public class MarketViewModel : BindableBase, INavigationAware
    {
        private readonly IRegionManager regionManager;
        private readonly IServiceLocator serviceLocator;
        private readonly IStockEngine stockEngine;

        [ImportingConstructor]
        public MarketViewModel(IRegionManager regionManager, 
                               IServiceLocator serviceLocator,
                               IStockEngine stockEngine)
        {
            this.regionManager = regionManager;
            this.serviceLocator = serviceLocator;
            this.stockEngine = stockEngine;

        }

        #region INavigationAware

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            var fundsAndStatisticsView = serviceLocator.GetInstance<FundsAndStatiscticsView>();
            regionManager.Regions[MarketRegionNames.MarketFundsAndStatisticsRegion].Activate(fundsAndStatisticsView);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            // This view will handle all navigation view requests for MarketView, so we always return true.
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            var fundsAndStatisticsView = serviceLocator.GetInstance<FundsAndStatiscticsView>();
            regionManager.Regions[MarketRegionNames.MarketFundsAndStatisticsRegion].Deactivate(fundsAndStatisticsView);
        }

        #endregion
    }
}