﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using CapitalExchange.Modules.Market.View;
using CapitalExchange.UI.Common;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;

namespace CapitalExchange.Modules.Market.ViewModel
{
    [Export]
    public class MarketViewNavigationItemViewModel : BindableBase, IPartImportsSatisfiedNotification
    {
        #region Fields

        private readonly IRegionManager _regionManager;
        private bool _isMarketActive;

        #endregion

        [ImportingConstructor]
        public MarketViewNavigationItemViewModel(IRegionManager regionManager)
        {
            this._regionManager = regionManager;
            this.NavigateCommand = new DelegateCommand(Navigate);
        }

        void IPartImportsSatisfiedNotification.OnImportsSatisfied()
        {
            IsMarketActive = true; // This is our default view so we're setting this to Active on import
        }

        private void Navigate()
        {
            this._regionManager.RequestNavigate(RegionNames.MainRegion, new Uri(nameof(MarketView), UriKind.Relative));
        }

        #region Commands

        public ICommand NavigateCommand { get; }

        #endregion

        #region Properties

        public bool IsMarketActive
        {
            get { return _isMarketActive; }
            set { SetProperty(ref _isMarketActive, value); }
        }

        #endregion
    }
}