﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.Market.ViewModel;

namespace CapitalExchange.Modules.Market.View
{
    /// <summary>
    /// Interaction logic for MarketView.xaml
    /// </summary>
    [Export]
    public partial class MarketView : UserControl
    {
        public MarketView()
        {
            InitializeComponent();
        }

        [Import]
        public MarketViewModel ViewModel
        {
            get
            {
                return (MarketViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}