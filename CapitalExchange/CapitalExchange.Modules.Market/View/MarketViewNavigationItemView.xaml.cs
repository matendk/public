﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.Market.ViewModel;

namespace CapitalExchange.Modules.Market.View
{
    /// <summary>
    /// Interaction logic for MarketViewNavigationItem.xaml
    /// </summary>
    [Export]
    public partial class MarketViewNavigationItemView : UserControl
    {
        public MarketViewNavigationItemView()
        {
            InitializeComponent();
        }

        [Import]
        public MarketViewNavigationItemViewModel ViewModel
        {
            set { this.DataContext = value; }
        }
    }
}