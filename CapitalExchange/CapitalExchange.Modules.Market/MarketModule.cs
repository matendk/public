﻿using System;
using System.ComponentModel.Composition;
using CapitalExchange.Modules.FundsAndStatistics.View;
using CapitalExchange.Modules.Market.View;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;

namespace CapitalExchange.Modules.Market
{
    [ModuleExport(nameof(MarketModule), typeof(MarketModule))]
    [ModuleDependency("StockChartModule")]
    [ModuleDependency("MarketDataTableModule")]
    [ModuleDependency("FundsAndStatisticsModule")]
    public class MarketModule : IModule
    {
        [Import]
        public IRegionManager _regionManager;

        public void Initialize()
        {
            this._regionManager.RegisterViewWithRegion(RegionNames.NavigationBarRegion, typeof(MarketViewNavigationItemView));
            this._regionManager.RegisterViewWithRegion(RegionNames.MainRegion, typeof(MarketView)); // Set as start up view
        }
    }
}