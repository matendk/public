﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using Prism.Mvvm;
using Prism.Regions;

namespace CapitalExchange.Modules.Portfolio.ViewModel
{
    [Export]
    public class PortfolioViewModel : BindableBase, INavigationAware
    {
        private ObservableCollection<IPortfolioItem> portfolios;
        private readonly IStockEngine stockEngine;
        private readonly IUserService userService;
        private readonly IUser activeUser;
        private PropertyObserver<IUser> activeUserObserver;

        [ImportingConstructor]
        public PortfolioViewModel(IStockEngine stockEngine, IUserService userService)
        {
            this.stockEngine = stockEngine;
            this.userService = userService;
            this.activeUser = userService.GetActiveUser();
            PropertyConnect();

            PopulatePortfolios();
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);
            activeUserObserver.RegisterHandler(user => user.Shares, user => UpdateShares());
            activeUserObserver.RegisterHandler(user => user.Portfolios, user => UpdatePortfolios());
        }

        private void UpdateShares()
        {
        }

        private void UpdatePortfolios()
        {
            if(Portfolios == null) Portfolios = new ObservableCollection<IPortfolioItem>();
            Portfolios.Clear();

            foreach (var portfolio in activeUser.Portfolios)
            {
                Portfolios.Add(new PortfolioItem()
                {
                    ID = portfolio.ID,
                    Name = portfolio.Name,
                    UserID = portfolio.UserID
                });
            }

            OnPropertyChanged(() => Portfolios);
        }

        private void PopulatePortfolios()
        {
            var tempList = new List<IPortfolioItem>
            {
                new PortfolioItem()
                {

                }
            };


            portfolios = new ObservableCollection<IPortfolioItem>();

        }

        public ObservableCollection<IPortfolioItem> Portfolios
        {
            get { return portfolios; }
            set { SetProperty(ref portfolios, value); }
        }

        private void UpdateAction(IEnumerable<LiveStockQuote> liveStockQuotes)
        {
            //Shares = new ObservableCollection<IOwnShare>();

            //Group shares by stock symbol, last buy price, & last buy date
            var shareGroups = activeUser.Shares.GroupBy(share => new { share.StockSymbol, share.LastBuyPrice, share.LastBuyDate })
                .Select(g => g.ToList())
                .ToList();

            foreach (var shareGroup in shareGroups)
            {
                LiveStockQuote stock = liveStockQuotes.FirstOrDefault(e => e.Symbol == shareGroup.First().StockSymbol);
                if (stock == null) continue;

                //Setup new own share and calculate values by looping through share group
                string name = "";
                decimal lastBuyPrice = 0m;
                decimal price = 0m;
                decimal value = 0m;
                foreach (var share in shareGroup)
                {
                    name = share.Name + " (" + share.StockSymbol + ")";
                    lastBuyPrice = share.LastBuyPrice;

                    decimal tempPrice;
                    decimal.TryParse(stock.LastTradePriceOnly, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out tempPrice);
                    price = tempPrice;

                    decimal tempValue;
                    decimal.TryParse(stock.LastTradePriceOnly, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out tempValue);
                    value += tempValue;
                }

                //Add a new own share
                //Shares.Add(new OwnShare(name, lastBuyPrice.ToString("C", usCultureInfo), shareGroup.Count.ToString(), price.ToString("C", usCultureInfo), value.ToString("C", usCultureInfo)));
            }
        }

        #region INavigationAware 

        public void OnNavigatedTo(NavigationContext navigationContext)
        {
            //var fundsAndStatisticsView = _serviceLocator.GetInstance<FundsAndStatiscticsView>();
            //_regionManager.Regions[StockRegionNames.StockFundsAndStatisticsRegion].Activate(fundsAndStatisticsView);
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            // This view will handle all navigation view requests for StockView, so we always return true.
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //var fundsAndStatisticsView = _serviceLocator.GetInstance<FundsAndStatiscticsView>();
            //_regionManager.Regions[StockRegionNames.StockFundsAndStatisticsRegion].Deactivate(fundsAndStatisticsView);
        }

        #endregion
    }
}