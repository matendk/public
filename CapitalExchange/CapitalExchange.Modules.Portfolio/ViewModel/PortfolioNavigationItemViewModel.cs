﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;
using CapitalExchange.Modules.Portfolio.View;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using CapitalExchange.UI.Common.Dialogs.Account.View;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;

namespace CapitalExchange.Modules.Portfolio.ViewModel
{
    [Export]
    public class PortfolioNavigationItemViewModel : BindableBase, IPartImportsSatisfiedNotification
    {
        private readonly IRegionManager regionManager;
        private readonly IUserService userService;
        private bool isPortfolioActive;
        private IUserNotificationService userNotificationService;

        [ImportingConstructor]
        public PortfolioNavigationItemViewModel(IRegionManager regionManager, IUserService userService, IUserNotificationService userNotificationService)
        {
            this.regionManager = regionManager;
            this.userNotificationService = userNotificationService;
            this.userService = userService;
            this.NavigateCommand = new DelegateCommand(Navigate);
        }

        private void Navigate()
        {
            if (userService.GetActiveUser().AccessLevel.AccessLevelType == AccessLevelType.Premium)
            {
                this.regionManager.RequestNavigate(RegionNames.MainRegion, new Uri(nameof(PortFolioView), UriKind.Relative));
            }
            else
            {
                IsPortfolioActive = false;
                // User has no access, let them pay?
                userNotificationService.NotifyUser("You have no access to this Feature - Please pay to access.");
                var premiumPaymentDialogView = new PremiumPaymentDialogView();

                var result = premiumPaymentDialogView.ShowDialog();
                if (result != null && result.Value)
                {
                    userService.SetAccessLevel(ActiveUser, AccessLevelType.Premium);
                    userNotificationService.NotifyUser("Payment accepted.");
                }
            }
        }

        public void OnImportsSatisfied()
        {

        }

        public ICommand NavigateCommand { get; }

        #region Properties

        public bool IsPortfolioActive
        {
            get { return isPortfolioActive; }
            set { SetProperty(ref isPortfolioActive, value); }
        }

        public IUser ActiveUser => userService.GetActiveUser();

        #endregion

    }
}