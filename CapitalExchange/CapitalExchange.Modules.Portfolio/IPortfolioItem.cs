﻿using System.Collections.ObjectModel;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.Modules.Portfolio
{
    public interface IPortfolioItem : IPortfolio
    {
        decimal Value { get; }
        decimal MonthReturnMin { get; }
        decimal MonthReturnMax { get; }
        decimal YearReturnMin { get; }
        decimal YearReturnMax { get; }

        string MonthReturnString { get; }
        string YearReturnString { get; }

        ObservableCollection<IPortfolioShare> PortfolioShares { get; }
    }
}