﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.Portfolio.ViewModel;

namespace CapitalExchange.Modules.Portfolio.View
{
    /// <summary>
    /// Interaction logic for PortfolioNavigationItemView.xaml
    /// </summary>
    [Export]
    public partial class PortfolioNavigationItemView : UserControl
    {
        public PortfolioNavigationItemView()
        {
            InitializeComponent();
        }

        [Import]
        public PortfolioNavigationItemViewModel ViewModel
        {
            set { this.DataContext = value; }
        }
    }
}   