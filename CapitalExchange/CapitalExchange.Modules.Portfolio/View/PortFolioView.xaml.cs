﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.Portfolio.ViewModel;
using Prism.Regions;

namespace CapitalExchange.Modules.Portfolio.View
{
    /// <summary>
    /// Interaction logic for PortFolioView.xaml
    /// </summary>
    [Export(nameof(PortFolioView))]
    public partial class PortFolioView : UserControl
    {
        public PortFolioView()
        {
            InitializeComponent();
        }

        [Import]
        public IRegionManager regionManager;

        [Import]
        public PortfolioViewModel ViewModel
        {
            get
            {
                return (PortfolioViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}