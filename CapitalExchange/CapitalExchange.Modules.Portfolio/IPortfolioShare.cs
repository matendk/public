﻿using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.Modules.Portfolio
{
    public interface IPortfolioShare : IShare
    {
        double ReturnProcent { get; }
        double RiskProcent { get; }
    }
}