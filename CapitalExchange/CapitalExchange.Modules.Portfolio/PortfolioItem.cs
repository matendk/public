﻿using System;
using System.Collections.ObjectModel;
using CapitalExchange.SharedTypes.Interfaces;

namespace CapitalExchange.Modules.Portfolio
{
    public class PortfolioItem : IPortfolioItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ObservableCollection<IShare> Shares { get; set; }
        public int UserID { get; set; }

        public decimal Value { get; set; }
        public decimal MonthReturnMin { get; set; }
        public decimal MonthReturnMax { get; set; }
        public decimal YearReturnMin { get; set; }
        public decimal YearReturnMax { get; set; }

        public string MonthReturnString => Math.Round(MonthReturnMin) + " ~ " + Math.Round(MonthReturnMax);
        public string YearReturnString => Math.Round(YearReturnMin) + " ~ " + Math.Round(YearReturnMax);

        public ObservableCollection<IPortfolioShare> PortfolioShares { get; set; }
    }
}