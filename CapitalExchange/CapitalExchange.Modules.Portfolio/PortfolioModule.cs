﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.Portfolio.View;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;

namespace CapitalExchange.Modules.Portfolio
{
    [ModuleExport(nameof(PortfolioModule), typeof(PortfolioModule))]
    public class PortfolioModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(RegionNames.NavigationBarRegion, typeof(PortfolioNavigationItemView));
        }
    }
}