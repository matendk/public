﻿using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Portfolio.ViewModel
{
    public class ManagePortfolioSharesDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;

        public ManagePortfolioSharesDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
        }
    }
}