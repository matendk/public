﻿using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Portfolio.ViewModel
{
    public class AddPortfolioDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;

        public AddPortfolioDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
        }
    }
}
