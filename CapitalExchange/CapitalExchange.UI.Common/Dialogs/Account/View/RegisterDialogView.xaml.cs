﻿using System.ComponentModel.Composition;
using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Account.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class RegisterDialogView : Window
    {
        public RegisterDialogView()
        {
            InitializeComponent();
            ViewModel = new RegisterDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public RegisterDialogViewModel ViewModel
        {
            get
            {
                return (RegisterDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
