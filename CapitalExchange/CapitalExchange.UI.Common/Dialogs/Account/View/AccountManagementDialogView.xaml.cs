﻿using System.ComponentModel.Composition;
using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Account.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class AccountManagementDialogView : Window
    {
        public AccountManagementDialogView()
        {
            InitializeComponent();
            ViewModel = new AccountManagementDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public AccountManagementDialogViewModel ViewModel
        {
            get
            {
                return (AccountManagementDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
