﻿using System.ComponentModel.Composition;
using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Account.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class PremiumPaymentDialogView : Window
    {
        public PremiumPaymentDialogView()
        {
            InitializeComponent();
            ViewModel = new PremiumPaymentDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public PremiumPaymentDialogViewModel ViewModel
        {
            get
            {
                return (PremiumPaymentDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
