﻿using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Account.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class AccountRecoveryDialogView : Window
    {
        public AccountRecoveryDialogView()
        {
            InitializeComponent();
            ViewModel = new AccountRecoveryDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public AccountRecoveryDialogViewModel ViewModel
        {
            get
            {
                return (AccountRecoveryDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
