﻿using System.ComponentModel.Composition;
using System.Windows;
using CapitalExchange.UI.Common.Dialogs.Account.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Account.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginDialogView : Window
    {
        public LoginDialogView()
        {
            InitializeComponent();
            ViewModel = new LoginDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public LoginDialogViewModel ViewModel
        {
            get
            {
                return (LoginDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
