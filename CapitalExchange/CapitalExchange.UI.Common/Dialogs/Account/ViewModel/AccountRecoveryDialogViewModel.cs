﻿using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using System.Windows;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.ServiceInterfaces;

namespace CapitalExchange.UI.Common.Dialogs.Account.ViewModel
{
    public class AccountRecoveryDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        private string email;
        private bool isEmailValid;

        public AccountRecoveryDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();

            RecoverCommand = new DelegateCommand<Window>(Recover, CanRecover);
        }

        private bool CanRecover(Window arg)
        {
            return IsEmailValid;
        }

        private void Recover(Window view)
        {
            userService.GenerateNewPasswordForUser(Email);
            userNotificationService.NotifyUser("A new password has sent to this email if it exists.");
            view.DialogResult = true;
            view.Close();
        }

        public string Email
        {
            get { return email; }
            set
            {
                if (SetProperty(ref email, value))
                {
                    IsEmailValid = InputValidationRules.ValidateEmail(value);
                    RecoverCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DelegateCommand<Window> RecoverCommand { get; set; }

        public bool IsEmailValid
        {
            get { return isEmailValid; }
            set { SetProperty(ref isEmailValid, value); }
        }
    }
}