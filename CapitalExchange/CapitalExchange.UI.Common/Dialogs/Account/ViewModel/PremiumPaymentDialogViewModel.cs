﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using CapitalExchange.Frameworks.Helpers;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using System.Text.RegularExpressions;

namespace CapitalExchange.UI.Common.Dialogs.Account.ViewModel
{
    public class PremiumPaymentDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        private readonly IAccessLevelService accessLevelService;
        private string name;
        private string cardNumber;
        private bool isNameValid;
        private bool isCardNumberValid;
        private bool isExpireDateValid;
        private bool isCvvValid;
        private string cardValidation;
        private DateTime expirationDate = DateTime.Now;
        private bool isVisaType;
        private bool isMasterCardType;
        private bool isAmericanExpressType;
        private bool isDiscoverType;
        private bool isJcbType;

        public PremiumPaymentDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();
            accessLevelService = serviceLocator.GetInstance<IAccessLevelService>();

            PayCommand = new DelegateCommand<Window>(PayCommandExecute, IsAllInputValid);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);
        }

        private void CancelCommandExecute(Window view)
        {
            view.Close();
        }

        private void PayCommandExecute(Window view)
        {
            // TODO: We should take 100 dollars here :)
            view.DialogResult = true;
            view.Close();
        }

        private bool IsAllInputValid(Window arg)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Name) &&
                   IsCardNumberValid &&
                   IsExpireDateValid &&
                   IsCvvValid;
        }

        private void SetActiveCreditCardType(CreditCardType creditCardTypeInput)
        {
            IsAmericanExpressType =
                IsDiscoverType =
                    IsVisaType =
                        IsJcbType =
                            IsMasterCardType = false; // Set everything to false

            switch (creditCardTypeInput)
            {
                case CreditCardType.Unavailable:
                    break;
                case CreditCardType.Visa:
                    IsVisaType = true;
                    break;
                case CreditCardType.MasterCard:
                    IsMasterCardType = true;
                    break;
                case CreditCardType.AmericanExpress:
                    IsAmericanExpressType = true;
                    break;
                case CreditCardType.Discover:
                    IsDiscoverType = true;
                    break;
                case CreditCardType.Jcb:
                    IsJcbType = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(creditCardTypeInput), creditCardTypeInput, null);
            }
        }

        public DelegateCommand<Window> PayCommand { get; }
        public ICommand CancelCommand { get; }

        public bool IsRegisterEnabled { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                if (!SetProperty(ref name, value)) return;

                IsNameValid = InputValidationRules.StringNotEmptyWhiteSpaceOrNull(value);
                PayCommand.RaiseCanExecuteChanged();
            }
        }

        public string CardNumber
        {
            get { return cardNumber; }
            set
            {
                Regex regex = new Regex("^[0-9]+$");
                if (regex.IsMatch(value))
                {
                    SetProperty(ref cardNumber, value);
                }
                else
                {
                    string onlynumber = "";
                    value.ToList().ForEach(c => { if (char.IsNumber(c)) onlynumber += c; });
                    SetProperty(ref cardNumber, onlynumber);
                }

                IsCardNumberValid = InputValidationRules.ValidateCardNumber(value);
                SetActiveCreditCardType(CreditCardHelper.GetCreditCardType(value));
                PayCommand.RaiseCanExecuteChanged();
            }
        }

        public DateTime ExpirationDate
        {
            get { return expirationDate; }
            set
            {
                if (!SetProperty(ref expirationDate, value)) return;

                IsExpireDateValid = InputValidationRules.ValidateCreditCardExpirationDate(value);
                PayCommand.RaiseCanExecuteChanged();
            }
        }

        public string CardValidation
        {
            get { return cardValidation; }
            set
            {
                Regex regex = new Regex("^[0-9]+$");
                if (regex.IsMatch(value))
                {
                    SetProperty(ref cardValidation, value);
                }
                else
                {
                    string onlynumber = "";
                    value.ToList().ForEach(c => { if (char.IsNumber(c)) onlynumber += c; });
                    SetProperty(ref cardValidation, onlynumber);
                }
                IsCvvValid = InputValidationRules.ValidateCvvNumber(value);
                PayCommand.RaiseCanExecuteChanged();
            }
        }

        public string PremiumPrice
        {
            get
            {
                var accessLevel =
                    accessLevelService.GetAccessLevels().FirstOrDefault(x => x.AccessLevelType == AccessLevelType.Premium);

                if (accessLevel != null)
                    return "$" + accessLevel.Price.ToString("##.0");

                throw new ArgumentNullException(@"Could not get premium price.");
            }
        }

        public bool IsNameValid
        {
            get { return isNameValid; }
            set { SetProperty(ref isNameValid, value); }
        }

        public bool IsCardNumberValid
        {
            get { return isCardNumberValid; }
            set { SetProperty(ref isCardNumberValid, value); }
        }

        public bool IsExpireDateValid
        {
            get { return isExpireDateValid; }
            set { SetProperty(ref isExpireDateValid, value); }
        }

        public bool IsCvvValid
        {
            get { return isCvvValid; }
            set { SetProperty(ref isCvvValid, value); }
        }

        public bool IsVisaType
        {
            get { return isVisaType; }
            set { SetProperty(ref isVisaType, value); }
        }

        public bool IsMasterCardType
        {
            get { return isMasterCardType; }
            set { SetProperty(ref isMasterCardType, value); }
        }

        public bool IsAmericanExpressType
        {
            get { return isAmericanExpressType; }
            set { SetProperty(ref isAmericanExpressType, value); }
        }

        public bool IsDiscoverType
        {
            get { return isDiscoverType; }
            set { SetProperty(ref isDiscoverType, value); }
        }

        public bool IsJcbType
        {
            get { return isJcbType; }
            set { SetProperty(ref isJcbType, value); }
        }
    }
}