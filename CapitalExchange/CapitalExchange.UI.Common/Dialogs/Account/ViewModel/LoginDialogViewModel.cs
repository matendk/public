﻿using System;
using System.Windows;
using System.Windows.Input;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common.Dialogs.Account.View;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using System.Windows.Controls;

namespace CapitalExchange.UI.Common.Dialogs.Account.ViewModel
{
    public class LoginDialogViewModel : BindableBase
    {
        #region Constructors

        public LoginDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();

            //this.userService = serviceLocator.GetInstance<IUserService>();
            LoginCommand = new DelegateCommand<Window>(LoginCommandExecute, CanExecuteMethod);
            RegisterCommand = new DelegateCommand(RegisterCommandExecute);
            RecoverAccountCommand = new DelegateCommand(RecoverAccountCommandExecute);
            PasswordChangedCommand = new DelegateCommand<PasswordBox>(PasswordChangedCommandExecute);
        }

        #endregion


        #region Privates

        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        private bool isRegisterEnabled = true;

        #endregion


        #region Properties

        private string email;

        public string Email
        {
            get { return email; }
            set
            {
                if (SetProperty(ref email, value))
                {
                    LoginCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set
            {
                if (SetProperty(ref password, value))
                {
                    LoginCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public bool IsRegisterEnabled
        {
            get { return isRegisterEnabled; }
            set { SetProperty(ref isRegisterEnabled, value); }
        }


        #endregion


        #region Commands

        public DelegateCommand<Window> LoginCommand { get; }

        private async void LoginCommandExecute(Window view)
        {
            IsRegisterEnabled = false;
            view.Cursor = Cursors.Wait;
            try
            {
                if (await userService.LoginUser(Email, Password))
                {
                    view.DialogResult = true;
                    view.Close();
                }
                else
                    userNotificationService.NotifyUser("Unable to validate user.");
            }
            catch (Exception)
            {
                userNotificationService.NotifyUser("Capital Exchange was unable to connect to the database.");
                view.Cursor = Cursors.Arrow;
                IsRegisterEnabled = true;
            }
            view.Cursor = Cursors.Arrow;
            IsRegisterEnabled = true;
        }

        public DelegateCommand RegisterCommand { get; }
        private void RegisterCommandExecute()
        {
            var registerDialogView = new RegisterDialogView();
            var dialogResult = registerDialogView.ShowDialog();
        }

        public DelegateCommand RecoverAccountCommand { get; }
        private void RecoverAccountCommandExecute()
        {
            var recoverAccountView = new AccountRecoveryDialogView();
            var recoverAccountResult = recoverAccountView.ShowDialog();
        }

        public DelegateCommand<PasswordBox> PasswordChangedCommand { get; set; }
        private void PasswordChangedCommandExecute(PasswordBox passwordBox)
        {
            Password = passwordBox.Password;
        }
        #endregion


        #region Methods
        private bool CanExecuteMethod(Window window)
        {
            return InputValidationRules.ValidateEmail(Email) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Password) & InputValidationRules.ValidatePassword(Password);
        }
        #endregion
    }
}