﻿using System.Windows;
using System.Windows.Input;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Account.ViewModel
{
    public class AccountManagementDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private IUser iUser;
        private string name;
        private string surname;
        private string address;
        private string country;

        public AccountManagementDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            iUser = userService.GetActiveUser();

            SaveCommand = new DelegateCommand<Window>(SaveCommandExecute, IsAllInputValid);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);

            Name = iUser.Name;
            Email = iUser.Email;
            Address = iUser.Address;
            Country = iUser.Country;
            JoinedDate = iUser.RegistrationDate.ToShortDateString();
            Surname = iUser.Surname;
        }

        private void CancelCommandExecute(Window window)
        {
            window.Close();
        }

        private void SaveCommandExecute(Window window)
        {
            User user = (User) iUser;
            user.Address = Address;
            user.Name = Name;
            user.Surname = Surname;
            user.Country = Country;
            userService.PersistUser(user);
            serviceLocator.GetInstance<IUserNotificationService>().NotifyUser("Information Updated.");
            window.Close();
        }
         
        private bool IsAllInputValid(Window window)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Name) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Surname) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Address) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Country);
        }

        public DelegateCommand<Window> SaveCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public string JoinedDate { get; set; }
        public string Email { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                if (SetProperty(ref name, value))
                {
                    SaveCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                if (SetProperty(ref surname, value))
                {
                    SaveCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                if (SetProperty(ref address, value))
                {
                    SaveCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string Country
        {
            get { return country; }
            set
            {
                if (SetProperty(ref country, value))
                {
                    SaveCommand.RaiseCanExecuteChanged();
                }
            }
        }
    }
}