﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common.Dialogs.Account.View;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Account.ViewModel
{
    public class RegisterDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        private readonly AccessLevel accessLevel = new AccessLevel { AccessLevelType = AccessLevelType.Basic };

        private bool isBasicSelected = true;
        private bool isPremiumSelected;
        private string email;
        private string password;
        private string name;
        private string surname;
        private string address;
        private string country;
        private bool isEmailValid;
        private bool isPasswordValid;

        public RegisterDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();

            RegisterCommand = new DelegateCommand<Window>(RegisterCommandExecute, IsAllInputValid);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);
            PasswordChangedCommand = new DelegateCommand<PasswordBox>(PasswordChangedCommandExecute);
        }

        private void CancelCommandExecute(Window view)
        {
            view.Close();
        }

        private async void RegisterCommandExecute(Window view)
        {
            if (IsPremiumSelected)
            {
                view.Visibility = Visibility.Collapsed;

                var premiumPaymentDialog = new PremiumPaymentDialogView();
                var result = premiumPaymentDialog.ShowDialog();

                if (result == null || !result.Value)
                {
                    // User not paid
                    premiumPaymentDialog.Close();
                    view.Visibility = Visibility.Visible;
                    return;
                }

                // User paid
                premiumPaymentDialog.Close();
            }

             await userService.AddUser(new User(Name, Surname, Address, Country, Email, DateTime.Now, accessLevel), Password);

            // view.DialogResult = true;
            userNotificationService.NotifyUser("User Registration complete.\nYou can now log in!");

            view.Close();
        }

        public DelegateCommand<PasswordBox> PasswordChangedCommand { get; set; }
        private void PasswordChangedCommandExecute(PasswordBox passwordBox)
        {
            Password = passwordBox.Password;
        }

        private bool IsAllInputValid(Window arg)
        {
            return IsEmailValid &&
                   IsPasswordValid &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Name) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Surname) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Address) &&
                   InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Country);
        }

        public DelegateCommand<Window> RegisterCommand { get; }
        public ICommand CancelCommand { get; }

        public string Email
        {
            get { return email; }
            set
            {
                if (!SetProperty(ref email, value)) return;

                IsEmailValid = InputValidationRules.ValidateEmail(value);
                RegisterCommand.RaiseCanExecuteChanged();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                if (!SetProperty(ref password, value)) return;

                IsPasswordValid = InputValidationRules.ValidatePassword(value);
                RegisterCommand.RaiseCanExecuteChanged();
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (SetProperty(ref name, value))
                {
                    RegisterCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                if (SetProperty(ref surname, value))
                {
                    RegisterCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                if (SetProperty(ref address, value))
                {
                    RegisterCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public string Country
        {
            get { return country; }
            set
            {
                if (SetProperty(ref country, value))
                {
                    RegisterCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public bool IsBasicSelected
        {
            get { return isBasicSelected; }
            set
            {
                isBasicSelected = value;

                if (value)
                    accessLevel.AccessLevelType = AccessLevelType.Basic;
            }
        }

        public bool IsPremiumSelected
        {
            get { return isPremiumSelected; }
            set
            {
                isPremiumSelected = value;

                if (value)
                    accessLevel.AccessLevelType = AccessLevelType.Premium;
            }
        }

        public bool IsEmailValid
        {
            get { return isEmailValid; }
            set { SetProperty(ref isEmailValid, value); }
        }

        public bool IsPasswordValid
        {
            get { return isPasswordValid; }
            set { SetProperty(ref isPasswordValid, value); }
        }
    }
}