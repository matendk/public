﻿using System;
using System.Globalization;
using System.Windows;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.View;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;

// ReSharper disable InconsistentNaming
namespace CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.ViewModel
{
    public class WithdrawDialogViewModel : BindableBase
    {
        #region Constructor
        public WithdrawDialogViewModel()
        {
            var serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            activeUser = userService.GetActiveUser();
            USCultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            Amount = 1;
            PropertyConnect();

            WithdrawCommand = new DelegateCommand<Window>(WithdrawCommandExecute, CanExecuteMethod);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);
        }
        #endregion


        #region Privates
        private readonly IUserService userService;
        private readonly IUser activeUser;
        private PropertyObserver<IUser> activeUserObserver;
        #endregion


        #region Properties
        private CultureInfo usCultureInfo;
        public CultureInfo USCultureInfo
        {
            get { return usCultureInfo; }
            set { SetProperty(ref usCultureInfo, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (SetProperty(ref name, value))
                {
                    IsNameValid = InputValidationRules.StringNotEmptyWhiteSpaceOrNull(value);
                }
            }
        }

        private bool isNameValid;
        public bool IsNameValid
        {
            get { return isNameValid; }
            set
            {
                SetProperty(ref isNameValid, value);
                WithdrawCommand.RaiseCanExecuteChanged();
            }
        }

        private int account;
        public int Account
        {
            get { return account; }
            set { SetProperty(ref account, value); }
        }

        private string iban;
        public string IBAN
        {
            get { return iban; }
            set
            {
                SetProperty(ref iban, value);
                IsIBANValid = InputValidationRules.ValidateIBAN(value);
            }
        }

        private bool isIBANValid;
        public bool IsIBANValid
        {
            get { return isIBANValid; }
            set
            {
                SetProperty(ref isIBANValid, value);
                WithdrawCommand.RaiseCanExecuteChanged();
            }
        }

        private string bic;
        public string BIC
        {
            get { return bic; }
            set
            {
                SetProperty(ref bic, value);
                IsBICValid = InputValidationRules.ValidateBIC(value);
            }
        }

        private bool isBICValid;
        public bool IsBICValid
        {
            get { return isBICValid; }
            set
            {
                SetProperty(ref isBICValid, value);
                WithdrawCommand.RaiseCanExecuteChanged();
            }
        }

        private decimal amount;
        public decimal Amount
        {
            get { return amount; }
            set
            {
                if (value > MaxAmount)
                    SetProperty(ref amount, MaxAmount);
                else
                    SetProperty(ref amount, value);
            }
        }

        public decimal MaxAmount => activeUser.Funds;
        public string Funds => activeUser.Funds.ToString("C", usCultureInfo);
        #endregion


        #region Commands
        public DelegateCommand<Window> WithdrawCommand { get; set; }
        private void WithdrawCommandExecute(Window view)
        {
            var result = (new ConfirmIdentityDialogView() { Owner = view }).ShowDialog();
            if (result == true)
            {
                userService.RemoveFunds(Amount, activeUser);
                userService.AddWithdrawTransaction(new WithdrawTransaction(Amount, DateTime.Now, activeUser.ID));
                view.Close();
            }
        }

        public DelegateCommand<Window> CancelCommand { get; set; }
        private void CancelCommandExecute(Window view)
        {
            view.Close();
        }
        #endregion


        #region Methods
        public bool CanExecuteMethod(Window window)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Name) && IsIBANValid && IsIBANValid;
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);
            activeUserObserver.RegisterHandler(user => user.Funds, user => OnPropertyChanged(() => Funds));
        }
        #endregion
    }
}