﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.View;
using Microsoft.Practices.ServiceLocation;
using OxyPlot;
using OxyPlot.Series;
using Prism.Commands;
using Prism.Mvvm;

// ReSharper disable InconsistentNaming
namespace CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.ViewModel
{
    public class ManageFundsDialogViewModel : BindableBase
    {
        #region Constructor
        public ManageFundsDialogViewModel()
        {
            var serviceLocator = ServiceLocator.Current;
            this.stockEngine = serviceLocator.GetInstance<IStockEngine>();
            this.userService = serviceLocator.GetInstance<IUserService>();
            this.activeUser = userService.GetActiveUser();
            this.usCultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            this.usCultureInfo.NumberFormat.CurrencyNegativePattern = 1; //Allow negative numbers in the currency format

            PropertyConnect();
            stockEngine.RegisterForUpdate(UpdateAction);
            SetupPlotModel();

            DepositCommand = new DelegateCommand<Window>(DepositCommandExecute);
            WithdrawCommand = new DelegateCommand<Window>(WithdrawCommandExecute);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);
        }
        #endregion


        #region Privates
        private readonly IStockEngine stockEngine;
        private readonly IUserService userService;
        private readonly IUser activeUser;
        private readonly CultureInfo usCultureInfo;
        private PropertyObserver<IUser> activeUserObserver;
        #endregion


        #region Properties
        public string Funds => activeUser.Funds.ToString("C", usCultureInfo);
        public string ReservedFunds => activeUser.ReservedFunds.ToString("C", usCultureInfo);
        public string ShareValueString => ShareValue.ToString("C", usCultureInfo);

        private decimal shareValue;
        public decimal ShareValue
        {
            get { return shareValue; }
            set { SetProperty(ref shareValue, value); }
        }

        private PlotModel plotModel;
        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { SetProperty(ref plotModel, value); }
        }

        public string Balance => (activeUser.Funds + activeUser.ReservedFunds + ShareValue).ToString("C", usCultureInfo);
        #endregion


        #region Commands
        public ICommand DepositCommand { get; set; }
        private void DepositCommandExecute(Window view)
        {
            (new DepositDialogView() { Owner = view }).ShowDialog();
        }

        public ICommand WithdrawCommand { get; set; }
        private void WithdrawCommandExecute(Window view)
        {
            (new WithdrawDialogView() { Owner = view }).ShowDialog();
        }

        public ICommand CancelCommand { get; set; }
        private void CancelCommandExecute(Window view)
        {
            view.Close();
        }
        #endregion


        #region Methods
        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);

            activeUserObserver.RegisterHandler(user => user.Shares, user =>
            {
                OnPropertyChanged(() => ShareValue);
                OnPropertyChanged(() => Balance);
                OnPropertyChanged(() => ShareValueString);
                SetupPlotModel();
            })
            .RegisterHandler(user => user.Funds, user =>
            {
                OnPropertyChanged(() => Funds);
                OnPropertyChanged(() => ReservedFunds);
                OnPropertyChanged(() => Balance);
            })
            .RegisterHandler(user => user.ReservedFunds, user =>
            {
                OnPropertyChanged(() => Funds);
                OnPropertyChanged(() => ReservedFunds);
                OnPropertyChanged(() => Balance);
            });
        }

        private void SetupPlotModel()
        {
            var tempModel = new PlotModel();

            var pieSeries = new PieSeries()
            {
                StrokeThickness = 1,
                AngleSpan = 360,
                StartAngle = 0,
                InsideLabelPosition = 0.8
            };

            pieSeries.Slices.Add(new PieSlice("Funds", (double)activeUser.Funds) { IsExploded = false, Fill = OxyColors.LightGray });
            pieSeries.Slices.Add(new PieSlice("Reserved Funds", (double)activeUser.ReservedFunds) { IsExploded = false, Fill = OxyColors.LightSlateGray });
            pieSeries.Slices.Add(new PieSlice("Share Value", (double)ShareValue) { IsExploded = false, Fill = OxyColors.Gray });

            tempModel.Series.Add(pieSeries);
            PlotModel = tempModel;
            PlotModel.InvalidatePlot(false);
        }

        private void UpdateAction(IEnumerable<LiveStockQuote> liveStockQuotes)
        {
            ShareValue = 0;
            foreach (var share in activeUser.Shares)
            {
                var stock = liveStockQuotes.FirstOrDefault(s => s.Symbol.Equals(share.StockSymbol));
                if (stock == null)
                    continue;

                decimal lastTradPriceOnly;
                decimal.TryParse(stock.LastTradePriceOnly, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out lastTradPriceOnly);
                ShareValue += lastTradPriceOnly;
            }
        }
        #endregion
    }
}