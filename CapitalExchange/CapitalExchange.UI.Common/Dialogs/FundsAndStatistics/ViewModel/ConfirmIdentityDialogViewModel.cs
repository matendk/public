﻿using System.Windows;
using System.Windows.Controls;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;

// ReSharper disable InconsistentNaming
namespace CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.ViewModel
{
    public class ConfirmIdentityDialogViewModel : BindableBase
    {
        #region Constructor
        public ConfirmIdentityDialogViewModel()
        {
            var serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();

            ConfirmCommand = new DelegateCommand<Window>(ConfirmCommandExecute, CanExecuteMethod);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);
            PasswordChangedCommand = new DelegateCommand<PasswordBox>(PasswordChangedCommandExecute);
        }
        #endregion


        #region Properties
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        #endregion


        #region Properties
        private string password;
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); ConfirmCommand.RaiseCanExecuteChanged(); }
        }
        #endregion


        #region Commmands
        private bool CanExecuteMethod(Window view)
        {
            return InputValidationRules.ValidatePassword(password);
        }

        public DelegateCommand<Window> ConfirmCommand { get; set; }
        private async void ConfirmCommandExecute(Window view)
        {
            var user = userService.GetActiveUser();

            if (await userService.ValidateActiveUser(user.Email, password))
            {
                view.DialogResult = true;
                view.Close();
            }
            else
            {
                userNotificationService.NotifyUser("Password does not match."); //Make better msg, maybe add msg type posiblilty (error, info, msg)?
            }
        }

        public DelegateCommand<Window> CancelCommand { get; set; }
        private void CancelCommandExecute(Window view)
        {
            view.Close();
        }

        public DelegateCommand<PasswordBox> PasswordChangedCommand { get; set; }
        private void PasswordChangedCommandExecute(PasswordBox passwordBox)
        {
            Password = passwordBox.Password;
        }
        #endregion
    }
}