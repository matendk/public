﻿using System;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using System.Windows;
using CapitalExchange.Frameworks.Helpers;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.View;
using System.Globalization;

// ReSharper disable InconsistentNaming
namespace CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.ViewModel
{
    public class DepositDialogViewModel : BindableBase
    {

        #region Constructor
        public DepositDialogViewModel()
        {
            var serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            activeUser = userService.GetActiveUser();
            USCultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            Amount = 1;

            //Set commands
            DepositCommand = new DelegateCommand<Window>(DepositCommandExecute, CanExecuteMethod);
            CancelCommand = new DelegateCommand<Window>(CancelCommandExecute);
        }
        #endregion


        #region Privates
        private readonly IUserService userService;
        private readonly IUser activeUser;
        #endregion


        #region Properties
        private CultureInfo usCultureInfo;
        public CultureInfo USCultureInfo
        {
            get { return usCultureInfo; }
            set { SetProperty(ref usCultureInfo, value); }    
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (SetProperty(ref name, value))
                {

                    IsNameValid = InputValidationRules.StringNotEmptyWhiteSpaceOrNull(value);
                }
            }
        }

        private bool isNameValid;
        public bool IsNameValid
        {
            get { return isNameValid; }
            set
            {
                SetProperty(ref isNameValid, value);
                DepositCommand.RaiseCanExecuteChanged();
            }
        }

        private string cardNo;
        public string CardNo
        {
            get { return cardNo; }
            set { SetProperty(ref cardNo, value); SetActiveCreditCardType(CreditCardHelper.GetCreditCardType(value)); IsCardNoValid = InputValidationRules.ValidateCardNumber(value); }
        }

        private bool isCardNoValid;
        public bool IsCardNoValid
        {
            get { return isCardNoValid; }
            set { SetProperty(ref isCardNoValid, value); DepositCommand.RaiseCanExecuteChanged(); }
        }

        private DateTime expirationDate = DateTime.Now;
        public DateTime ExpirationDate
        {
            get { return expirationDate; }
            set { SetProperty(ref expirationDate, value); IsExpirationDateValid = InputValidationRules.ValidateCreditCardExpirationDate(value); }
        }

        private bool isExpirationDateValid;
        public bool IsExpirationDateValid
        {
            get { return isExpirationDateValid; }
            set { SetProperty(ref isExpirationDateValid, value); DepositCommand.RaiseCanExecuteChanged(); }
        }

        private string cvv;
        public string CVV
        {
            get { return cvv; }
            set { SetProperty(ref cvv, value); IsCVVValid = InputValidationRules.ValidateCvvNumber(value); }
        }

        private bool isCVVValid;
        public bool IsCVVValid
        {
            get { return isCVVValid; }
            set { SetProperty(ref isCVVValid, value); DepositCommand.RaiseCanExecuteChanged(); }
        }

        private decimal amount;
        public decimal Amount
        {
            get { return amount; }
            set { SetProperty(ref amount, value); }
        }

        private bool isVisaType;
        public bool IsVisaType
        {
            get { return isVisaType; }
            set { SetProperty(ref isVisaType, value); }
        }

        private bool isMasterCardType;
        public bool IsMasterCardType
        {
            get { return isMasterCardType; }
            set { SetProperty(ref isMasterCardType, value); }
        }

        private bool isAmericanExpressType;
        public bool IsAmericanExpressType
        {
            get { return isAmericanExpressType; }
            set { SetProperty(ref isAmericanExpressType, value); }
        }

        private bool isDiscoverType;
        public bool IsDiscoverType
        {
            get { return isDiscoverType; }
            set { SetProperty(ref isDiscoverType, value); }
        }

        private bool isJcbType;
        public bool IsJcbType
        {
            get { return isJcbType; }
            set { SetProperty(ref isJcbType, value); }
        }
        #endregion


        #region Commands
        public DelegateCommand<Window> DepositCommand { get; set; }
        private void DepositCommandExecute(Window view)
        {
            var dialog = new ConfirmIdentityDialogView() { Owner = view };
            var result = dialog.ShowDialog();
            if (result == true)
            {
                userService.AddFunds(Amount, activeUser);
                userService.AddDepositTransaction(new DepositTransaction(Amount, DateTime.Now, activeUser.ID));
                view.Close();
            }
        }

        public DelegateCommand<Window> CancelCommand { get; set; }
        private void CancelCommandExecute(Window view)
        {
            view.Close();
        }
        #endregion


        #region Methods
        public bool CanExecuteMethod(Window window)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(Name) &&
                                              IsCardNoValid &&
                                              IsExpirationDateValid &&
                                              IsCVVValid;
        }

  private void SetActiveCreditCardType(CreditCardType creditCardTypeInput)
        {
            IsAmericanExpressType = IsDiscoverType = IsVisaType = IsJcbType = IsMasterCardType = false; // Set everything to false

            switch (creditCardTypeInput)
            {
                case CreditCardType.Unavailable:
                    break;
                case CreditCardType.Visa:
                    IsVisaType = true;
                    break;
                case CreditCardType.MasterCard:
                    IsMasterCardType = true;
                    break;
                case CreditCardType.AmericanExpress:
                    IsAmericanExpressType = true;
                    break;
                case CreditCardType.Discover:
                    IsDiscoverType = true;
                    break;
                case CreditCardType.Jcb:
                    IsJcbType = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(creditCardTypeInput), creditCardTypeInput, null);
            }
        }
        #endregion
    }
}