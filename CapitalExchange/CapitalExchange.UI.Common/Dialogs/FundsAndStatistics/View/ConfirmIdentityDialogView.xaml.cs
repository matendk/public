﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.FundsAndStatistics.View
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ConfirmIdentityDialogView : Window
    {
        public ConfirmIdentityDialogView()
        {
            InitializeComponent();
            ViewModel = new ConfirmIdentityDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public ConfirmIdentityDialogViewModel ViewModel
        {
            get
            {
                return (ConfirmIdentityDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
