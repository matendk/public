﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.UI.Common.Dialogs.Orders.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Orders.View
{
    /// <summary>
    /// Interaction logic for CreateSellOrderDialogView.xaml
    /// </summary>
    public partial class EditSellOrderDialogView : Window
    {
        public EditSellOrderDialogView(ISellOrder selected)
        {
            InitializeComponent();
            ViewModel = new EditSellOrderDialogViewModel(selected); // No prism on dialog, so no viewmodel import
        }

        public EditSellOrderDialogViewModel ViewModel
        {
            get
            {
                return (EditSellOrderDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
