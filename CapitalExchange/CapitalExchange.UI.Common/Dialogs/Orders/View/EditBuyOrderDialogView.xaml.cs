﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.UI.Common.Dialogs.Orders.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Orders.View
{
    /// <summary>
    /// Interaction logic for CreateSellOrderDialogView.xaml
    /// </summary>
    public partial class EditBuyOrderDialogView : Window
    {
        public EditBuyOrderDialogView(IBuyOrder selected)
        {
            InitializeComponent();
            ViewModel = new EditBuyOrderDialogViewModel(selected); // No prism on dialog, so no viewmodel import
        }

        public EditBuyOrderDialogViewModel ViewModel
        {
            get
            {
                return (EditBuyOrderDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
