﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.UI.Common.Dialogs.Account.ViewModel;
using CapitalExchange.UI.Common.Dialogs.Orders.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Orders.View
{
    /// <summary>
    /// Interaction logic for CreateSellOrderDialogView.xaml
    /// </summary>
    public partial class CreateSellOrderDialogView : Window
    {
        public CreateSellOrderDialogView()
        {
            InitializeComponent();
            ViewModel = new CreateSellOrderDialogViewModel(); // No prism on dialog, so no viewmodel import
        }

        public CreateSellOrderDialogViewModel ViewModel
        {
            get
            {
                return (CreateSellOrderDialogViewModel)this.DataContext;
            }   
            set
            {
                this.DataContext = value;
            }
        }
    }
}
