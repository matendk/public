﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.UI.Common.Dialogs.Orders.ViewModel;

namespace CapitalExchange.UI.Common.Dialogs.Orders.View
{
    /// <summary>
    /// Interaction logic for CreateSellOrderDialogView.xaml
    /// </summary>
    public partial class SellStockDialogView : Window
    {

        public SellStockDialogView(IBuyOrder selected)
        {
            InitializeComponent();
            ViewModel = new SellStockDialogViewModel(selected); // No prism on dialog, so no viewmodel import
        }

        public SellStockDialogViewModel ViewModel
        {
            get
            {
                return (SellStockDialogViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
