﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using CapitalExchange.SharedTypes.Interfaces;
using Prism.Commands;
using System.Windows;
using System.Windows.Input;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Orders.ViewModel
{
    public class SellStockDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IOrderService orderService;
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        private IBuyOrder buyOrder;
        private readonly IUser activeUser;
        private PropertyObserver<IUser> activeUserObserver;
        private PropertyObserver<IOrderService> orderServicePropertyObserver;
        private int amountOfSharesToSell;
        private int amountOfOwnedShares;
        private int availableVolume;
        private decimal shareUnitPrice;
        private bool orderActive = true;

        public SellStockDialogViewModel(IBuyOrder buyOrder)
        {
            this.serviceLocator = ServiceLocator.Current;
            orderService = serviceLocator.GetInstance<IOrderService>();
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();
            ConfirmCommand = new DelegateCommand<Window>(Confirm, CanExecuteConfirmCommand);
            CancelCommand = new DelegateCommand<Window>(Cancel);
            activeUser = userService.GetActiveUser();

            this.buyOrder = buyOrder;
            StockName = buyOrder.NameAndSymbol;

            AskedVolume = buyOrder.Volume;
            ShareUnitPrice = buyOrder.Price;
            AmountOfOwnedShares = activeUser.Shares.Count(x => x.StockSymbol == buyOrder.StockSymbol);
            AmountOfSharesToSell = 1;

            PropertyConnect();
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);
            activeUserObserver.RegisterHandler(user => user.Shares, user => UpdateShares());

            orderServicePropertyObserver = new PropertyObserver<IOrderService>(orderService);
            orderServicePropertyObserver.RegisterHandler(orders => orderService.BuyOrdersList, orders => UpdateBuyOrder(orders.BuyOrdersList));
        }

        private void UpdateShares()
        {
            AmountOfOwnedShares = activeUser.Shares.Count(s => s.StockSymbol == buyOrder.StockSymbol);
        }

        private void UpdateBuyOrder(IEnumerable<IBuyOrder> buyOrdersList)
        {
            var newBuyOrder = buyOrdersList.FirstOrDefault(o => o.ID == buyOrder.ID);

            if (!orderActive)
                return;

            if (newBuyOrder == null && orderActive)
            {
                // Order has been deleted.
                orderActive = false;
                ConfirmCommand.RaiseCanExecuteChanged();
                userNotificationService.NotifyUser("This order has been removed.");
            }
            else if(newBuyOrder != null && orderActive)
            {
                buyOrder = newBuyOrder;
                AskedVolume = newBuyOrder.Volume;
                ShareUnitPrice = newBuyOrder.Price;
            }
        }

        private bool CanExecuteConfirmCommand(Window arg)
        {
            return AmountOfSharesToSell > 0 &&
                AmountOfSharesToSell <= MaxSellAmount
                && orderActive;
        }

        private void Cancel(Window view)
        {
            view.Close();
        }

        private void Confirm(Window view)
        {
            if (AmountOfSharesToSell > 0)
            {
                orderService.SellStockToBuyOrder(buyOrder, activeUser, amountOfSharesToSell);
                userNotificationService.NotifyUser("Shares have been sold.");
                view.Close();
            }
        }

        public string StockName { get; set; }

        public int AskedVolume
        {
            get { return availableVolume; }
            set
            {
                if (SetProperty(ref availableVolume, value))
                {
                    if (AmountOfSharesToSell > MaxSellAmount)
                        AmountOfSharesToSell = MaxSellAmount;

                    OnPropertyChanged(() => MaxSellAmount);
                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int MaxSellAmount => Math.Min(AskedVolume, AmountOfOwnedShares);

        public decimal ShareUnitPrice
        {
            get { return shareUnitPrice; }
            set
            {
                if (SetProperty(ref shareUnitPrice, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSharesToSell
        {
            get { return amountOfSharesToSell; }
            set
            {
                if (SetProperty(ref amountOfSharesToSell, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }
        public int AmountOfOwnedShares
        {
            get { return amountOfOwnedShares; }
            set
            {
                if (SetProperty(ref amountOfOwnedShares, value))
                {
                    if (AmountOfSharesToSell > MaxSellAmount)
                        AmountOfSharesToSell = MaxSellAmount;

                    OnPropertyChanged(() => MaxSellAmount);
                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal TotalPrice => ShareUnitPrice * AmountOfSharesToSell;

        public DelegateCommand<Window> ConfirmCommand { get; }
        public ICommand CancelCommand { get; }
    }
}