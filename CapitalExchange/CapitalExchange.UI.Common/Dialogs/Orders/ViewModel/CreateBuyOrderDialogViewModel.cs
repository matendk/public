﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using System.Windows.Input;
using Prism.Commands;
using System.Windows;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.Interfaces;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Orders.ViewModel
{
    public class CreateBuyOrderDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IStockEngine stockEngine;
        private readonly IUserService userService;
        private readonly IOrderService orderService;
        private readonly IUserNotificationService userNotificationService;
        private string selectedStock;
        private int amountOfSharesToBuy;
        private decimal shareUnitPrice;
        private List<string> stockQuoteNames;
        private DateTime expiresDate;
        private IUser activeUser;

        public CreateBuyOrderDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            orderService = serviceLocator.GetInstance<IOrderService>();
            stockEngine = serviceLocator.GetInstance<IStockEngine>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();

            activeUser = userService.GetActiveUser();

            SetQuoteNamesList(stockEngine.StockQuotes);
            stockEngine.RegisterForUpdate(UpdateAction);

            SubmitCommand = new DelegateCommand<Window>(Confirm, CanExecuteSubmitCommand);
            CancelCommand = new DelegateCommand<Window>(Cancel);
            ExpiresDate = DateTime.Today.AddDays(14);
        }

        private void SetQuoteNamesList(IEnumerable<LiveStockQuote> stockQuotes)
        {
            StockQuoteNames = stockQuotes.Select(item => item.Name + " (" + item.Symbol + ")").ToList();
        }

        private void SetQuoteNamesList(ConcurrentDictionary<string, LiveStockQuote> stockQuotes)
        {
            StockQuoteNames = stockQuotes.Select(item => item.Value.Name + " (" + item.Value.Symbol + ")").ToList();
        }

        private void UpdateAction(IEnumerable<LiveStockQuote> liveStockQuotes)
        {
            SetQuoteNamesList(liveStockQuotes);
        }

        private void Cancel(Window view)
        {
            view.Close();
        }

        private async void Confirm(Window view)
        {
            var stockSymbol = stockEngine.StockQuotes.FirstOrDefault(q => q.Value.NameAndSymbol == SelectedStock).Value.Symbol;
            var stockName = stockEngine.StockQuotes.FirstOrDefault(q => q.Value.NameAndSymbol == SelectedStock).Value.Name;

            userService.RemoveFunds(ShareUnitPrice * AmountOfSharesToBuy, activeUser);
            userService.AddReservedFunds(ShareUnitPrice * AmountOfSharesToBuy, activeUser);

            await orderService.AddBuyOrder(
                new BuyOrder(ExpiresDate,
                ShareUnitPrice,
                AmountOfSharesToBuy,
                stockName,
                stockSymbol,
                DateTime.Now,
                userService.GetActiveUser().ID));

            userNotificationService.NotifyUser("Your Buy order has been submitted.");

            view.Close();
        }

        private bool CanExecuteSubmitCommand(Window arg)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(SelectedStock) &&
                   AmountOfSharesToBuy > 0 &&
                   ShareUnitPrice > 0 &&
                   ExpiresDate > DateTime.Now.AddDays(1);
        }

        public string SelectedStock
        {
            get { return selectedStock; }
            set
            {
                if (SetProperty(ref selectedStock, value))
                {
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DateTime ExpiresDate
        {
            get { return expiresDate; }
            set
            {
                if (SetProperty(ref expiresDate, value))
                {
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public List<string> StockQuoteNames
        {
            get { return stockQuoteNames; }
            set
            {
                var selectedStockQuote = SelectedStock;
                if (!SetProperty(ref stockQuoteNames, value)) return;
                if (!InputValidationRules.StringNotEmptyWhiteSpaceOrNull(selectedStockQuote)) return;
                if (value.Contains(selectedStockQuote))
                    SelectedStock = selectedStockQuote;
            }
        }

        public decimal ShareUnitPrice
        {
            get { return shareUnitPrice; }
            set
            {
                if (SetProperty(ref shareUnitPrice, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSharesToBuy
        {
            get { return amountOfSharesToBuy; }
            set
            {
                if (SetProperty(ref amountOfSharesToBuy, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal TotalPrice => AmountOfSharesToBuy * ShareUnitPrice;

        public DelegateCommand<Window> SubmitCommand { get; }
        public ICommand CancelCommand { get; }
    }
}