﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using System.Windows.Input;
using Prism.Commands;
using System.Windows;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Orders.ViewModel
{
    public class BuyStockDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IOrderService orderService;
        private readonly IUserService userService;
        private readonly IUserNotificationService userNotificationService;
        private ISellOrder sellOrder;
        private readonly IUser activeUser;
        private PropertyObserver<IUser> activeUserObserver;
        private PropertyObserver<IOrderService> orderServicePropertyObserver;

        private int amountOfSharesToBuy = 1;
        private int availableVolume;
        private decimal shareUnitPrice;
        private bool orderActive = true;

        public BuyStockDialogViewModel(ISellOrder sellOrder)
        {
            this.serviceLocator = ServiceLocator.Current;
            orderService = serviceLocator.GetInstance<IOrderService>();
            userService = serviceLocator.GetInstance<IUserService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();
            ConfirmCommand = new DelegateCommand<Window>(Confirm, CanExecuteConfirmCommand);
            CancelCommand = new DelegateCommand<Window>(Cancel);
            activeUser = userService.GetActiveUser();

            this.sellOrder = sellOrder;
            StockName = sellOrder.NameAndSymbol;

            AvailableVolume = sellOrder.Volume;
            ShareUnitPrice = sellOrder.Price;
            PropertyConnect();
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);
            activeUserObserver.RegisterHandler(user => user.Funds, user => UpdateFunds());

            orderServicePropertyObserver = new PropertyObserver<IOrderService>(orderService);
            orderServicePropertyObserver.RegisterHandler(orders => orderService.SellOrdersList, orders => UpdateSellOrder(orders.SellOrdersList));
        }

        private void UpdateSellOrder(IEnumerable<ISellOrder> sellOrderList)
        {
            var newSellOrder = sellOrderList.FirstOrDefault(o => o.ID == sellOrder.ID);

            if (!orderActive)
                return;

            if (newSellOrder == null && orderActive)
            {
                // Order has been deleted.

                orderActive = false;
                ConfirmCommand.RaiseCanExecuteChanged();
                userNotificationService.NotifyUser("This order has been deleted.");
            }
            else
            {
                sellOrder = newSellOrder;
                AvailableVolume = newSellOrder.Volume;
                ShareUnitPrice = newSellOrder.Price;
            }
        }

        private void UpdateFunds()
        {
            OnPropertyChanged(() => AvailableFunds);
        }

        private bool CanExecuteConfirmCommand(Window arg)
        {
            return TotalPrice <= AvailableFunds
                && orderActive;
        }

        private void Cancel(Window view)
        {
            view.Close();
        }

        private void Confirm(Window view)
        {
            if (AmountOfSharesToBuy > 0)
            {
                orderService.PurchaseStockFromSellOrder(sellOrder, activeUser, amountOfSharesToBuy);
                userNotificationService.NotifyUser("Shares have been purchased.");
                view.Close();
            }
        }

        public string StockName { get; set; }

        public int AvailableVolume
        {
            get { return availableVolume; }
            set
            {
                if (SetProperty(ref availableVolume, value))
                {
                    if (AmountOfSharesToBuy > value)
                        AmountOfSharesToBuy = value;

                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal ShareUnitPrice
        {
            get { return shareUnitPrice; }
            set
            {
                if (SetProperty(ref shareUnitPrice, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSharesToBuy
        {
            get { return amountOfSharesToBuy; }
            set
            {
                if (SetProperty(ref amountOfSharesToBuy, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    ConfirmCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal TotalPrice => ShareUnitPrice * AmountOfSharesToBuy;

        public decimal AvailableFunds => activeUser.Funds;

        public DelegateCommand<Window> ConfirmCommand { get; }
        public ICommand CancelCommand { get; }
    }
}