﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using System.Windows;
using System.Windows.Input;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Prism.Commands;
using Prism.Mvvm;
using CapitalExchange.Frameworks.Validation;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;

namespace CapitalExchange.UI.Common.Dialogs.Orders.ViewModel
{
    public class CreateSellOrderDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IStockEngine stockEngine;
        private readonly IUserService userService;
        private readonly IUser activeUser;
        private PropertyObserver<IUser> activeUserObserver;
        private readonly IOrderService orderService;
        private readonly IUserNotificationService userNotificationService;
        private string selectedStock;
        private int amountOfSharesToSell;
        private int amountOfSellableSharesOwned;
        private decimal shareUnitPrice;
        private List<string> stockQuoteNames;
        private DateTime expiresDate;

        public CreateSellOrderDialogViewModel()
        {
            this.serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            orderService = serviceLocator.GetInstance<IOrderService>();
            stockEngine = serviceLocator.GetInstance<IStockEngine>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();
            activeUser = userService.GetActiveUser();

            SubmitCommand = new DelegateCommand<Window>(Confirm, CanExecuteSubmitCommand);
            CancelCommand = new DelegateCommand<Window>(Cancel);
            ExpiresDate = DateTime.Today.AddDays(14);

            StockQuoteNames = new List<string>();

            PropertyConnect();
            PopulateStockNameList();
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);

            activeUserObserver.RegisterHandler(user => user.Shares, Handler);
        }

        private void Handler(IUser user)
        {
            PopulateStockNameList();
        }

        private void PopulateStockNameList()
        {
            var shares = activeUser.Shares.GroupBy(o => o.Name + " (" + o.StockSymbol + ")")
                .Select(grp => grp.ToList()).ToList();

            shares.ForEach(s =>
            {
                var name = s[0].Name + " (" + s[0].StockSymbol + ")";

                if (!StockQuoteNames.Contains(name))
                    StockQuoteNames.Add(name);
            });

            OnPropertyChanged(() => StockQuoteNames);
        }

        private void Cancel(Window view)
        {
            view.Close();
        }

        private async void Confirm(Window view)
        {
            var stockSymbol = stockEngine.StockQuotes.FirstOrDefault(q => q.Value.NameAndSymbol == SelectedStock).Value?.Symbol;
            var stockName = stockEngine.StockQuotes.FirstOrDefault(q => q.Value.NameAndSymbol == SelectedStock).Value?.Name;

            await orderService.AddSellOrder(new SellOrder(ExpiresDate, ShareUnitPrice, AmountOfSharesToSell, stockName, stockSymbol, DateTime.Now, activeUser.ID), activeUser);
            userNotificationService.NotifyUser("Your Sell order has been submitted.");

            view.Close();
        }

        private bool CanExecuteSubmitCommand(Window arg)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(SelectedStock) &&
                   AmountOfSharesToSell > 0 &&
                   ShareUnitPrice > 0 &&
                   ExpiresDate > DateTime.Now.AddDays(1);
        }

        public string SelectedStock
        {
            get { return selectedStock; }
            set
            {
                if (!SetProperty(ref selectedStock, value)) return;

                var shares = activeUser.Shares.GroupBy(o => o.Name + " (" + o.StockSymbol + ")")
                    .Select(grp => grp.ToList()).ToList();

                shares.ForEach(s =>
                {
                    var name = s[0].Name + " (" + s[0].StockSymbol + ")";

                    if (name == SelectedStock)
                        AmountOfSellableSharesOwned = s.Where(x => x.SellOrderID == null).ToList().Count;
                });

                SubmitCommand.RaiseCanExecuteChanged();
            }
        }

        public DateTime ExpiresDate
        {
            get { return expiresDate; }
            set
            {
                if (SetProperty(ref expiresDate, value))
                {
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public List<string> StockQuoteNames
        {
            get { return stockQuoteNames; }
            set
            {
                var selectedStockQuote = SelectedStock;
                if (!SetProperty(ref stockQuoteNames, value)) return;
                if (!InputValidationRules.StringNotEmptyWhiteSpaceOrNull(selectedStockQuote)) return;
                if (value.Contains(selectedStockQuote))
                    SelectedStock = selectedStockQuote;
            }
        }

        public decimal ShareUnitPrice
        {
            get { return shareUnitPrice; }
            set
            {
                if (SetProperty(ref shareUnitPrice, value))
                {
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSharesToSell
        {
            get { return amountOfSharesToSell; }
            set
            {
                if (SetProperty(ref amountOfSharesToSell, value))
                {
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSellableSharesOwned
        {
            get { return amountOfSellableSharesOwned; }
            set
            {
                if (SetProperty(ref amountOfSellableSharesOwned, value))
                {
                    SubmitCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DelegateCommand<Window> SubmitCommand { get; }
        public ICommand CancelCommand { get; }
    }
}