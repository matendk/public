﻿using System;
using System.Linq;
using System.Windows.Input;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using System.Windows;
using CapitalExchange.Frameworks.Validation;
using Prism.Mvvm;

namespace CapitalExchange.UI.Common.Dialogs.Orders.ViewModel
{
    public class EditSellOrderDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IOrderService orderService;
        private readonly IUserNotificationService userNotificationService;
        private readonly IUser activeUser;
        private readonly IStockEngine stockEngine;

        private PropertyObserver<IUser> activeUserObserver;

        private readonly ISellOrder sellOrder;
        private string selectedStock;
        private int amountOfSharesToSell;
        private int amountOfSellableSharesOwned;
        private decimal shareUnitPrice;
        private DateTime expiresDate;

        public EditSellOrderDialogViewModel(ISellOrder sellOrder)
        {
            this.serviceLocator = ServiceLocator.Current;
            this.sellOrder = sellOrder;

            ApplyCommand = new DelegateCommand<Window>(Apply, CanExecuteApplyCommand);
            CancelCommand = new DelegateCommand<Window>(Cancel);
            stockEngine = serviceLocator.GetInstance<IStockEngine>();
            userService = serviceLocator.GetInstance<IUserService>();
            orderService = serviceLocator.GetInstance<IOrderService>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();
            activeUser = userService.GetActiveUser();

            var stock = stockEngine.StockQuotes.FirstOrDefault(x => x.Value.Symbol == sellOrder.StockSymbol);

            SelectedStock = stock.Value.NameAndSymbol;

            AmountOfSellableSharesOwned = SellableShares; 

            ExpiresDate = sellOrder.ExpirationDate;
            ShareUnitPrice = sellOrder.Price;
            AmountOfSharesToSell = sellOrder.Volume;

            PropertyConnect();
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);

            activeUserObserver.RegisterHandler(user => user.Shares, user => UpdateShares());
        }

        private void UpdateShares()
        {
            AmountOfSellableSharesOwned = SellableShares;
        }

        private void Cancel(Window view)
        {
            view.Close();
        }

        private async void Apply(Window view)
        {
            sellOrder.Price = ShareUnitPrice;
            sellOrder.Volume = AmountOfSharesToSell;
            sellOrder.ExpirationDate = ExpiresDate;
            await orderService.UpdateSellOrder(sellOrder, activeUser);
            view.Close();

            userNotificationService.NotifyUser("Your buy order has been changed.");
            view.Close();
        }

        private bool CanExecuteApplyCommand(Window arg)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(SelectedStock) &&
                   AmountOfSharesToSell > 0 &&
                   AmountOfSharesToSell <= AmountOfSellableSharesOwned &&
                   ShareUnitPrice > 0 &&
                   ExpiresDate > DateTime.Now.AddDays(1);
        }

        public string SelectedStock
        {
            get { return selectedStock; }
            set
            {
                if (SetProperty(ref selectedStock, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DateTime ExpiresDate
        {
            get { return expiresDate; }
            set
            {
                if (SetProperty(ref expiresDate, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal ShareUnitPrice
        {
            get { return shareUnitPrice; }
            set
            {
                if (SetProperty(ref shareUnitPrice, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSharesToSell
        {
            get { return amountOfSharesToSell; }
            set
            {
                if (SetProperty(ref amountOfSharesToSell, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSellableSharesOwned
        {
            get { return amountOfSellableSharesOwned; }
            set
            {
                if (SetProperty(ref amountOfSellableSharesOwned, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int SellableShares => activeUser.Shares.Where(x => x.SellOrderID == null ||
                                                            x.SellOrderID == sellOrder.ID)
                                                            .Count(s => s.StockSymbol == sellOrder.StockSymbol);

        public DelegateCommand<Window> ApplyCommand { get; }
        public ICommand CancelCommand { get; }
    }
}