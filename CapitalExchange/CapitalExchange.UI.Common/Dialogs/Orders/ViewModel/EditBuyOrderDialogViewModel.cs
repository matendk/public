﻿using System;
using System.Linq;
using System.Windows.Input;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;
using System.Windows;
using Prism.Commands;
using CapitalExchange.Frameworks.Validation;

namespace CapitalExchange.UI.Common.Dialogs.Orders.ViewModel
{
    public class EditBuyOrderDialogViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private readonly IOrderService orderService;
        private readonly IStockEngine stockEngine;
        private readonly IUserNotificationService userNotificationService;
        private readonly IBuyOrder buyOrder;
        private string selectedStock;
        private int amountOfSharesToBuy;
        private decimal shareUnitPrice;
        private DateTime expiresDate;
        private IUser activeUser;

        public EditBuyOrderDialogViewModel(IBuyOrder buyOrder)
        {
            this.serviceLocator = ServiceLocator.Current;
            this.buyOrder = buyOrder;
            userService = serviceLocator.GetInstance<IUserService>();
            orderService = serviceLocator.GetInstance<IOrderService>();
            stockEngine = serviceLocator.GetInstance<IStockEngine>();
            userNotificationService = serviceLocator.GetInstance<IUserNotificationService>();
            activeUser = userService.GetActiveUser();

            ApplyCommand = new DelegateCommand<Window>(Apply, CanExecuteApplyCommand);
            CancelCommand = new DelegateCommand<Window>(Cancel);

            var stock = stockEngine.StockQuotes.FirstOrDefault(x => x.Value.Symbol == buyOrder.StockSymbol);

            SelectedStock = stock.Value.NameAndSymbol;
            ShareUnitPrice = buyOrder.Price;
            ExpiresDate = buyOrder.ExpirationDate;
            AmountOfSharesToBuy = buyOrder.Volume;
        }

        private void Cancel(Window view)
        {
            view.Close();
        }

        private async void Apply(Window view)
        {
            var newPrice = (ShareUnitPrice * AmountOfSharesToBuy) - (buyOrder.Price * buyOrder.Volume);

            if (newPrice > 0)
            {
                userService.RemoveFunds(Math.Abs(newPrice), activeUser);
                userService.AddReservedFunds(Math.Abs(newPrice), activeUser);
            }
            else
            {
                userService.RemoveReservedFunds(Math.Abs(newPrice), activeUser);
                userService.AddFunds(Math.Abs(newPrice), activeUser);
            }

            buyOrder.Price = ShareUnitPrice;
            buyOrder.Volume = AmountOfSharesToBuy;
            buyOrder.ExpirationDate = ExpiresDate;
            await orderService.UpdateBuyOrder(buyOrder);
            view.Close();

            userNotificationService.NotifyUser("Your buy order has been changed.");
            view.Close();
        }

        private bool CanExecuteApplyCommand(Window arg)
        {
            return InputValidationRules.StringNotEmptyWhiteSpaceOrNull(SelectedStock) &&
                   AmountOfSharesToBuy > 0 &&
                   ShareUnitPrice > 0 &&
                   ExpiresDate > DateTime.Now.AddDays(1);
        }

        public string SelectedStock
        {
            get { return selectedStock; }
            set
            {
                if (SetProperty(ref selectedStock, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public DateTime ExpiresDate
        {
            get { return expiresDate; }
            set
            {
                if (SetProperty(ref expiresDate, value))
                {
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal ShareUnitPrice
        {
            get { return shareUnitPrice; }
            set
            {
                if (SetProperty(ref shareUnitPrice, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int AmountOfSharesToBuy
        {
            get { return amountOfSharesToBuy; }
            set
            {
                if (SetProperty(ref amountOfSharesToBuy, value))
                {
                    OnPropertyChanged(() => TotalPrice);
                    ApplyCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public decimal TotalPrice => AmountOfSharesToBuy * ShareUnitPrice;

        public DelegateCommand<Window> ApplyCommand { get; }
        public ICommand CancelCommand { get; }
    }
}