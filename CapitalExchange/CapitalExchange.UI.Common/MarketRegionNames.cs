﻿namespace CapitalExchange.UI.Common
{
    public static class MarketRegionNames
    {
        public const string StockChartRegion = nameof(StockChartRegion);
        public const string MarketDataTableRegion = nameof(MarketDataTableRegion);
        public const string MarketFundsAndStatisticsRegion = nameof(MarketFundsAndStatisticsRegion);
    }
}