﻿namespace CapitalExchange.UI.Common
{
    public static class StockRegionNames
    {
        public const string BuyOrdersRegion = nameof(BuyOrdersRegion);
        public const string SellOrdersRegion = nameof(SellOrdersRegion);
        public const string OwnedSharesRegion = nameof(OwnedSharesRegion);
        public const string StockFundsAndStatisticsRegion = nameof(StockFundsAndStatisticsRegion);
    }
}