﻿namespace CapitalExchange.UI.Common
{
    public static class RegionNames
    {
        public const string NavigationBarRegion = nameof(NavigationBarRegion);
        public const string MainRegion = nameof(MainRegion);
        public const string AccountSetting = nameof(AccountSetting);
        public const string StatusRegion = nameof(StatusRegion);
    }
}