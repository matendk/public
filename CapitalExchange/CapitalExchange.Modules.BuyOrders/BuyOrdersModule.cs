﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.BuyOrders.View;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;

namespace CapitalExchange.Modules.BuyOrders
{
    [ModuleExport(nameof(BuyOrdersModule), typeof(BuyOrdersModule))]
    public class BuyOrdersModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(StockRegionNames.BuyOrdersRegion, typeof(BuyOrdersView));
        }
    }
}