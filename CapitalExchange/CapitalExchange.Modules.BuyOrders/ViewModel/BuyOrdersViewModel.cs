﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using Prism.Mvvm;
using CapitalExchange.SharedTypes.Interfaces;
using System.Windows.Input;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using CapitalExchange.UI.Common.Dialogs.Orders.View;
using System.Linq;
using System.Windows;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;

namespace CapitalExchange.Modules.BuyOrders.ViewModel
{
    [Export]
    public class BuyOrdersViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IOrderService orderService;
        private readonly IUserService userService;
        private IBuyOrder selectedOrder;
        private readonly IUser activeUser;
        private ObservableCollection<IBuyOrder> buyOrderList;
        private PropertyObserver<IOrderService> orderServicePropertyObserver;

        [ImportingConstructor]
        public BuyOrdersViewModel()
        {
            serviceLocator = ServiceLocator.Current;
            orderService = serviceLocator.GetInstance<IOrderService>();
            userService = serviceLocator.GetInstance<IUserService>();
            activeUser = userService.GetActiveUser();


            CreateBuyOrderCommand = new DelegateCommand(CreateBuyOrderCommandExecute);
            SellCommand = new DelegateCommand(Sell);
            DeleteCommand = new DelegateCommand(Delete);
            EditCommand = new DelegateCommand(Edit);

            PropertyConnect();
        }

        private void PropertyConnect()
        {
            orderServicePropertyObserver = new PropertyObserver<IOrderService>(orderService);
            orderServicePropertyObserver.RegisterHandler(orders => orderService.BuyOrdersList, orders => UpdateBuyOrder(orders.BuyOrdersList));
        }

        private void UpdateBuyOrder(IEnumerable<IBuyOrder> buyOrdersList)
        {
            Application.Current.Dispatcher.BeginInvoke((Action)(() =>
            {
                if (BuyOrderList == null)
                {
                    BuyOrderList = new ObservableCollection<IBuyOrder>(buyOrdersList);
                    return;
                }

                int? tempSelectedOrderId = null;

                if (SelectedOrder != null)
                    tempSelectedOrderId = SelectedOrder.ID;

                BuyOrderList.Clear();
                foreach (var item in buyOrdersList)
                {
                    BuyOrderList.Add(item);
                }

                if (tempSelectedOrderId != null)
                {
                    var order = BuyOrderList.SingleOrDefault(x => x.ID == tempSelectedOrderId.Value);

                    if (order != null)
                        SelectedOrder = order;
                }
            }));
        }

        private void CreateBuyOrderCommandExecute()
        {
            var createBuyOrderDialogView = new CreateBuyOrderDialogView();
            createBuyOrderDialogView.Show();
        }

        private void Edit()
        {
            var editBuyOrderDialogView = new EditBuyOrderDialogView(SelectedOrder);
            editBuyOrderDialogView.ShowDialog();
        }

        private void Sell()
        {
            var sellStockDialogView = new SellStockDialogView(SelectedOrder);
            sellStockDialogView.Show();           
        }

        private void Delete()
        {
            if (SelectedOrder == null) return;
            if (MessageBox.Show("Are you certain you want to delete order?", "Cofirm Delete",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes) return;

            userService.AddFunds(selectedOrder.Price * selectedOrder.Volume, activeUser);
            userService.RemoveReservedFunds(selectedOrder.Price * selectedOrder.Volume, activeUser);
            orderService.DeleteBuyOrder(selectedOrder);
        }

        public IBuyOrder SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                SetProperty(ref selectedOrder, value);

                OnPropertyChanged(() => UserOwnsSelectedOrder);
                OnPropertyChanged(() => UserOwnsSelectedShareAndOrderNotOwned);
            }
        }

        public ObservableCollection<IBuyOrder> BuyOrderList
        {
            get { return buyOrderList; }
            set
            {
                SetProperty(ref buyOrderList, value);
            }
        }

        public bool UserOwnsSelectedOrder => SelectedOrder?.UserID == userService.GetActiveUser().ID;

        public bool UserOwnsSelectedShareAndOrderNotOwned
        {
            get
            {
                return SelectedOrder != null &&
                       userService.GetActiveUser().Shares.Any(s => s.StockSymbol == SelectedOrder.StockSymbol) &&
                       SelectedOrder.UserID != activeUser.ID;
            }
        }

        public ICommand SellCommand { get; }
        public ICommand CreateBuyOrderCommand { get; }
        public ICommand DeleteCommand { get; }
        public ICommand EditCommand { get; set; }
    }
}