﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.BuyOrders.ViewModel;

namespace CapitalExchange.Modules.BuyOrders.View
{
    /// <summary>
    /// Interaction logic for BuyOrdersView.xaml
    /// </summary>
    [Export]
    public partial class BuyOrdersView : UserControl
    {
        [ImportingConstructor]
        public BuyOrdersView()
        {
            InitializeComponent();
        }

        [Import]
        public BuyOrdersViewModel ViewModel
        {
            get
            {
                return (BuyOrdersViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
