﻿using Prism.Modularity;
using Prism.Regions;
using System;
using System.ComponentModel.Composition;
using CapitalExchange.Modules.Account.View;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;

namespace CapitalExchange.Modules.Account
{
    [ModuleExport(nameof(AccountModule), typeof(AccountModule))]
    public class AccountModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(RegionNames.AccountSetting, typeof(AccountNavigationView));
        }
    }
}