﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.Modules.Account.ViewModel;

namespace CapitalExchange.Modules.Account.View
{
    /// <summary>
    /// Interaction logic for AccountNavigationView.xaml
    /// </summary>
    [Export]
    public partial class AccountNavigationView : UserControl
    {
        [ImportingConstructor]
        public AccountNavigationView()
        {
            InitializeComponent();
        }

        [Import]
        public AccountNavigationViewModel ViewModel
        {
            get { return (AccountNavigationViewModel) this.DataContext; }
            set { DataContext = value; }
        }
    }
}
