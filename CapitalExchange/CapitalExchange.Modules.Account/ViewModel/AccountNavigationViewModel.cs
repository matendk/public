﻿using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Input;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using CapitalExchange.UI.Common.Dialogs.Account.View;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;

namespace CapitalExchange.Modules.Account.ViewModel
{
    [Export]
    public class AccountNavigationViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserService userService;
        private PropertyObserver<IUser> userPropertyObserver;
        private IUser user;
        [ImportingConstructor]
        public AccountNavigationViewModel()
        {
            serviceLocator = ServiceLocator.Current;
            userService = serviceLocator.GetInstance<IUserService>();
            user = userService.GetActiveUser();
            AccountCommand = new DelegateCommand<UIElement>(Manage);
            if (user != null) //else it crash due to user being null on property connect
            {
                UpdateName();
                PropertyConnect();
            }
        }

        private void PropertyConnect()
        {
            userPropertyObserver = new PropertyObserver<IUser>(user);
            userPropertyObserver.RegisterHandler(user => user.Name, user => UpdateName())
                .RegisterHandler(user => user.Surname, user => UpdateName());
        }

        private void UpdateName()
        {
            OnPropertyChanged(() => Name);
        }

        private void Manage(UIElement element)
        {
            AccountManagementDialogView accountManagementDialogView = new AccountManagementDialogView();
            accountManagementDialogView.Owner = Window.GetWindow(element);
            accountManagementDialogView.ShowDialog();
        }

        public string Name => (user.Name + " " + user.Surname);

        public ICommand AccountCommand { get; set; }
    }
}
