﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using System.Windows;
using CapitalExchange.SharedTypes.Interfaces;
using CapitalExchange.SharedTypes.Models;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using CapitalExchange.UI.Common.Dialogs.Orders.View;
using Prism.Commands;
using Prism.Mvvm;

// ReSharper disable InconsistentNaming
namespace CapitalExchange.Modules.OwnedShares.ViewModel
{
    [Export]
    public class OwnedSharesViewModel : BindableBase
    {
        #region Constructor
        [ImportingConstructor]
        public OwnedSharesViewModel(IStockEngine stockEngine, IUserService userService)
        {
            this.stockEngine = stockEngine;
            this.userService = userService;
            this.activeUser = userService.GetActiveUser();
            this.usCultureInfo = CultureInfo.CreateSpecificCulture("en-US");
            this.usCultureInfo.NumberFormat.NumberNegativePattern = 1;

            PropertyConnect();
            stockEngine.RegisterForUpdate(UpdateAction);

            SellStockCommand = new DelegateCommand<UIElement>(SellStockCommandExecute);
        }
        #endregion


        #region Privates
        private readonly IStockEngine stockEngine;
        private readonly IUserService userService;
        private readonly IUser activeUser;
        private readonly CultureInfo usCultureInfo;
        private PropertyObserver<IUser> activeUserObserver;
        #endregion


        #region Properties
        private ObservableCollection<IOwnShare> shares;
        public ObservableCollection<IOwnShare> Shares { get { return shares; } set { SetProperty(ref shares, value); } }
        #endregion


        #region Commands
        public DelegateCommand<UIElement> SellStockCommand { get; set; }
        private void SellStockCommandExecute(UIElement element)
        {
            var createSellOrderDialogView = new CreateSellOrderDialogView();
            createSellOrderDialogView.Owner = Window.GetWindow(element);
            createSellOrderDialogView.ShowDialog();
        }
        #endregion


        #region Methods
        private void UpdateAction(IEnumerable<LiveStockQuote> liveStockQuotes)
        {
            Shares = new ObservableCollection<IOwnShare>();

            //Group shares by stock symbol, last buy price, & last buy date
            var shareGroups = activeUser.Shares.GroupBy(share => new { share.StockSymbol, share.LastBuyPrice, share.LastBuyDate })
                .Select(g => g.ToList())
                .ToList();

            foreach (var shareGroup in shareGroups)
            {
                LiveStockQuote stock = liveStockQuotes.FirstOrDefault(e => e.Symbol == shareGroup.First().StockSymbol);
                if (stock == null) continue;

                //Setup new own share and calculate values by looping through share group
                string name = "";
                decimal lastBuyPrice = 0m;
                decimal price = 0m;
                decimal value = 0m;
                foreach (var share in shareGroup)
                {
                    name = share.Name + " (" + share.StockSymbol + ")";
                    lastBuyPrice = share.LastBuyPrice;

                    decimal tempPrice;
                    decimal.TryParse(stock.LastTradePriceOnly, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out tempPrice);
                    price = tempPrice;

                    decimal tempValue;
                    decimal.TryParse(stock.LastTradePriceOnly, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo, out tempValue);
                    value += tempValue;
                }

                //Add a new own share
                Shares.Add(new OwnShare(name, shareGroup.Count.ToString(), lastBuyPrice.ToString("C", usCultureInfo), price.ToString("C", usCultureInfo), value.ToString("C", usCultureInfo)));
            }
        }

        private void PropertyConnect()
        {
            activeUserObserver = new PropertyObserver<IUser>(activeUser);
            activeUserObserver.RegisterHandler(user => user.Shares, user => OnPropertyChanged(() => Shares));
        }
        #endregion
    }
}