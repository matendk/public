﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapitalExchange.Modules.OwnedShares.ViewModel;

namespace CapitalExchange.Modules.OwnedShares.View
{
    /// <summary>
    /// Interaction logic for OwnedSharesView.xaml
    /// </summary>
    [Export]
    public partial class OwnedSharesView : UserControl
    {
        [ImportingConstructor]
        public OwnedSharesView()
        {
            InitializeComponent();
        }

        [Import]
        public OwnedSharesViewModel ViewModel
        {
            get
            {
                return (OwnedSharesViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
