﻿using System.ComponentModel.Composition;
using CapitalExchange.UI.Common;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using CapitalExchange.Modules.OwnedShares.View;

namespace CapitalExchange.Modules.OwnedShares
{
    [ModuleExport(nameof(OwnedSharesModule), typeof(OwnedSharesModule))]
    public class OwnedSharesModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(StockRegionNames.OwnedSharesRegion, typeof(OwnedSharesView));
        }
    }
}