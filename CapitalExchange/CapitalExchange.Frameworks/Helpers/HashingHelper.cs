﻿using System.Security.Cryptography;
using System.Text;

namespace CapitalExchange.Frameworks.Helpers
{
    public static class HashingHelper
    {
        /// <summary>
        /// Generates the hash.
        /// </summary>
        /// <param name="salt">The salt.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public static string GenerateHash(string salt, string password)
        {     
            //Generate salt and the password that will be hashed    
            var firstHalfOfpassword = password.Substring(0, password.Length / 2);
            var secondHalfOfpassword = password.Substring(password.Length / 2, password.Length - password.Length / 2);
            var passwordToHash = firstHalfOfpassword + salt + secondHalfOfpassword;
            
            //Hash the password            
            var sha = new SHA512Managed();
            var hash = sha.ComputeHash(Encoding.ASCII.GetBytes(passwordToHash));
            
            //Stringify the hash         
            var strBuilder = new StringBuilder();
            foreach (byte b in hash) { strBuilder.AppendFormat("{0:x2}", b); }
            return strBuilder.ToString();
        }

        /// <summary>
        /// Generates the salt.
        /// </summary>
        /// <returns></returns>
        public static string GenerateSalt()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.            
                var data = new byte[64];
                
                // Fill buffer.               
                rng.GetBytes(data);
                
                //Convert the bytes to a string       
                var strBuilder = new StringBuilder();
                foreach (byte b in data)
                {
                    strBuilder.AppendFormat("{0:x2}", b);
                }

                return strBuilder.ToString();
            }
        }
    }
}
