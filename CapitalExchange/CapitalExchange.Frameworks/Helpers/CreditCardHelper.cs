﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CapitalExchange.SharedTypes;

namespace CapitalExchange.Frameworks.Helpers
{
    /// <summary>
    /// Credit Card validation helper
    /// </summary>
    public static class CreditCardHelper
    {
        /// <summary>
        ///  This dictionary contains the type of credit card with a regular expression to validate for said type.
        /// For reference look here: http://www.regular-expressions.info/creditcard.html
        /// </summary>
        private static readonly Dictionary<CreditCardType, Regex> CreditCardDictionary =
            new Dictionary<CreditCardType, Regex>()
            {
                {  CreditCardType.Visa, new Regex(@"^4[0-9]{6,}$") },
                {  CreditCardType.MasterCard, new Regex(@"^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$") },
                {  CreditCardType.AmericanExpress, new Regex(@"^3[47][0-9]{5,}$") },
                {  CreditCardType.Discover, new Regex(@"^6(?:011|5[0-9]{2})[0-9]{3,}$") },
                {  CreditCardType.Jcb, new Regex(@"^6(?:011|5[0-9]{2})[0-9]{3,}$") },
            };

        internal static bool LuhnCheck(string number)
        {
            if (number.Length < 10 || number.Length > 19) return false; //If the number is too short or to long, return false
            return number.Reverse().Select(CheckDigit).Sum() % 10 == 0; //Get the sum of all numbers and do modulus 10 on it, do boolean return
        }

        internal static int CheckDigit(char digitChar, int position)
        {
            var digit = Convert.ToInt32(digitChar.ToString()); //Convert digit char to int
            if (position % 2 == 1) //If digit position is odd, multiply digit by 2
                digit *= 2;

            return digit % 10 + digit / 10; //Do modulus 10 on digit and then add the result of digit / 10 and return
        }

        internal static bool LuhnCheckV(string number)
        {
            //If the provided number is too short, return false
            if (number.Length < 10) return false;

            // 1. Convert the string to array of ints and reverse
            var array = number.Select(Convert.ToInt32).Reverse().ToArray();
            var result = 0;

            // 2. Loop through the array
            for (var i = 0; i < array.Count(); i++)
            {
                var currentNumber = array[i];

                // 3. Get each second number and multiply by 2
                if (i % 2 == 0)
                {
                    currentNumber *= 2;

                    // 4. If number is 2 digits (over 10) then seperate digits into 2 and add together
                    if (currentNumber > 9)
                    {
                        currentNumber = (currentNumber - 10) + 1;
                    }
                }

                // 5. Add the result to variable
                result += currentNumber;
            }

            // 6. If the result of mod 10 of result is 0, then its a valid credit card number.
            return result % 10 == 0;
        }

        public static CreditCardType GetCreditCardType(string creditCard)
        {
            return CreditCardDictionary.Where(type => type.Value.IsMatch(creditCard)).Select(type => type.Key).FirstOrDefault();
        }
    }
}