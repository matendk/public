﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CapitalExchange.Frameworks.Helpers
{
    public static class SimpleMailer
    {
        /// <summary>
        /// Mails to receiver
        /// </summary>
        /// <param name="destinationAddress">The destination address.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        public static void MailTo( string destinationAddress, string subject, string body)
        {
            Task.Run(() =>
            {
                const string smtpAddress = "smtp.gmail.com";
                const int portNumber = 587;
                const bool enableSsl = true;

                const string emailFrom = "capitalexchangedk@gmail.com";
                const string password = "CapExchangeH6";

                using (var mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(destinationAddress);
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = false;

                    using (var smtpClient = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new NetworkCredential(emailFrom, password);
                        smtpClient.EnableSsl = enableSsl;
                        smtpClient.Send(mail);
                    }
                }
            });
        }
    }
}