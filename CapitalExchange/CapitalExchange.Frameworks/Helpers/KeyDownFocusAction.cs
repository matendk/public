﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace CapitalExchange.Frameworks.Helpers
{
    public class KeyDownFocusAction : TriggerAction<UIElement>
    {
        public static readonly DependencyProperty KeyProperty = DependencyProperty.Register("Key", typeof(Key), typeof(KeyDownFocusAction));
        public Key Key
        {
            get { return (Key)GetValue(KeyProperty); }
            set { SetValue(KeyProperty, value); }
        }

        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register("Target", typeof(UIElement), typeof(KeyDownFocusAction), new UIPropertyMetadata(null));
        public UIElement Target
        {
            get { return (UIElement)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }

        protected override void Invoke(object parameter)
        {
            if (Keyboard.IsKeyDown(Key) && Target != null)
                Target.Focus();
        }
    }
}