﻿using System;
using System.Linq;
using System.Text;

namespace CapitalExchange.Frameworks.Helpers
{
    public static class PasswordGeneratorHelper
    {
        public static string GenerateNewPassword(int length)
        {
            var tempLength = length;

            const string validCamelCharacters = "abcdefghijklmnopqrstuvwxyz";
            const string validPascalCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string validNumbers = "1234567890";

            var result = new StringBuilder();
            var rnd = new Random();

            var amount1 = rnd.Next(1, tempLength - 2);
            tempLength -= amount1;

            var amount2 = rnd.Next(1, tempLength - 1);
            tempLength -= amount2;

            var amount3 = tempLength;

            while (0 < amount1--)
            {
                result.Append(validCamelCharacters[rnd.Next(validCamelCharacters.Length)]);
            }
            while (0 < amount2--)
            {
                result.Append(validPascalCharacters[rnd.Next(validPascalCharacters.Length)]);
            }
            while (0 < amount3--)
            {
                result.Append(validNumbers[rnd.Next(validNumbers.Length)]);
            }

            // Create new string from the reordered char array
            var randomizedPassword = new string(result.ToString().ToCharArray().
                            OrderBy(s => (rnd.Next(2) % 2) == 0).ToArray());

            return randomizedPassword;
        }
    }
}