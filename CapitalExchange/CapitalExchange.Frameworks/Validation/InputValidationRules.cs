﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CapitalExchange.Frameworks.Helpers;
using CapitalExchange.SharedTypes;

namespace CapitalExchange.Frameworks.Validation
{
    public static class InputValidationRules
    {
        public static bool ValidateEmail(string email)
        {
            // TODO: Handle this more nicely
            try
            {
                if (string.IsNullOrEmpty(email)) return false;

                var isAddress = (new System.Net.Mail.MailAddress(email)).Address == email;
                return isAddress && Path.HasExtension(email);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool ValidatePassword(string password)
        {
            if (string.IsNullOrEmpty(password)) return false;

            var numberCheck = new Regex(@"[0-9]+"); // Does password include numbers?
            var upperCharacterCheck = new Regex(@"[A-Z]+"); // does password include one upper letter?

            return numberCheck.IsMatch(password)
                && upperCharacterCheck.IsMatch(password)
                && password.Length >= 8; // Also check if length is over 8
        }

        public static bool StringNotEmptyWhiteSpaceOrNull(string input)
        {
            return !(string.IsNullOrWhiteSpace(input) || string.IsNullOrEmpty(input));
        }

        public static bool ValidateCardNumber(string cardNumber)
        {
            if (string.IsNullOrEmpty(cardNumber)) return false;

            return cardNumber.All(char.IsDigit) && CreditCardHelper.LuhnCheck(cardNumber);
        }

        public static CreditCardType GetCreditCardType(string cardNumber)
        {
            return CreditCardHelper.GetCreditCardType(cardNumber);
        }

        public static bool ValidateCvvNumber(string number)
        {
            var cvvRegex = new Regex(@"^[0-9]{3,4}$");
            return cvvRegex.IsMatch(number);
        }

        public static bool ValidateCreditCardExpirationDate(DateTime expirationDate)
        {
            return expirationDate > DateTime.Today.AddDays(1);
        }

        public static bool ValidateUserPassword(string password, string salt, string hashedPassword)
        {
            return HashingHelper.GenerateHash(salt, password) == hashedPassword;
        }

        //https://dotnet-snippets.de/snippet/iban-checksumme-pruefen/3809
        //http://iban.saltug.net/
        public static bool ValidateIBAN(string iban)
        {
            if (iban.Length < 15 || iban.Length > 31 || !StringNotEmptyWhiteSpaceOrNull(iban))
                return false;

            //Move first four characters to the end of the string
            iban = iban.Substring(4) + iban.Substring(0, 4);

            //Convert letters in iban to digits
            var convertedIban = iban.ToUpper().Aggregate("", (current, c) => current + (char.IsLetter(c) ? (c - 55).ToString() : c.ToString()));

            //Convert iban to decimal (int is too short)
            decimal resultIban = 0;
            decimal.TryParse(convertedIban, out resultIban);

            //Do modulus 97 on iban and return boolean
            return ((resultIban % 97) == 1);
        }

        //http://stackoverflow.com/questions/3028150/what-is-proper-regex-expression-for-swift-codes
        //https://en.wikipedia.org/wiki/ISO_9362
        public static bool ValidateBIC(string bic)
        {
            return new Regex("^[A-Z]{6}[A-Z0-9]{2}([A-Z0-9]{3})?$").IsMatch(bic);
        }
    }
}