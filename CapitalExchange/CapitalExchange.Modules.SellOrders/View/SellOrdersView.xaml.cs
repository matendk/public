﻿using System.ComponentModel.Composition;
using System.Windows.Controls;
using CapitalExchange.Modules.SellOrders.ViewModel;

namespace CapitalExchange.Modules.SellOrders.View
{
    /// <summary>
    /// Interaction logic for SellOrdersView.xaml
    /// </summary>
    [Export]
    public partial class SellOrdersView : UserControl
    {
        [ImportingConstructor]
        public SellOrdersView()
        {
            InitializeComponent();
        }

        [Import]
        public SellOrdersViewModel ViewModel
        {
            get
            {
                return (SellOrdersViewModel)this.DataContext;
            }
            set
            {
                this.DataContext = value;
            }
        }
    }
}
