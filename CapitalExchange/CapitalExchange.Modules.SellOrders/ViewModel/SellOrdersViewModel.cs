﻿using System;
using CapitalExchange.SharedTypes.Interfaces;
using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using System.ComponentModel.Composition;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CapitalExchange.UI.Common.Dialogs.Orders.View;
using System.Windows;
using System.Linq;
using CapitalExchange.SharedTypes.ServiceInterfaces;
using CapitalExchange.UI.Common;
using Prism.Mvvm;

namespace CapitalExchange.Modules.SellOrders.ViewModel
{
    [Export]
    public class SellOrdersViewModel : BindableBase
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IOrderService orderService;
        private readonly IUserService userService;
        private PropertyObserver<IOrderService> orderServicePropertyObserver;
        private ObservableCollection<ISellOrder> sellOrderList;

        [ImportingConstructor]
        public SellOrdersViewModel()
        {
            serviceLocator = ServiceLocator.Current;
            orderService = serviceLocator.GetInstance<IOrderService>();
            userService = serviceLocator.GetInstance<IUserService>();

            BuyCommand = new DelegateCommand(Buy);
            CreateSellCommand = new DelegateCommand(CreateSell);
            DeleteCommand = new DelegateCommand(Delete);
            EditCommand = new DelegateCommand(Edit);

            PropertyConnect();
        }

        private void PropertyConnect()
        {
            orderServicePropertyObserver = new PropertyObserver<IOrderService>(orderService);
            orderServicePropertyObserver.RegisterHandler(orders => orderService.SellOrdersList, orders => UpdateSellOrders(orders.SellOrdersList));
        }

        private void UpdateSellOrders(IEnumerable<ISellOrder> sellOrders)
        {
            Application.Current.Dispatcher.BeginInvoke((Action)(() =>
            {
                if (SellOrderList == null)
                {
                    SellOrderList = new ObservableCollection<ISellOrder>(sellOrders);
                    return;
                }

                int? tempSelectedOrderId = null;

                if (SelectedOrder != null)
                    tempSelectedOrderId = SelectedOrder.ID;

                SellOrderList.Clear();
                foreach (var item in sellOrders)
                {
                    SellOrderList.Add(item);
                }

                if (tempSelectedOrderId != null)
                {
                    var order = SellOrderList.SingleOrDefault(x => x.ID == tempSelectedOrderId.Value);

                    if (order != null)
                        SelectedOrder = order;
                }
            }));
        }

        private void Edit()
        {
            var editSellOrderDialogView = new EditSellOrderDialogView(selectedOrder);
            editSellOrderDialogView.ShowDialog();
        }

        private void Buy()
        {
            var buyStockDialogView = new BuyStockDialogView(selectedOrder);
            buyStockDialogView.ShowDialog();
        }

        private void CreateSell()
        {
            var createSellOrderDialogView = new CreateSellOrderDialogView();
            createSellOrderDialogView.ShowDialog();
        }

        private void Delete()
        {
            if (SelectedOrder != null)
            {
                if (MessageBox.Show("Are you certain you want to delete order", "Cofirm Delete",
                    MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    orderService.DeleteSellOrder(selectedOrder);
                }
            }
        }

        public bool UserOwnsSelectedOrder => SelectedOrder?.UserID == userService.GetActiveUser().ID;
        public bool HaveSelectedAndNotOwned => SelectedOrder != null && SelectedOrder.UserID != userService.GetActiveUser().ID;
        public bool UserOwnsSelectedShare
        {
            get
            {
                return SelectedOrder != null && userService.GetActiveUser().Shares.Any(s => s.StockSymbol == SelectedOrder.StockSymbol);
            }
        }

        private ISellOrder selectedOrder;
        public ISellOrder SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                if (SetProperty(ref selectedOrder, value))
                {
                    OnPropertyChanged(nameof(UserOwnsSelectedOrder));
                    OnPropertyChanged(nameof(HaveSelectedAndNotOwned));
                    OnPropertyChanged(nameof(UserOwnsSelectedShare));
                }
            }
        }

        public ObservableCollection<ISellOrder> SellOrderList
        {
            get { return sellOrderList; }
            set
            {
                SetProperty(ref sellOrderList, value);
            }
        }

        public ICommand EditCommand { get; set; }
        public ICommand BuyCommand { get; }
        public ICommand CreateSellCommand { get; }
        public ICommand DeleteCommand { get; }
    }
}