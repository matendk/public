﻿using System.ComponentModel.Composition;
using CapitalExchange.Modules.SellOrders.View;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using CapitalExchange.UI.Common;

namespace CapitalExchange.Modules.SellOrders
{
    [ModuleExport(nameof(SellOrdersModule), typeof(SellOrdersModule))]
    public class SellOrdersModule : IModule
    {
        [Import]
        public IRegionManager RegionManager;

        public void Initialize()
        {
            this.RegionManager.RegisterViewWithRegion(StockRegionNames.SellOrdersRegion, typeof(SellOrdersView));
        }
    }
}